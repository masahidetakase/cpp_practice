#pragma once
/* 
 * Large-size memory stream with own virtual memory system.
 */

#include <streambuf>
#include <cstdio>
#include <boost/optional.hpp>
#include <memory>

namespace utils
{

class VirtMemStream : public std::streambuf
{
	public:
		explicit
		VirtMemStream(
			const size_t& bank_size_kb, // the size of the bank(window) on memory (unit: kbyte=1024 byte).
			const char* vm_file_path = nullptr // the swap file path. When null, tmpfile() is used.
			);

		virtual
		~VirtMemStream();

	private:
		std::FILE* m_swap_fp; // swap file

		const size_t m_bank_size; // the bank size (unit: byte)

		std::unique_ptr<char[]> m_buffer; // read/write buffer 
};

} // the end of utils namespace.