/* 
 * Large-size memory stream with own virtual memory system.
 */
#include <VirtMemStream.hpp>
#include <string>
#include <cstdio>
#include <stdexcept>
#include <algorithm>

using namespace std;
using namespace utils;

VirtMemStream::VirtMemStream(
	const size_t& bank_size_kb,
	const char* vm_file_path
	) :
	m_bank_size( bank_size_kb * 1024),
	m_buffer( new char[m_bank_size])
{
	// Create a swap file.
	if( vm_file_path != nullptr) {
		if( ( m_swap_fp = fopen( vm_file_path, "w+b")) == nullptr) {
			string ex_str = string( __PRETTY_FUNCTION__) + " file: " + vm_file_path + " not found.";
			throw invalid_argument( ex_str.c_str());
		}
	} else {
		m_swap_fp = tmpfile();
	}

	// set the buffer
	fill( m_buffer.get(), m_buffer.get() + m_bank_size, 0);
	setp( m_buffer.get(), m_buffer.get() + m_bank_size);
	setg( m_buffer.get(), m_buffer.get(), m_buffer.get() + m_bank_size);
}

VirtMemStream::~VirtMemStream()
{
	fclose( m_swap_fp);
}