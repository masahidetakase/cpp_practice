#include <iostream>
#include <string>
#include <cstring>
#include <cstdint>
#include <sstream>

using namespace std ;

struct EncodeStep
{
    uint16_t mask ;
    uint32_t num_rshift_bit ;
    size_t next_step_byte ;
} ;

template<typename T>
T doSomething()
{
    cout << "value = "  << endl ;

    return  (T)1;
}

int
main( int argc, char* argv[])
{
    static const char Sextet_To_Char[] =
        "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/" ;

    static const EncodeStep Enc_Steps[] =
        {
            { 0xFC00, 10, 0},
            { 0x03F0,  4, 1},
            { 0x0FC0,  6, 0},
            { 0x003F,  0, 2}
        } ;

    size_t original_str_len = strlen( argv[1]) ;
    size_t sextets_len = ( original_str_len * 8 + 6 - 1) / 6 ;
    size_t b64_str_len = ( (sextets_len + 4 - 1) / 4) * 4 ;

    char* b64_str = new char[b64_str_len + 1] ;
    memset( b64_str, '=', b64_str_len) ;
    b64_str[b64_str_len] = '\0' ;

    size_t enc_step_idx = 0 ;
    uint8_t* cur_octet_loc = (uint8_t*)argv[1] ;
    uint8_t* end_octet_loc = cur_octet_loc + original_str_len - 1 ;

    for( size_t sextet_idx = 0 ; sextet_idx < sextets_len ; ++sextet_idx) {

        uint16_t upper_octet = *cur_octet_loc ;
        uint16_t lower_octet = cur_octet_loc != end_octet_loc ? *( cur_octet_loc + 1) : 0 ;

        size_t table_idx =
            ( ( upper_octet << 8 | lower_octet) & Enc_Steps[enc_step_idx].mask) >>
            Enc_Steps[enc_step_idx].num_rshift_bit ;

        b64_str[sextet_idx] = Sextet_To_Char[table_idx] ;

        cur_octet_loc += Enc_Steps[enc_step_idx].next_step_byte ;

        enc_step_idx = ( enc_step_idx == 3) ? 0 : enc_step_idx + 1 ;
    }

    cout << "original: \"" << argv[1] << "\"" << endl ;
    cout << endl << b64_str << endl ;


    stringstream strm ;
    cout << "empty stream: tellp()" << strm.tellp() << endl ;
    strm << endl ;
    cout << "EOL stream: tellp()" << strm.tellp() << endl ;

    stringstream strm2 ;
    strm2 << "12 char+endl" << endl ;
    cout << "12 char+endl stream: tellp()" << strm2.tellp() << endl ;

    cout << "T = int" << endl ;
    doSomething<int>();
    doSomething<void>() ;

    return 0 ;
}
