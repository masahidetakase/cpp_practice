#pragma once

#include "RawImage.hpp"

struct ImageProcessCond
{
    std::string img_format ;
    
    std::uint32_t img_width ;
    std::uint32_t img_height ;

    image_process::ROI roi ;

    std::uint16_t rel_bits ;

    std::uint8_t r_tex_l_th ;

    std::uint32_t hist_embedded_line ;
} ;

// Create an histogram-embedded luminance image.
bool 
embedHistogramLumImage( const std::string& input_img_path, // path of intput luminance raw image file.
                        const std::string& output_img_path, // path of histogram-embedded raw image file.
                        const ImageProcessCond& cond, // image process conditions 
                        const bool& do_save_debug_infos = false) ; // When the histogram output as CSV, set this arg. true. 

bool 
embedHistogramDisparityImage( const std::string& input_img_path, // path of intput disparity raw image file.
                              const std::string& output_img_path, // path of histogram-embedded raw image file.
                              const ImageProcessCond& cond, // image process conditions 
                              const bool& do_save_debug_infos = false) ; // When the histogram output as CSV, set this arg. true. 
