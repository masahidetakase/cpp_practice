#pragma once
#include <vector>
#include "mycstdint.hpp"
#include <ostream>

namespace image_process
{

struct HistogramBin
{
    std::int32_t lowest_val ;
    std::uint32_t frequency ;
} ;


class Histogram
{
    public:
        Histogram( const std::uint16_t& lower_limit,
                   const std::uint16_t& upper_limit,
                   const std::uint16_t& bin_width) ;

        virtual ~Histogram() {}

        std::uint32_t
        pushValue( const std::uint16_t& val) ;

        const std::uint32_t& 
        getNumBins() const { return m_num_bins ;}

        const HistogramBin& 
        getBin( const std::uint16_t& bin_idx) const ;

        const std::uint32_t&
        getNumSamples() const { return m_num_samples ;}

    private:
        std::uint32_t m_num_bins ;
        std::uint32_t m_bin_width ;
        std::uint32_t m_num_samples ;

        std::vector<HistogramBin> m_bins ;
} ;

} // the end of image_process namespace

// For output a histogram data to std::ostream.
std::ostream&
operator<< ( std::ostream& output_strm,
             const image_process::Histogram& histogram) ;
