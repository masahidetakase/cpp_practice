#pragma once
#include <string>
#include <vector>
#include <map>

namespace my_utils
{

// value of command line options.
// e.g. command line option "-hoge fuga" means -> a key is "-hoge" and its value is "fuga".
class ArgValue
{
    public:
        // value type
        enum Type
        {
            string_type,
            integer_type,
            double_type,
            uhex_type,
            bool_type,

            invalid_type
        } ;

        ArgValue( const std::string& str_val) ;

        ArgValue( const int& int_val) ;

        ArgValue( const double& dbl_val) ;

        ArgValue( const unsigned int& uint_hex) ; // unsigned hexadecimal

        ArgValue( const bool& do_exist) ;

        ArgValue( const ArgValue& src) ;

        ArgValue() ;

        static ArgValue
        makeValueFromString( const std::string& str) ;

        const ArgValue&
        operator= ( const ArgValue& rhs) ;

        virtual ~ArgValue() {}

        const Type&
        getType() const ;

        // type mismatch causes a exception.
        template<typename T>
        const T&
        get() const ;

        operator std::string() const ;

    private:
        Type m_val_type ; // the type of the value.

        std::string m_str_val ; // string
        int m_int_val ; // integer value.
        double m_dbl_val ; // Double precision floating point number.
        unsigned int m_uint_val ; // unsigned integer
        bool m_bool_val ; // A option be set or not.

        void swap( ArgValue& arg_val) ;
} ;


class CmdlineArgParser
{
    public:
        typedef std::map<std::string, ArgValue> opt_val_pairs_type ;
        typedef opt_val_pairs_type result_type ;
        typedef std::vector<std::string> error_list_type ;

        CmdlineArgParser() ;

        CmdlineArgParser( const CmdlineArgParser& src) ;

        const CmdlineArgParser&
        operator= ( const CmdlineArgParser& rhs) ;

        virtual ~CmdlineArgParser() {}

        /*
           Command line options are classified into the four types.

            "-signle_opt","--signle_opt": it has no value (bool).
            "-key value", "--key value": key-value pair( string, integer or double).
            "single_value": "<0>", "<1>", "<2>",...... :"<0>" indicates argv[0]
         */
        template<typename T>
        void
        registerOpt( const std::string& opt_str, const T& default_val) ;

        void
        registerOpt( const std::string& opt_str, const char* default_val) ;

        // Clear the registered options.
        void
        reset() ;

        result_type
        parse( int argc, char* argv[], size_t* num_single_vals = NULL) ;

        // get the error information of parsing.
        const error_list_type&
        getLastErrors() const ;

    private:
        opt_val_pairs_type m_registered_pairs ;

        error_list_type m_errors ;

        void
        swap( CmdlineArgParser& cmdl_parser) ;

        // Detect whether an arg. is a key of "key-value opt"(ex. "--output hoge" or "-num 12").
        bool
        isOptionKey( const std::string& opt_str) ;

        ArgValue::Type
        getOptionType( const std::string& opt_str) ;
} ;

} // the end of my_utils namespace
