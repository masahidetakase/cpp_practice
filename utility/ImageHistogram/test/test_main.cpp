#include <iostream>
#include <fstream>
#include "../mycstdint.hpp"
#include <vector>
#include <cstring>
#include <string>
#include <cstdlib>
#include <cmath>
#include <limits>
#include <cassert> 
#include "../ImageHistogramCreator.hpp"
#include "../EdgeImageCreator.hpp"
#include "../EmbedHistogram.hpp"

using namespace std ;
using namespace image_process ;

// Constants
const uint32_t Image_Width = 1280 ;
const uint32_t Image_Height = 960 ;
const uint32_t Embed_Histogram_Col = 640 ;

// Utility functions
static uint16_t
randomUint( const uint16_t& min, const uint16_t& max) ;

static bool
isIncludedInROI( const uint32_t& x, const uint32_t& y, const ROI& roi) ;

// Test functions
static bool
test_luminanceHistogram() ;

static bool
test_DisparityHistogram() ;

int
main( int argc, char *argv[])
{
    cout << "Histogram of a luminace image: " ;
    if( test_luminanceHistogram()) {
        cout << "SUCCEED" << endl ;
    } else {
        cout << "FAIL" << endl ;
    }

    cout << "Histogram of a disparity image: " ;
    if( test_DisparityHistogram()) {
        cout << "SUCCEED" << endl ;
    } else {
        cout << "FAIL" << endl ;
    }
}

static bool
test_luminanceHistogram()
{
    // the test file created by this func.
    static const string test_lum_img_path = "_TestLumImage.raw" ;
    static const string hist_embeded_img_path = "_TestLumImage_EmbeddeHist.raw" ;
    static const string edge_8bb_img_path = "_TestEdgeImage16to8bpp.raw" ;

    // the threshold for calculating edge.
    static const uint8_t r_tex_l_th = 30u ;
     
    vector<uint32_t> cmp_lum_histgram(0x100, 0u) ;
    vector<uint32_t> cmp_edge_histgram(0x100, 0u) ;

    ROI roi( 0, 20, Image_Width - 1, Image_Height - 3) ;

    // Make a luminance image.
    {
        uint8_t* lum_image = new uint8_t[Image_Width * Image_Height] ;
        memset( lum_image, 0x00, Image_Width * Image_Height) ;
        
        // Draw random dot.
        for( uint32_t y = 0 ; y <= roi.end.y ; ++y) {
            for( uint32_t x = 0; x < Image_Width; ++x) {
                uint8_t pxlVal = (uint8_t)randomUint(0xC0, 0xFF) ;

                // draw black rect
                if( 300 <= x && x <400 && 300 <= y && y <400) {
                    pxlVal = 0 ;
                }

                // draw white rect
                if( 400 <= x && x < 600 && 300 <= y && y < 400) {
                    pxlVal = 0xFF ;
                }

                lum_image[ x + y * Image_Width] = pxlVal ;
                if( isIncludedInROI( x, y, roi)) {
                    cmp_lum_histgram[pxlVal] += 1 ;
                }
            }
        }


        // create an edge histogram.
        {
            for( uint32_t y = roi.begin.y ; y <= roi.end.y ; ++y) {
                for( uint32_t x = roi.begin.x + 2 ; x <= roi.end.x - 2; ++x) {
                    int16_t edge = 0 ;
                    int8_t s8_edge = 0 ;

                    if( isIncludedInROI( x, y, roi)) {
                        const uint8_t* cur_pxl = lum_image + x + y * Image_Width ;
                    
                        // calculate edges.
                        edge = (*( cur_pxl - 2) + *( cur_pxl - 1)) - ( *( cur_pxl + 1) + *( cur_pxl + 2)) ;

                        if( ( (*( cur_pxl - 2) + *( cur_pxl - 1)) / 2 + ( *( cur_pxl + 1) + *( cur_pxl + 2)) / 2) / 2 < r_tex_l_th) {
                            edge *= 2 ;
                        }

                        if( edge < 512) {
                            s8_edge = (int8_t)( edge >> 2) ;
                        } else {
                            s8_edge = 127 ;
                        }

                        cmp_edge_histgram[s8_edge + 128] += 1 ;
                    }
                }
            }
        }

        fstream lum_img_fstrm( test_lum_img_path.c_str(), ios::out | ios::binary) ;

        lum_img_fstrm.write( (char*)lum_image, Image_Width * Image_Height) ;

        delete[] lum_image ;
    } 

    ImageProcessCond img_cond =
        {
            string("LUM8"),
            Image_Width,
            Image_Height,
            roi,
            0,
            r_tex_l_th,
            Image_Height - 2
        } ;

    // Save the luminance image which the histograms are embeded in.
    embedHistogramLumImage( test_lum_img_path, hist_embeded_img_path, img_cond, true) ;

    // Load the the histogram-embedded luminance image.
    fstream hist_embedded_img_fstrm( hist_embeded_img_path.c_str(), ios::in | ios::binary) ;

    // verify the embedded luminance histogram.
    {
        hist_embedded_img_fstrm.seekg( Embed_Histogram_Col + Image_Width * img_cond.hist_embedded_line) ;

        uint8_t embedded_lum_hist[0x100*2] ;
        hist_embedded_img_fstrm.read( (char*)embedded_lum_hist, sizeof(embedded_lum_hist));

        for( uint32_t val = 0 ; val < 0x100 ; ++val) {
            uint32_t cmp_hist_bin_freq = cmp_lum_histgram[val] >> ( 21 - 16) ;
            uint32_t embedded_hist_bin_freq = (embedded_lum_hist[2*val] << 8) + embedded_lum_hist[2*val + 1] ;
            if( cmp_hist_bin_freq != embedded_hist_bin_freq) {
                cout << "val: " << val << " " << cmp_hist_bin_freq  << " != " << embedded_hist_bin_freq << endl ;
                return false ;
            }
        }
    }

    // verify the embedded edge histogram.
    {
        hist_embedded_img_fstrm.seekg( Embed_Histogram_Col + Image_Width * (img_cond.hist_embedded_line + 1),
                                       ios_base::beg) ;
        uint8_t embedded_edge_hist[0x100*2] ;

        hist_embedded_img_fstrm.read( (char*)embedded_edge_hist, sizeof(embedded_edge_hist)) ;

        for( int i = 0; i<= 255 ; ++i) {
            cout << "edge comp:" << (cmp_edge_histgram[i] >> ( 21 - 16)) <<  "  embd:" << (embedded_edge_hist[2*i] << 8) + embedded_edge_hist[2*i + 1] << endl ; 
        }

        for( uint32_t val = 0 ; val < 0x100 ; ++val) {
            uint32_t cmp_hist_bin_freq = cmp_edge_histgram[val] >> ( 21 - 16) ;
            uint32_t embedded_hist_bin_freq = (embedded_edge_hist[2*val] << 8) + embedded_edge_hist[2*val + 1] ;
            if( cmp_hist_bin_freq != embedded_hist_bin_freq) {
                cout << "edge val: " << val << " " << cmp_hist_bin_freq  << " != " << embedded_hist_bin_freq << endl ;
                return false ;
            }
        }
    }

    return true ;
}


static bool
test_DisparityHistogram()
{
    static const string test_disp_img_path = "_TestDispImage.raw" ;
    static const string hist_embeded_img_path = "_TestDispImage_EmbeddeHist.raw" ;
    
    vector<uint32_t> cmp_histgram(0x100, 0u) ;

    ROI roi( 0, 20, Image_Width - 1, Image_Height - 3) ;

    // Make an disparity image(I6D8R2).
    {
        uint16_t* disp_image = new uint16_t[Image_Width * Image_Height] ;
        memset( disp_image, 0x00, Image_Width * Image_Height*sizeof(uint16_t)) ;
        
        for( uint32_t y = 0 ; y <= roi.end.y ; ++y) {
            for( uint32_t x = 0; x < Image_Width; ++x) {
                uint16_t disparity = (uint16_t)randomUint(0, (1u << (6 + 8)) -1) ;
                uint16_t rel = (uint16_t)randomUint(1, (1u << 2) -1) ;
                uint16_t pxlVal = ( disparity << 2) + rel ;

                disp_image[ x + y * Image_Width] = pxlVal  ;

                if( isIncludedInROI( x, y, roi) && (rel == 3u)) {
                    cmp_histgram[disparity >> (6 + 8 - 8)] += 1 ; // 256 bins
                }
            }
        }

        fstream disp_img_fstrm( test_disp_img_path.c_str(), ios::out | ios::binary) ;

        disp_img_fstrm.write( (char*)disp_image, Image_Width * Image_Height * sizeof(uint16_t)) ;

        delete[] disp_image ;
    }

    ImageProcessCond img_cond =
        {
            string("I6D8R2"),
            Image_Width,
            Image_Height,
            roi,
            3,
            30,
            Image_Height - 2
        } ;

    // save the disparity image. 
    embedHistogramDisparityImage( test_disp_img_path, hist_embeded_img_path, img_cond, true) ;

    // load the disparity image. 
    fstream hist_embedded_img_fstrm( hist_embeded_img_path.c_str(), ios::in | ios::binary) ;

    hist_embedded_img_fstrm.seekg( ( Embed_Histogram_Col + Image_Width * img_cond.hist_embedded_line) * sizeof(uint16_t)) ;

    // read the embedded histogram from the image file. 
    uint16_t embedded_hist[0x100*2] ;
    hist_embedded_img_fstrm.read( (char*)embedded_hist, sizeof(embedded_hist));

    // compare the histograms.
    for( uint32_t val = 0 ; val < 0x100 ; ++val) {
        uint32_t cmp_hist_bin_freq = cmp_histgram[val] >> ( 21 - 16) ;
        uint32_t embedded_hist_bin_freq = (embedded_hist[2*val] << 8) + embedded_hist[2*val + 1] ;
        if( cmp_hist_bin_freq != embedded_hist_bin_freq) {
            cout << "val: " << val << " " << cmp_hist_bin_freq  << " != " << embedded_hist_bin_freq << endl ;
            return false ;
        }
    }

    return true ;
}

// utility functions.
static uint16_t
randomUint( const uint16_t& min, const uint16_t& max)
{
    assert( min < max) ;
    assert( max < std::numeric_limits<uint16_t>::max());

    return  (uint16_t)( min + (uint16_t)(rand() * ( max - min + 1.) / ( 1. + RAND_MAX))) ;
}

static bool
isIncludedInROI( const uint32_t& x, const uint32_t& y, const ROI& roi)
{
    if( (roi.begin.x <= x) && ( x <= roi.end.x) &&
        (roi.begin.y <= y) && ( y <= roi.end.y)) {
        return true ;
    } else {
        return false ;
    }
}
