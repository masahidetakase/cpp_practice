#pragma once
#include <string>
#include "RawImage.hpp"
#include "Histogram.hpp"


/* Constants ------------------------------------------------*/
#define DEF_EXT_STR_CONST  extern const std::string
#define DEF_EXT_INT_CONST  extern const int
#define DEF_EXT_UINT_CONST extern const unsigned int
#define DEF_EXT_DBL_CONST  extern const double
#define DEF_EXT_FLT_CONST  extern const float
#define DEF_EXT_BOOL_CONST extern const bool

namespace raw_img_formats
{

DEF_EXT_STR_CONST Luminance8bpp ;
DEF_EXT_STR_CONST Edge16bpp ;
DEF_EXT_STR_CONST Int6Dec5Rel5 ;
DEF_EXT_STR_CONST Int6Dec8Rel2 ;
DEF_EXT_STR_CONST Int7Dec7Rel2 ;

} // the end of raw_img_formats namespace.


namespace raw_img_geometry
{

DEF_EXT_UINT_CONST Def_Width ;
DEF_EXT_UINT_CONST Def_Height ;
DEF_EXT_UINT_CONST Def_Top_Line ;
DEF_EXT_UINT_CONST Def_Bottom_Line ;
DEF_EXT_UINT_CONST Def_Left_Column ;
DEF_EXT_UINT_CONST Def_Right_Column ;

} // the end of raw_img_geometry


namespace image_process
{

class ImageHistogramCreator
{
    public:
        ImageHistogramCreator( const std::string& img_format) ;

        virtual ~ImageHistogramCreator() {}

        Histogram*
        create( const RawImage* raw_image, const ROI& roi, const std::uint16_t& rel_bits = 0) ;

    private:
        // prohibition against cloning.
        ImageHistogramCreator( const ImageHistogramCreator& src) ;
        const ImageHistogramCreator& operator= ( const ImageHistogramCreator& rhs) ;

        void
        init( const std::string& img_format) ;

        std::uint16_t m_upper_limit ;
        std::uint16_t m_bin_width ;


        // Functions for Pixel value -> bin value convertion
        std::uint16_t (*m_extract_val_func)( const std::uint16_t& pxl_val) ;

        static std::uint16_t
        extractRawVal( const std::uint16_t& pxl_val) ; // no treatment.
        
        static std::uint16_t
        extractI6D5R5( const std::uint16_t& pxl_val) ;

        static std::uint16_t
        extractI6D8R2( const std::uint16_t& pxl_val) ;

        static std::uint16_t
        extractI7D7R2( const std::uint16_t& pxl_val) ;
} ;








} // the end of image_process namespace.