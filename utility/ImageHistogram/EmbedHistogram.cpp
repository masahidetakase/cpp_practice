#include "EmbedHistogram.hpp"
#include "ImageHistogramCreator.hpp"
#include "EdgeImageCreator.hpp"
#include <memory>
#include <fstream>
#include <algorithm>

using namespace std ;
using namespace image_process ;

/* local functions *************************************************/
static string
makeDebugFilePath( const string& input_img_path,
                   const string& extention) ;

static void
convRaisedEdge2AbsEdge( RawImage* raised_edge_img) ;

template<typename T>
class _scoped_ptr
{
    public:
        typedef T* pointer_type ; 

        _scoped_ptr( const pointer_type& ptr) : m_ptr( ptr) {}

        virtual ~_scoped_ptr() { delete m_ptr ;}

        const pointer_type&
        operator->() const { return m_ptr ;}

        const pointer_type&
        get() const { return m_ptr ;}

        T&
        operator* () const { return *m_ptr ;}

    private:
        // prohibition against cloning.
        _scoped_ptr( const _scoped_ptr&) ;
        const _scoped_ptr& operator= ( const _scoped_ptr&) ;

        pointer_type m_ptr ;
} ;


/* Constans ********************************************************/
// The position of embedded histogram data in raw images. 
static const uint32_t Embedde_Histogram_Col_pxl = 640 ;

bool 
embedHistogramLumImage( const std::string& input_img_path,
                        const std::string& output_img_path,
                        const ImageProcessCond& cond,
                        const bool& do_save_debug_infos)
{
    // load luminance image.
    RawImage input_raw_img( cond.img_width,
                            cond.img_height,
                            RawImage::Pxl8bpp,
                            input_img_path) ;
    // create output image.
    _scoped_ptr<RawImage> output_raw_img( input_raw_img.clone()) ;

    // create edge image.
    EdgeImageCreator edge_img_creator( cond.r_tex_l_th) ;
    _scoped_ptr<RawImage> edge_img( edge_img_creator.create( &input_raw_img, cond.roi)) ;

    // create histograms.
    ImageHistogramCreator lum_hist_creator( raw_img_formats::Luminance8bpp) ;
    _scoped_ptr<Histogram> lum_histogram( lum_hist_creator.create( &input_raw_img, cond.roi)) ;

    ImageHistogramCreator edge_hist_creator( raw_img_formats::Luminance8bpp) ;

    // shrink ROI for edge.
    ROI edge_img_roi( cond.roi) ;
    edge_img_roi.begin.x += 2 ;
    edge_img_roi.end.x -= 2 ;

    // create the edge histogram.
    _scoped_ptr<Histogram> edge_histogram( edge_hist_creator.create( edge_img.get(), edge_img_roi)) ;

    if( ( output_raw_img.get() == NULL) ||
        ( edge_img.get() == NULL) ||
        ( lum_histogram.get() == NULL) ||
        ( edge_histogram.get() == NULL)) {
        return false ;
    }

    uint16_t num_bins = (uint16_t)lum_histogram->getNumBins() ;
    assert( num_bins == 256) ;

    for( uint16_t bin_idx = 0 ; bin_idx < num_bins ; ++bin_idx) {
        uint32_t lum_frequency = lum_histogram->getBin( bin_idx).frequency ;
        uint32_t edge_frequency = edge_histogram->getBin( bin_idx).frequency ;

        // frequencies are represented as 21 bit unsigned integers (1280*960=1228800 < 2^21=2097152).
        // Higher 16 bits of them are stored in embedded lines.
        lum_frequency >>= ( 21 - 16) ;
        edge_frequency >>= ( 21 - 16) ;

        // luminance bin
        output_raw_img->setPxl( Embedde_Histogram_Col_pxl + bin_idx * 2,
                                cond.hist_embedded_line,
                                lum_frequency >> 8) ;

        output_raw_img->setPxl( Embedde_Histogram_Col_pxl + bin_idx * 2 + 1,
                                cond.hist_embedded_line,
                                lum_frequency & 0x00FF) ;
        // edge bin
        output_raw_img->setPxl( Embedde_Histogram_Col_pxl + bin_idx * 2,
                                cond.hist_embedded_line + 1,
                                edge_frequency >> 8) ;

        output_raw_img->setPxl( Embedde_Histogram_Col_pxl + bin_idx * 2 + 1,
                                cond.hist_embedded_line + 1,
                                edge_frequency & 0x00FF) ;
    }

    // save debug informations.
    if( do_save_debug_infos) {
        string edge_img_path = makeDebugFilePath( output_img_path, "edge.raw") ;
        string lum_hist_path = makeDebugFilePath( output_img_path, "lum_hist.csv") ;
        string edge_hist_path = makeDebugFilePath( output_img_path, "edge_hist.csv") ;
        
        // save the edge image.
        convRaisedEdge2AbsEdge( edge_img.get()) ;
        edge_img->save( edge_img_path) ;

        // save the luminance histogram.
        ofstream lum_hist_strm( lum_hist_path.c_str()) ;
        lum_hist_strm << *lum_histogram ;

        // save the edge histogram.
        ofstream edge_hist_strm( edge_hist_path.c_str()) ;
        edge_hist_strm << "# Bin value is raised by " << EdgeImageCreator::Edge_Offset << "." << endl ;
        edge_hist_strm << *edge_histogram ;
    }

    // Save the luminance image with the histogram data.
    return output_raw_img->save( output_img_path) ;
}


bool 
embedHistogramDisparityImage( const std::string& input_img_path,
                              const std::string& output_img_path,
                              const ImageProcessCond& cond,
                              const bool& do_save_debug_infos)
{
    //preconditions
    assert( ( cond.img_format == raw_img_formats::Int6Dec5Rel5) ||
            ( cond.img_format == raw_img_formats::Int6Dec8Rel2) ||
            ( cond.img_format == raw_img_formats::Int7Dec7Rel2)) ;

    // load disparity image.
    RawImage input_raw_img( cond.img_width,
                            cond.img_height,
                            RawImage::Pxl16bpp_LSB,
                            input_img_path) ;

    // create output image.
    _scoped_ptr<RawImage> output_raw_img( input_raw_img.clone()) ;

    // create disparity histogram.
    ImageHistogramCreator disp_hist_creator( cond.img_format) ;

    _scoped_ptr<Histogram> disp_histogram( disp_hist_creator.create( &input_raw_img, cond.roi, cond.rel_bits)) ;

    if( ( output_raw_img.get() == NULL) ||
        ( disp_histogram.get() == NULL)) {
        return false ;
    }

    uint16_t num_bins = (uint16_t)disp_histogram->getNumBins() ;
    assert( num_bins == 256) ;

    for( uint16_t bin_idx = 0 ; bin_idx < num_bins ; ++bin_idx) {
        uint32_t disp_frequency = disp_histogram->getBin( bin_idx).frequency ;

        // frequencies are represented as 11 bit unsigned integers (1280*960=1228800 < 2^11=2097152).
        // Higher 16 bits of them are stored in embedded lines.
        disp_frequency >>= ( 21 - 16) ;

        output_raw_img->setPxl( Embedde_Histogram_Col_pxl + bin_idx * 2,
                                cond.hist_embedded_line,
                                (uint16_t)( disp_frequency >> 8)) ;

        output_raw_img->setPxl( Embedde_Histogram_Col_pxl + bin_idx * 2 + 1,
                                cond.hist_embedded_line,
                                (uint16_t)( disp_frequency & 0x00FF)) ;
    }

    // save debug informations.
    if( do_save_debug_infos) {
        string disp_hist_path = makeDebugFilePath( output_img_path, "disp_hist.csv") ;
        
        // save the edge histogram.
        ofstream disp_hist_strm( disp_hist_path.c_str()) ;
        disp_hist_strm << *disp_histogram ;
    }

    return output_raw_img->save( output_img_path) ;
}


/* local functions *********************************************************/
static string
makeDebugFilePath( const string& input_img_path, const string& extention)
{
    // preconditions
    assert( !input_img_path.empty() && 0 < input_img_path.length()) ;

    string dot_extention( extention) ;

    size_t ext_pos = input_img_path.find_last_of( ".") ;
    if( ext_pos == string::npos) {
        ext_pos = input_img_path.length() ;
    }

    if( string::npos == dot_extention.find(".")) {
        dot_extention.insert( 0, ".") ;
    }

    return input_img_path.substr( 0, ext_pos) + "-DEBUG-" + extention ;
}

static void
convRaisedEdge2AbsEdge( RawImage* raised_edge_img)
{
    // preconditions
    assert( raised_edge_img != NULL) ;

    for( uint32_t y = 0; y < raised_edge_img->getHeight() ; ++y) {
        for( uint32_t x = 0; x < raised_edge_img->getWidth() ; ++x) {
            uint8_t raised_edge = raised_edge_img->getPxl( x, y) ;
            uint8_t abs_edge = std::abs( raised_edge - EdgeImageCreator::Edge_Offset) ;
            raised_edge_img->setPxl( x, y, abs_edge) ;
        }
    }
}