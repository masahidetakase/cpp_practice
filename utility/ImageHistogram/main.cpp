#include <iostream>
#include <sstream>
#include <vector>
#include <utility>
#include <algorithm>
#include "CmdlineArgument.hpp"
#include "ImageHistogramCreator.hpp"
#include "EmbedHistogram.hpp"

using namespace my_utils ;
using namespace std ;

/* constants------------------------------------------------------- */
static const string Version_No = "0.13" ;

/* local functions ------------------------------------------------ */
static CmdlineArgParser::result_type
parseCmdline( int argc, char* argv[]) ;

static bool
checkOptionVals( const CmdlineArgParser::result_type& opt_vals) ;

static void
displayUsage() ;

static void
displayImageProcCondition( const ImageProcessCond& cond) ;


static ImageProcessCond
cmdlParseResultToImgProsessCond( CmdlineArgParser::result_type& result) ;


int main( int argc, char* argv[])
{
    // Parse command line options.
    CmdlineArgParser::result_type options = parseCmdline( argc, argv) ;

    // check the options.
    if( options.empty() || !checkOptionVals( options)) {
        return -1 ;
    }

    // get option values.
    string input_img_path = options["<1>"].get<string>() ;
    string output_img_path = options["-output"].get<string>() ;
    ImageProcessCond condition = cmdlParseResultToImgProsessCond( options) ;
    bool do_save_debug_info = options["-debug"].get<bool>() ; 

    // verbose mode.
    if( options["-v"].get<bool>()) {
        displayImageProcCondition( condition) ;
    }

    bool is_success = true ;

    // Embed the histogram in the Raw image.
    if( condition.img_format == raw_img_formats::Luminance8bpp) {
        // Luminance image
        is_success = embedHistogramLumImage( input_img_path, output_img_path, condition, do_save_debug_info) ;
    } else {
        // disparity image
        is_success = embedHistogramDisparityImage( input_img_path, output_img_path, condition, do_save_debug_info) ;
    }

    if( !is_success) {
        cerr << "Error: Please check the options." << endl ;
    }

    return 0 ;
}


/* implementation of local functions -------------------------------------------------*/

static CmdlineArgParser::result_type
parseCmdline( int argc, char* argv[])
{
    CmdlineArgParser cmdlineParser ;

    cmdlineParser.registerOpt( "-h",          false) ;
    cmdlineParser.registerOpt( "-v",          false) ;
    cmdlineParser.registerOpt( "--help",      false) ;
    cmdlineParser.registerOpt( "-fmt",        raw_img_formats::Luminance8bpp);
    cmdlineParser.registerOpt( "-width",      (int)raw_img_geometry::Def_Width) ;
    cmdlineParser.registerOpt( "-height",     (int)raw_img_geometry::Def_Height) ;
    cmdlineParser.registerOpt( "-tline",      (int)raw_img_geometry::Def_Top_Line) ;
    cmdlineParser.registerOpt( "-bline",      (int)raw_img_geometry::Def_Bottom_Line) ;
    cmdlineParser.registerOpt( "-lcol",       (int)raw_img_geometry::Def_Left_Column) ;
    cmdlineParser.registerOpt( "-rcol",       (int)raw_img_geometry::Def_Right_Column) ;
    cmdlineParser.registerOpt( "-relbit",     0) ;
    cmdlineParser.registerOpt( "-r-tex-l-th", 30) ;
    cmdlineParser.registerOpt( "-hist-line",  -1) ;
    cmdlineParser.registerOpt( "-output",     "") ;
    cmdlineParser.registerOpt( "-debug",      false) ;

    size_t num_single_vals = 0 ;
    CmdlineArgParser::result_type result = cmdlineParser.parse( argc, argv, &num_single_vals) ;

    CmdlineArgParser::error_list_type errors = cmdlineParser.getLastErrors() ;

    if( result["-h"].get<bool>() || result["--help"].get<bool>()) {
        displayUsage() ;
        result.clear() ;
        return result ;
    }

    if( !errors.empty()) {
        for( CmdlineArgParser::error_list_type::iterator iter = errors.begin() ;
             iter != errors.end() ;
             ++iter) {
            cerr << "ERROR: " << *iter << endl ;
        }
        result.clear() ;
    }

    if( num_single_vals < 2) {
        cerr << "ERROR: input file not found." << endl ;
        result.clear() ;
    }

    return result ;
}


static bool
checkOptionVals( const CmdlineArgParser::result_type& opt_vals)
{
    static string format_list[] =
        { raw_img_formats::Luminance8bpp,
          raw_img_formats::Int6Dec5Rel5,
          raw_img_formats::Int6Dec8Rel2,
          raw_img_formats::Int7Dec7Rel2 } ;

    if( opt_vals.empty()) {
        return false ;
    }

    size_t num_formats = sizeof(format_list) / sizeof(format_list[0]) ;

    string img_format = opt_vals.at("-fmt").get<string>() ;
    string output_file_path = opt_vals.at("-output").get<string>() ;
    string input_file_path = opt_vals.at("<1>").get<string>() ;
    int img_width = opt_vals.at("-width").get<int>() ;
    int img_height = opt_vals.at("-height").get<int>() ;
    int top_line = opt_vals.at("-tline").get<int>() ;
    int bottom_line = opt_vals.at("-bline").get<int>() ;
    int left_col = opt_vals.at("-lcol").get<int>() ;
    int right_col = opt_vals.at("-rcol").get<int>() ;
    int r_tex_l_th = opt_vals.at("-r-tex-l-th").get<int>() ;
    int relbit = opt_vals.at("-relbit").get<int>() ;
    int hist_line = opt_vals.at("-hist-line").get<int>() ;

    stringstream err_message_strm ;

    // "-fmt"
    string* format_ptr =
        find( format_list,
              format_list + num_formats,
              img_format) ;

    if( format_ptr == format_list + num_formats) {
        err_message_strm << "ERROR: unknown image format." << endl ;
    }

    // "-output"
    if( output_file_path.size() == 0) {
        err_message_strm << "ERROR: output file path not found." << endl ;
    }

    // input raw file path
    if( input_file_path.size() == 0) {
        err_message_strm << "ERROR: input file path not found." << endl ;
    }

    // "-width"
    if( img_width < 640 + 512) {
        err_message_strm << "ERROR: invalid image width" << endl ;
    }

    // "-height"
    if( img_height <= 0) {
        err_message_strm << "ERROR: invalid image height" << endl ;
    }

    // "-tline"
    if( ( top_line < 0) ||
        ( img_height <= top_line)) {
        err_message_strm << "ERROR: invalid top line" << endl ;
    }

    // "-bline"
    if( bottom_line < top_line) {
        err_message_strm << "ERROR: invalid bottom line" << endl ;
    } else if( img_height - 2 <= bottom_line) {
        err_message_strm << "ERROR: no space for emdedding the histograms." << endl ;
    }

    // "-lcol"
    if( ( left_col < 0) ||
        ( img_width <= left_col -2)) {
        err_message_strm << "ERROR: invalid left column" << endl ;
    }

    // "-rcol"
    if( ( right_col < left_col) ||
        ( img_width <= right_col)) {
        err_message_strm << "ERROR: invalid right column" << endl ;
    }

    // "-r-tex-l-th"
    if( ( r_tex_l_th < 0) ||
        ( 127 < r_tex_l_th)) {
        err_message_strm << "ERROR: invalid right column" << endl ;
    }

    if( ( relbit < 0) || ( 0xFFFF < relbit)) {
        err_message_strm << "ERROR: invalid reliability bits [0,65535]." << endl ;
    }

    if( hist_line >= 0) { 
        if( ( hist_line <= bottom_line) || ( img_height - 2 < hist_line)) {
            err_message_strm << "ERROR: invalid histogram position." << endl ;
        }
    }

    if( 0 < err_message_strm.tellp()) {
        cout << err_message_strm.str() ;
        return false ;
    }

    return true ;
}


static void
displayImageProcCondition( const ImageProcessCond& cond)
{
    cout <<
        endl << 
        "Image process conditions" << endl <<
        "  Format     : " << cond.img_format << endl <<
        "  Image size : " << cond.img_width << " x " << cond.img_height << endl <<
        "  ROI        : " << cond.roi << endl << 
        "  r-tex-l-th : " << (uint32_t)cond.r_tex_l_th << endl <<
        "  relbit     : " << cond.rel_bits << endl <<
        "  hist-line  : " << cond.hist_embedded_line << endl <<
        "  ----------------------------" << endl ;
}

static void
displayUsage()
{
    const char* usage_lines[] =
        {
            "",
            "Usage: MakeHistogram [options...] -output output_file input_file",
            "",
            "[options]",
            "  -fmt        type of intput raw image file",
            "                  LUM8:   8 bit luminance image(Default).",
            "                  I6D5R5: standard",
            "                  I6D8R2: high-end",
            "                  I7D7R2: ?",
            "",
            "  -v          show the details.",
            "",
            "  -width      width of the whole input raw image. Default: 1280.",
            "",
            "  -height     height of the whole input raw image. Default: 960.",
            "",
            "  -tline      index of the top line of ROI (zero-based). Default: 0.",
            "",
            "  -bline      index of the bottom line of ROI (zero-based). Default: 959.",
            "",
            "  -lcol       index of the left column of ROI (zero-based). Default: 0.",
            "",
            "  -rcol       index of the right column of ROI (zero-based). Default: 1279.",
            "",
            "  -relbit     reliability bits. Zero means that all pixels in ROI are counted. Default : 0.",
            "",
            "  -r-tex-l-th register value. Default: 30.",
            "",
            "  -hist-line  index of the line where the histogram is embedded. Default: '-bline' + 1.",
            "",
            "  -output     path of the output raw image file.",
            "",
            "  -debug      save the debug informations: the edge image and the csv files of the histograms.",
            "",
            "  -h,--help   display this usage.",
            "",
            NULL
        } ;

    // Version
    cout << endl << "Version: " << Version_No << endl ;

    for( size_t line_idx = 0 ; usage_lines[line_idx] != NULL ; ++line_idx) {
        cout << usage_lines[line_idx] << endl ;
    }
}


static ImageProcessCond
cmdlParseResultToImgProsessCond( CmdlineArgParser::result_type& result)
{
    ImageProcessCond condition ;

    condition.img_format = result["-fmt"].get<string>() ;
    
    condition.img_width = (uint32_t)result["-width"].get<int>() ;
    
    condition.img_height = (uint32_t)result["-height"].get<int>() ;
    
    condition.roi.begin.x = (uint32_t)result["-lcol"].get<int>() ;
    condition.roi.begin.y = (uint32_t)result["-tline"].get<int>() ;
    condition.roi.end.x = (uint32_t)result["-rcol"].get<int>() ;
    condition.roi.end.y = (uint32_t)result["-bline"].get<int>() ;

    condition.rel_bits = (uint16_t)result["-relbit"].get<int>() ;

    condition.r_tex_l_th = (uint8_t)result["-r-tex-l-th"].get<int>() ;

    int32_t hist_line = result["-hist-line"].get<int>() ;
    if( hist_line < 0) {
        condition.hist_embedded_line = (uint32_t)condition.roi.end.y + 1 ;
    } else {
        condition.hist_embedded_line = (uint32_t)hist_line ;
    }

    return condition ;
}
