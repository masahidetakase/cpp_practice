#include "CmdlineArgument.hpp"
#include <stdexcept>
#include <algorithm>
#include <functional>
#include <cctype>
#include <sstream>
#include <iomanip>
#include <limits>
#include <utility>
#include <cassert>
#include "mycstdint.hpp"

static std::string
trimString( const std::string& str) ;


namespace my_utils
{

using namespace std ;


ArgValue
ArgValue::makeValueFromString( const std::string& str)
{
    string val_str = trimString( str) ;
    const size_t val_str_len = val_str.length() ;
    istringstream val_str_strm( val_str) ;

    if( val_str_len == 0) {
        return ArgValue() ;
    }

    // integer ?
    {
        int int_val = 0 ;
        val_str_strm >> std::dec >> int_val ;

        if( val_str_strm.eof()) {
            return ArgValue( int_val) ;
        } else {
            val_str_strm.clear() ;
            val_str_strm.seekg( 0, ios_base::beg) ;
        }
    }

    // double ?
    {
        double dbl_val = 0. ;
        val_str_strm >> std::dec >> dbl_val ;
        if( val_str_strm.eof()) {
            return ArgValue( dbl_val) ;
        } else {
            val_str_strm.clear() ;
            val_str_strm.seekg( 0, ios_base::beg) ;
        }
    }

    // unsigned hexadecimal
    {
        uint32_t uint_hex = 0x00 ;
        val_str_strm >> std::hex >> uint_hex ;
        if( val_str_strm.eof()) {
            return ArgValue( (uint32_t)uint_hex) ;
        } else {
            val_str_strm.clear() ;
            val_str_strm.seekg( 0, ios_base::beg) ;
        }
    }

    return ArgValue( val_str) ;
}


ArgValue::ArgValue( const string& str_val)
    : m_val_type( string_type),
      m_str_val( str_val) {}


ArgValue::ArgValue( const int& int_val)
    : m_val_type( integer_type),
      m_int_val( int_val) {}


ArgValue::ArgValue( const double& dbl_val)
    : m_val_type( double_type),
      m_dbl_val( dbl_val) {}


ArgValue::ArgValue( const unsigned int& uint_hex)
    : m_val_type( uhex_type),
      m_uint_val( uint_hex) {}

ArgValue::ArgValue( const bool& do_exist)
    : m_val_type( bool_type),
      m_bool_val( do_exist) {}


ArgValue::ArgValue()
    : m_val_type( ArgValue::invalid_type),
      m_str_val( ""),
      m_int_val( std::numeric_limits<int>::min()),
      m_dbl_val( -std::numeric_limits<double>::max()),
      m_uint_val( std::numeric_limits<unsigned int>::max()) {}


ArgValue::ArgValue( const ArgValue& src)
    : m_val_type( src.m_val_type)
{
    if( m_val_type == string_type) {
        m_str_val = src.m_str_val ;
    } else if( m_val_type == integer_type) {
        m_int_val = src.m_int_val ;
    } else if( m_val_type == double_type) {
        m_dbl_val = src.m_dbl_val ;
    } else if( m_val_type == uhex_type) {
        m_uint_val = src.m_uint_val ;
    } else if( m_val_type == bool_type) {
        m_bool_val = src.m_bool_val ;
    } else { 
        assert( m_val_type == ArgValue::invalid_type) ;
        
    }
}

void
ArgValue::swap( ArgValue& arg_val)
{
    std::swap( m_val_type, arg_val.m_val_type) ;
    std::swap( m_str_val, arg_val.m_str_val) ;
    std::swap( m_int_val, arg_val.m_int_val) ;
    std::swap( m_dbl_val, arg_val.m_dbl_val) ;
    std::swap( m_uint_val, arg_val.m_uint_val) ;
    std::swap( m_bool_val, arg_val.m_bool_val) ;
}


const ArgValue::Type&
ArgValue::getType() const
{
    return m_val_type ;
}


const ArgValue&
ArgValue::operator= ( const ArgValue& rhs)
{
    ArgValue tmp( rhs) ;

    this->swap( tmp) ;

    return *this ;
}


template<>
const string&
ArgValue::get<string>() const
{
    if( m_val_type != string_type) {
        throw std::domain_error( "type mismatch: Not string type") ;
    }

    return m_str_val ;
}


template<>
const int&
ArgValue::get<int>() const
{
    if( m_val_type != integer_type) {
        throw std::domain_error( "type mismatch: Not integer type") ;
    }

    return m_int_val ;
}


template<>
const double&
ArgValue::get<double>() const
{
    if( m_val_type != double_type) {
        throw std::domain_error( "type mismatch: Not double type") ;
    }

    return m_dbl_val ;
}


template<>
const unsigned int&
ArgValue::get<unsigned int>() const
{
    if( m_val_type != uhex_type) {
        throw std::domain_error( "type mismatch: Not unsigned integer type") ;
    }

    return m_uint_val ;
}


template<>
const bool&
ArgValue::get<bool>() const
{
    if( m_val_type != bool_type) {
        throw std::domain_error( "type mismatch: Not bool type") ;
    }

    return m_bool_val ;
}


ArgValue::operator std::string() const
{
    stringstream val_sstrm ;

    if( m_val_type == invalid_type) {
        val_sstrm << "ERROR: INVALID TYPE !!!" ;
    } else if( m_val_type == bool_type) {
        if( m_bool_val) {
            val_sstrm << "TRUE" ;
        } else {
            val_sstrm << "FALSE" ;
        }
    } else if( m_val_type == string_type) {
        val_sstrm << m_str_val ;
    } else if( m_val_type == integer_type) {
        val_sstrm << m_int_val ;
    } else if( m_val_type == double_type) {
        val_sstrm << std::setprecision(5) << m_dbl_val ;
    } else if( m_val_type == uhex_type) {
        val_sstrm << std::hex << m_uint_val ;
    } else {
        assert( false) ;
    }

    return val_sstrm.str() ;
}



CmdlineArgParser::CmdlineArgParser() {}


CmdlineArgParser::CmdlineArgParser( const CmdlineArgParser& src)
    : m_registered_pairs( src.m_registered_pairs) {}


const CmdlineArgParser&
CmdlineArgParser::operator= ( const CmdlineArgParser& rhs)
{
    CmdlineArgParser tmp( rhs) ;

    this->swap( tmp) ;

    return *this ;
}


void
CmdlineArgParser::swap( CmdlineArgParser& cmdl_parser)
{
    std::swap( m_registered_pairs, cmdl_parser.m_registered_pairs) ;
}


bool
CmdlineArgParser::isOptionKey( const string& opt_str)
{
    size_t opt_len = opt_str.length() ;

    if( ( opt_str.empty()) ||
        ( opt_len < 2) ||
        ( opt_str.find( "-") != 0)) {
        return false ;
    }

    if( 2 <= opt_len) {
        if( isalpha( opt_str[1]) || // "-opt"
            ( ( 3 <= opt_len) && // "--opt"
              ( opt_str[1] == '-') &&
              isalpha( opt_str[2]))) {
            return true ;
        }
    }

    return false ;
}


ArgValue::Type
CmdlineArgParser::getOptionType( const string& opt_str)
{
    opt_val_pairs_type::iterator iter = m_registered_pairs.find( opt_str) ;

    if( iter != m_registered_pairs.end()) {
        return iter->second.getType() ;
    } else {
        return ArgValue::invalid_type ;
    }
}


template<typename T>
void
CmdlineArgParser::registerOpt( const string& opt_str, const T& default_val)
{
    string trimmed_opt_str = trimString( opt_str) ;
    if( isOptionKey( trimmed_opt_str)) {
        m_registered_pairs.insert( make_pair( trimmed_opt_str, ArgValue(default_val))) ;
    }
}


template void
CmdlineArgParser::registerOpt<bool>( const string& opt_str,
                                     const bool& default_val) ;

template void
CmdlineArgParser::registerOpt<int>( const string& opt_str,
                                    const int& default_val) ;

template void
CmdlineArgParser::registerOpt<double>( const string& opt_str,
                                       const double& default_val) ;

template void
CmdlineArgParser::registerOpt<unsigned int>( const string& opt_str,
                                             const unsigned int& default_val) ;

template void
CmdlineArgParser::registerOpt<string>( const string& opt_str,
                                       const string& default_val) ;


void
CmdlineArgParser::registerOpt( const string& opt_str, const char* default_val)
{
    string trimmed_opt_str = trimString( opt_str) ;

    if( isOptionKey( trimmed_opt_str)) {
        m_registered_pairs.insert( make_pair( opt_str, ArgValue( string( default_val)))) ;
    }
}


CmdlineArgParser::result_type
CmdlineArgParser::parse( int argc, char* argv[], size_t* num_single_vals)
{
    m_errors.clear() ;

    opt_val_pairs_type result( m_registered_pairs) ;
    string key_str ; // key string of key-value pairs. If it is not empty, the next arg. must be a value, which does not begin '-' or '--'.
    int single_val_idx = 0 ; // index of single values
    key_str.clear() ;

    for( int arg_idx = 0 ; arg_idx < argc ; ++arg_idx) {
        string arg_str = trimString( argv[arg_idx]) ;

        if( isOptionKey( arg_str)) {
            // expected "value" does not exist.
            if( !key_str.empty()) {
                m_errors.push_back( string( "Option: ") + key_str + " has no values.") ;
                key_str.clear() ;
            } else {
                ArgValue::Type opt_type = getOptionType( arg_str) ;

                if( opt_type == ArgValue::invalid_type) { // Not regitered option.
                    m_errors.push_back( string( "Option: ") + arg_str + " is a unknown option.") ;
                } else if( opt_type == ArgValue::bool_type) { // registered "single_opt" is treated as TRUE.
                    result[arg_str] = ArgValue( true) ;
                    key_str.clear() ;
                } else { // registered "key-value opt"
                    key_str = arg_str ;
                }
            }
        } else {
            if( key_str.empty()) { // this arg. must be a single_value.
                stringstream sstrm ;
                sstrm << "<" << single_val_idx++ << ">" ;
                result[sstrm.str()] = ArgValue(arg_str) ;
            } else {
                ArgValue val = ArgValue::makeValueFromString( arg_str) ;
                if( val.getType() == m_registered_pairs[key_str].getType()) {
                    result[key_str] = val ;
                } else {
                    m_errors.push_back( string( "Option: ") + key_str + " has a wrong value: " + arg_str) ;
                }

                key_str.clear() ;
            }
        }

        // When the last arg. is a "key-value opt",
        if( !key_str.empty() && ( arg_idx == argc - 1)) {
             m_errors.push_back( string( "Option: ") + key_str + " has no values.") ;
        }
    }

    if( num_single_vals != NULL) {
        *num_single_vals = single_val_idx ;
    }

    return result ;
}


void
CmdlineArgParser::reset()
{
    m_registered_pairs.clear() ;
    m_errors.clear() ;
}


const CmdlineArgParser::error_list_type&
CmdlineArgParser::getLastErrors() const
{
    return m_errors ;
}

} // the end of my_utils namespace


static std::string
trimString( const std::string& str)
{
    if( str.empty() || ( str.length() == 0)) {
        return "" ;
    }

    std::string::const_iterator start_iter =
        std::find_if( str.begin(),
                      str.end(),
                      std::not1( std::ptr_fun( isspace))) ;

    std::string::const_reverse_iterator end_riter =
        std::find_if( str.rbegin(),
                      str.rend(),
                      std::not1( std::ptr_fun( isspace))) ;

    return std::string( start_iter, end_riter.base()) ;
}
