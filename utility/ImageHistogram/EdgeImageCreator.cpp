#include "EdgeImageCreator.hpp"
#include <cassert>

namespace image_process
{

using namespace std ;

const int16_t EdgeImageCreator::Edge_Offset = 127 ; 

EdgeImageCreator::EdgeImageCreator( const std::uint16_t& r_tex_l_th)
    : m_r_tex_l_th( r_tex_l_th)
    {
        assert( m_r_tex_l_th <= 255) ;
    }


RawImage*
EdgeImageCreator::create( const RawImage* lum8bpp_image, const ROI& roi)
{
    if( ( lum8bpp_image == NULL) || ( lum8bpp_image->getBpp() != 1)) {
        return NULL ;
    }

    uint32_t img_width = lum8bpp_image->getWidth() ;
    uint32_t img_height= lum8bpp_image->getHeight() ;

    RawImage* edge_img = new RawImage( img_width,
                                       img_height,
                                       RawImage::Pxl8bpp) ;

    uint8_t neighbour_pxls[5] ;

    for( uint32_t y = roi.begin.y; y <= roi.end.y; ++y) {
        for( uint32_t x= roi.begin.x + 2 ; x <= roi.end.x - 2 ; ++x) {
            if( ( x < 2) || ( img_width - 3 < x)) {
                continue ;
            }

            neighbour_pxls[0] = (uint8_t)lum8bpp_image->getPxl( x - 2, y) ;
            neighbour_pxls[1] = (uint8_t)lum8bpp_image->getPxl( x - 1, y) ;
            neighbour_pxls[2] = (uint8_t)lum8bpp_image->getPxl( x, y) ; // target
            neighbour_pxls[3] = (uint8_t)lum8bpp_image->getPxl( x + 1, y) ;
            neighbour_pxls[4] = (uint8_t)lum8bpp_image->getPxl( x + 2, y) ;

            // -510 <= edge <= +510
            int16_t edge = calcEdge( neighbour_pxls + 2) ;

            int8_t signed_8bpp_edge = 0 ;

            signed_8bpp_edge = (int8_t)( edge >> 2) ; 

            // -127~+127 ---> 1~255
            uint8_t raised_edge = signed_8bpp_edge + Edge_Offset ;

            // write the raised edge in the 8bpp image. 
            edge_img->setPxl( x, y, raised_edge) ;
        }
    }

    return edge_img ;
}



} // the end of image_process namespace.
