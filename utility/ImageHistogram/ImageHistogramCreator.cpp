#include "ImageHistogramCreator.hpp"
#include <cassert>

#define DEF_STR_CONST  const std::string
#define DEF_INT_CONST  const int
#define DEF_UINT_CONST const unsigned int
#define DEF_DBL_CONST  const double
#define DEF_FLT_CONST  const float
#define DEF_BOOL_CONST const bool

namespace raw_img_formats {

DEF_STR_CONST Luminance8bpp = "LUM8" ;
DEF_STR_CONST Edge16bpp = "EDGE8" ;
DEF_STR_CONST Int6Dec5Rel5 = "I6D5R5" ;
DEF_STR_CONST Int6Dec8Rel2 = "I6D8R2" ;
DEF_STR_CONST Int7Dec7Rel2 = "I7D7R2" ;

} // the end of raw_img_formats namespace.

namespace raw_img_geometry {

DEF_UINT_CONST Def_Width = 1280 ;
DEF_UINT_CONST Def_Height = 960 ;
DEF_UINT_CONST Def_Top_Line = 0 ;
DEF_UINT_CONST Def_Bottom_Line = 957 ;
DEF_UINT_CONST Def_Left_Column = 0 ;
DEF_UINT_CONST Def_Right_Column = 1279 ;

} // the end of raw_img_geometry

namespace image_process
{

using namespace std ;

ImageHistogramCreator::ImageHistogramCreator( const string& img_format)
    : m_upper_limit( 0),
      m_bin_width( 0),
      m_extract_val_func( NULL)
{
    init( img_format) ;
}

Histogram*
ImageHistogramCreator::create( const RawImage* raw_image, const ROI& roi, const uint16_t& rel_bits)
{
    // preconditions
    assert( raw_image != NULL) ;

    // When ROI is not included in the image,
    if( ( raw_image->getWidth() <= roi.end.x) ||
        ( raw_image->getHeight() <= roi.end.y)) {
        return NULL ;
    }

    Histogram* histogram = new Histogram( 0, m_upper_limit, m_bin_width) ;
    assert( histogram != NULL) ;
    assert( histogram->getNumBins() == 256) ;

    uint16_t pxl ;

    for( uint32_t y = roi.begin.y ; y <= roi.end.y ; ++y) {
        for( uint32_t x = roi.begin.x ; x <= roi.end.x ; ++x) {
            pxl = raw_image->getPxl( x, y) ;

            if( ( pxl & rel_bits) == rel_bits) {
                histogram->pushValue( m_extract_val_func( pxl)) ;
            }
        }
    }

    return histogram ;
}


void
ImageHistogramCreator::init( const string& img_format)
{
    // preconditions
    assert( m_extract_val_func == NULL) ;

    // num. of bins must be 256. 
    if( img_format == raw_img_formats::Luminance8bpp) { // 8bpp
        m_upper_limit = 0xFF ;
        m_bin_width = 1 ;
        m_extract_val_func = extractRawVal ;
    } else if( img_format == raw_img_formats::Edge16bpp) { // 8bpp [-128 + 128, 127 + 128]
        m_upper_limit = 0xFF; 
        m_bin_width = 1 ;
        m_extract_val_func = extractRawVal ;
    } else if( img_format == raw_img_formats::Int6Dec5Rel5) { // I6D5R5
        m_upper_limit = (1 << ( 6 + 5)) - 1 ;
        m_bin_width = 1 << ( 6 + 5 - 8) ;
        m_extract_val_func = extractI6D5R5 ;
    } else if( img_format == raw_img_formats::Int6Dec8Rel2) { // I6D8R2
        m_upper_limit = ( 1 << ( 6 + 8)) - 1 ;
        m_bin_width = 1 << ( 6 + 8 - 8) ;
        m_extract_val_func = extractI6D8R2 ;
    } else if( img_format == raw_img_formats::Int7Dec7Rel2) { // I7D7R2
        m_upper_limit = ( 1 << ( 7 + 7)) - 1 ;
        m_bin_width = 1 << ( 7 + 7 - 8) ;
        m_extract_val_func = extractI7D7R2 ;
    } else {
        assert( false) ;
    }

    // post conditions
    assert( 0 < m_upper_limit) ;
    assert( m_extract_val_func != NULL) ;
    assert( ( m_upper_limit - 0 + 1) % m_bin_width == 0) ;
    assert( ( m_upper_limit - 0 + 1) / m_bin_width == 256) ;
}


std::uint16_t
ImageHistogramCreator::extractRawVal( const std::uint16_t& pxl_val)
{
    return pxl_val ;
}

std::uint16_t
ImageHistogramCreator::extractI6D5R5( const std::uint16_t& pxl_val)
{
    return pxl_val >> 5 ;
}

std::uint16_t
ImageHistogramCreator::extractI6D8R2( const std::uint16_t& pxl_val)
{
    return pxl_val >> 2 ;
}

std::uint16_t
ImageHistogramCreator::extractI7D7R2( const std::uint16_t& pxl_val)
{
    return pxl_val >> 2 ;
}

} // the end of image_process namespace.
