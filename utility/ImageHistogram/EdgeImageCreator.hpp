#pragma once
#include "RawImage.hpp"
#include "mycstdint.hpp"
#include <cmath>

namespace image_process
{

class EdgeImageCreator
{
    public:
        // true edge value(signed 16bit) = pixel value(unsigned 8bit) - Edge_Offset.
        static const int16_t Edge_Offset ;

        EdgeImageCreator( const std::uint16_t& r_tex_l_th) ;

        virtual ~EdgeImageCreator() {}

        RawImage*
        create( const RawImage* lum8bpp_image, const ROI& roi) ;

    private:
        //prohibition against cloning.
        EdgeImageCreator( const EdgeImageCreator& src) ;
        const EdgeImageCreator& operator= ( const EdgeImageCreator& rhs) ;

        const std::uint16_t m_r_tex_l_th ; // a register value.

        inline std::int16_t
        calcEdge( const std::uint8_t *pxl)
        {
            std::int32_t edge = (*( pxl - 2) + *( pxl - 1)) - ( *( pxl + 1) + *( pxl + 2)) ;

            if( ( (*( pxl - 2) + *( pxl - 1)) / 2 + ( *( pxl + 1) + *( pxl + 2)) / 2) / 2 < m_r_tex_l_th) {
                edge *= 2 ;
            }
            
            assert( ( -510 <= edge) && ( edge <= 510)) ;

            return (int16_t)edge ;
        }
} ;

} // the end of image_process namespace.
