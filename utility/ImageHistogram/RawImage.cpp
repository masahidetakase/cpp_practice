#include "RawImage.hpp"
#include <cassert>
#include <fstream>
#include <stdexcept>
#include <cstring>
#include <iostream>

namespace image_process
{

using namespace std ;

RawImage::RawImage( const uint32_t& width,
                    const uint32_t& height,
                    const RawImage::PxlFormat& pxl_format,
                    const string& raw_img_path)
    : m_width( width),
      m_height( height),
      m_num_pixels( width * height),
      m_pxl_format( pxl_format),
      m_byte_per_pxl( 0),
      m_image( NULL),
      m_get_pxl_func( NULL),
      m_set_pxl_func ( NULL)
{
    // preconditions
    assert( ( 0 < m_width) && ( 0 < m_height)) ;

    initPxlMethods( pxl_format) ;

    // create a image area. 
    m_image = new uint8_t[ m_num_pixels * m_byte_per_pxl] ;

    if( raw_img_path.length() == 0) {
        memset( m_image, 0, m_num_pixels * m_byte_per_pxl) ;
    } else {
        // Load a raw image file. 
        if( !loadRawImage( raw_img_path, pxl_format)) {
            delete[] m_image ;
            throw std::domain_error( "RawImage: Reading a file failed.") ;
        }
    }
}


RawImage::~RawImage()
{
    delete[] m_image ;
}


RawImage*
RawImage::clone() const
{
    RawImage* cloned_image = new RawImage( m_width, m_height, m_pxl_format) ;

    memcpy( cloned_image->m_image, m_image, m_num_pixels * m_byte_per_pxl) ;

    return cloned_image ;
}


bool
RawImage::save( const string& raw_img_path)
{
    streamsize img_size_byte = m_byte_per_pxl * m_num_pixels ;

    fstream img_stream( raw_img_path.c_str(), ios::out | ios::binary) ;
    
    if( !img_stream.is_open()) {
        return false ;
    }

    img_stream.write( (char*)m_image, img_size_byte) ;
    if( img_stream.fail()) {
        return false ;
    }

    return true ;
}


uint16_t
RawImage::getPxl( const uint32_t& x, const uint32_t& y) const 
{
    if( ( m_width <= x) || ( m_height <= y)) {
        throw std::out_of_range( "RawImage: out of image in getPxl()") ;
    }

    const uint8_t* pxl = m_image + ( y * m_width + x) * m_byte_per_pxl ;

    return m_get_pxl_func( pxl) ;
}


void
RawImage::setPxl( const uint32_t& x, const uint32_t& y, const uint16_t& value)
{
    if( ( m_width <= x) || ( m_height <= y)) {
        cout << " setPixel(" << x << "," << y << ")  " << "(" << m_width << "," << m_height <<")" << endl;
        throw std::out_of_range( "RawImage: out of image in setPxl()") ;
    }

    uint8_t* pxl = m_image + ( y * m_width + x) * m_byte_per_pxl ;
    m_set_pxl_func( pxl, value) ;
}


bool
RawImage::loadRawImage( const std::string& raw_img_path, const PxlFormat& pxl_format)
{
    // preconditions
    assert( 0 < raw_img_path.length()) ;
    assert( m_image != NULL) ;

    // Open a file.
    fstream img_strm( raw_img_path.c_str(), ios::in | ios::binary) ;
    if( !img_strm.is_open()) { return false ;}

    // Read an image from a file. 
    streamsize image_size_byte = m_byte_per_pxl * m_num_pixels ;
    streamsize read_len = (img_strm.read( (char*)m_image, image_size_byte)).gcount() ;
    
    if( read_len != image_size_byte) { return false ;}

    return true ;
}


void
RawImage::initPxlMethods( const RawImage::PxlFormat& pxl_format)
{
    // initialize
    if( pxl_format == Pxl8bpp) {
        m_byte_per_pxl = 1 ;
        m_get_pxl_func = getPxlValue8bpp ;
        m_set_pxl_func = setPxlValue8bpp ;

    } else if( pxl_format == Pxl16bpp_LSB) {
        m_byte_per_pxl = 2 ;
        m_get_pxl_func = getPxlValue16bppLSB ;
        m_set_pxl_func = setPxlValue16bppLSB ;
    } else if( pxl_format == Pxl16bpp_MSB) {
        m_byte_per_pxl = 2 ;
        m_get_pxl_func = getPxlValue16bppMSB ;
        m_set_pxl_func = setPxlValue16bppMSB ;
    } else {
        m_byte_per_pxl = 1 ;
        m_get_pxl_func = getPxlValue8bpp ;
        m_set_pxl_func = setPxlValue8bpp ;
    }
}

// Pixel-operation functions.
uint16_t
RawImage::getPxlValue8bpp( const uint8_t* pxl)
{
    return (uint16_t)*pxl ;
}


uint16_t
RawImage::getPxlValue16bppLSB( const uint8_t* pxl)
{
    return *(uint16_t*)pxl ;
}

uint16_t
RawImage::getPxlValue16bppMSB( const uint8_t* pxl)
{
    return ((uint16_t)(*pxl) << 8) + *(pxl + 1) ;
}


void
RawImage::setPxlValue8bpp( uint8_t* const pxl, const uint16_t& value)
{
    *pxl = (uint8_t)(value & 0x00FF) ;
}


void
RawImage::setPxlValue16bppLSB( uint8_t* const pxl, const uint16_t& value)
{
    *(uint16_t*)pxl = value ;
}

void
RawImage::setPxlValue16bppMSB( uint8_t* const pxl, const uint16_t& value)
{
    *pxl = value & 0x00FF ;
    *( pxl + 1) = value >> 8 ; 
}

} // the end of image_process namespace.

std::ostream&
operator << ( std::ostream& ostrm, const image_process::ROI& roi)
{
    ostrm << 
        "(" <<
        roi.begin.x << "," << roi.begin.y <<
        ")-(" <<
        roi.end.x << "," << roi.end.y << 
        ")" ;

    return ostrm ;
}
