#include "Histogram.hpp"
#include <cassert>
#include <stdexcept>

namespace image_process
{

using namespace std ;

Histogram::Histogram( const uint16_t& lower_limit,
                      const uint16_t& upper_limit,
                      const uint16_t& bin_width)
    : m_num_bins( 0),
      m_bin_width( bin_width),
      m_num_samples( 0)
{
    //preconditions
    assert( lower_limit < upper_limit) ;
    assert( 0 < bin_width) ;

    // value-range width must be divided by bin width.
    if( ( upper_limit - lower_limit + 1) % m_bin_width != 0) {
        throw std::invalid_argument( "Histogram: inappropriate bin width") ;
    }

    m_num_bins = ( upper_limit - lower_limit + 1) / m_bin_width ;

    m_bins.reserve( m_num_bins) ; 

    for( size_t bin_idx = 0 ; bin_idx < m_num_bins ; ++bin_idx) {
        HistogramBin bin = { (int32_t)( lower_limit + m_bin_width * bin_idx), 0} ;
        m_bins.push_back( bin) ;
    }

    // post-consditions
    assert( 5 <= m_num_bins) ;
}


uint32_t
Histogram::pushValue( const uint16_t& val)
{
    size_t bin_idx = ( val - m_bins[0].lowest_val) / m_bin_width ;

    if(  m_num_bins <= bin_idx) {
        return (uint32_t)-1 ;
    }

    m_bins[ bin_idx].frequency += 1 ;
    ++m_num_samples ;

    return bin_idx ;
}


const HistogramBin&
Histogram::getBin( const uint16_t& bin_idx) const
{
    if( m_num_bins <= bin_idx) {
        throw std::out_of_range( "Histogram: Bin does not exist.") ;
    }

    return m_bins[ bin_idx] ;
}

} // the end of image_process namespace


// For output a histogram data to std::ostream.
std::ostream&
operator<< ( std::ostream & output_strm,
             const image_process::Histogram& histogram)
{
    static const char* ASCII_LF = "\x0A" ;

    size_t num_bins = histogram.getNumBins() ;

    // header
    output_strm << "Lowest_value, frequency" << ASCII_LF ;

    // bin_value, frequency
    image_process::HistogramBin cur_bin ;
    for( size_t bin_idx = 0 ; bin_idx < num_bins ; ++bin_idx) {
        cur_bin = histogram.getBin( bin_idx) ;
        output_strm << cur_bin.lowest_val << "," << cur_bin.frequency << ASCII_LF ;
    }

    return output_strm ;
}
