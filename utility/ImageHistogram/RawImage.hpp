#pragma once
#include <string>
#include "mycstdint.hpp"
#include <cassert>
#include <ostream>

namespace image_process
{

class RawImage
{
    public:
        enum PxlFormat
        {
            Pxl8bpp,
            Pxl16bpp_LSB,
            Pxl16bpp_MSB
        } ;


        RawImage( const std::uint32_t& width,
                  const std::uint32_t& height,
                  const PxlFormat& pxl_format,
                  const std::string& raw_img_path = "") ;

        virtual ~RawImage() ;

        RawImage*
        clone() const ;

        bool
        save( const std::string& raw_img_path) ;

        std::uint16_t
        getPxl( const std::uint32_t& x, const std::uint32_t& y) const ;

        void
        setPxl( const std::uint32_t& x,
                const std::uint32_t& y,
                const std::uint16_t& value) ;

        std::uint32_t
        getWidth() const { return m_width ;}

        std::uint32_t
        getHeight() const { return m_height ;}

        std::uint32_t
        getBpp() const { return m_byte_per_pxl ;}

    private:
        RawImage( const RawImage& src) ;
        RawImage& operator= ( const RawImage& rhs) ;

        bool
        loadRawImage( const std::string& raw_img_path, const PxlFormat& pxl_format) ;

        void
        initPxlMethods( const PxlFormat& pxl_format) ;

        // Pixel operator functions.
        static std::uint16_t
        getPxlValue8bpp( const std::uint8_t* pxl) ;

        static std::uint16_t
        getPxlValue16bppLSB( const std::uint8_t* pxl) ;

        static std::uint16_t
        getPxlValue16bppMSB( const std::uint8_t* pxl) ;

        static void
        setPxlValue8bpp( std::uint8_t* const pxl, const std::uint16_t& value) ;

        static void 
        setPxlValue16bppLSB( std::uint8_t* const pxl, const std::uint16_t& value) ;

        static void 
        setPxlValue16bppMSB( std::uint8_t* const pxl, const std::uint16_t& value) ;

        const std::uint32_t m_width, m_height ;
        const std::uint32_t m_num_pixels ;
        const PxlFormat m_pxl_format ;

        std::uint32_t m_byte_per_pxl ;
        std::uint8_t* m_image ;

        // the pointer to the pixel operator functions.
        std::uint16_t (*m_get_pxl_func)( const std::uint8_t* pxl) ; // get pixel value.
        void (*m_set_pxl_func)( std::uint8_t* const pxl, const std::uint16_t& value) ; // set pixel value.;
} ;



// Region of Interest
struct ROI
{
    ROI() : begin(0, 0), end( 0, 0) {}

    ROI( const std::uint32_t& left,
         const std::uint32_t& top,
         const std::uint32_t& right,
         const std::uint32_t& bottom)
         : begin( left, top),
           end( right, bottom)
    {
        //preconditions
        assert( ( left <= right) && ( top <= bottom)) ;
    }

    struct Point{
        std::uint32_t x, y ;
        
        Point( const std::uint32_t& sx,
               const std::uint32_t& sy)
            : x(sx), y(sy) {}

    } begin, end ; // top-left and bottom-right point.
} ;

} // the end of image_process namespace.

std::ostream&
operator << ( std::ostream& ostrm, const image_process::ROI& roi) ;



