#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <assert.h>

#define SUBPIX_EXP_LOOP_NUM 30

static void
initFactorialTable( float* const factorials, const unsigned int len)
{
    // preconditions
    assert( factorials != NULL) ;
    assert( 0 < len) ;

    factorials[0] = 1.f ;
    {
        unsigned int n = 0 ;
        for( n = 1 ; n < len ; ++n) {
            factorials[n] = factorials[n -1] * n ;
        }
    }
}

typedef int INT ;
typedef float FLOAT ;

float
exponential( const float x)
{
    static float Factorial_Table[SUBPIX_EXP_LOOP_NUM] = { -1.} ; // 未初期化なら -1
    
    
    // 計算テーブルの初期化
    if( Factorial_Table[0] < 0.) {
        initFactorialTable( Factorial_Table, SUBPIX_EXP_LOOP_NUM) ;
    }

    float x_powered = 1.f ; // When n = 0
    float accumulated_terms = 1.f ; // When n = 0

    unsigned int n ;
    for( n = 1 ; n < SUBPIX_EXP_LOOP_NUM ; ++n) {
        x_powered *= x ;
        accumulated_terms += x_powered / Factorial_Table[n] ;
    }

    return accumulated_terms ;
}

FLOAT exp_m(
    FLOAT rv_x)                 /**<[in] 乗数 */
{
    INT at_i, at_k;
    FLOAT at_x, at_n1;
    FLOAT fRet = 0.0f;

    for(at_i = 1; at_i <= SUBPIX_EXP_LOOP_NUM; at_i++){
        at_x = 1;
        for(at_k = 0; at_k < at_i; at_k++){
            at_x *= rv_x;
        }
        at_n1 = 1.0f;
        for(at_k = 1; at_k <= at_i; at_k++){
            at_n1 *= (FLOAT)at_k;
        }
        fRet += at_x / at_n1;
    }
    fRet = fRet + 1.0f;
    return(fRet);
}

int
main( int argc, char* argv[])
{
    int i = 0 ;
    srand( time(NULL)) ;
    for( i = 0 ; i < 10 ; ++i) {
        float x = ( 2.f * rand()) / RAND_MAX ;

        printf( "x = %08f\n", x) ;
        printf( "exp() in math.h: %012e \n", exp( x)) ;
        printf( "exp_m()        : %012e \n", exp_m( x)) ;
        printf( "exponential()  : %012e \n", exponential( x)) ;
        puts( "\n") ;
    }

    return 0 ;
}