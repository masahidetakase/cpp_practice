#ifndef Modules_H_
#define Modules_H_
#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>

#ifdef __cplusplus
extern "C" {
#endif

#define TYPED_NULL(type) ((type*)NULL)

#define TYPED_MALLOC(type,n)  ((type*)malloc(sizeof(type)*(n)))

#define IDENTIFIER(str) (*(const uint32_t*)(str))

#define IS_VALID_INSTANCE( instance, ident_str) \
    ( ( (instance).identifier == IDENTIFIER((ident_str))) && \
      ( (instance).body != NULL))


// common parameter for all modules.
typedef struct tagCommonInitParam
{
    const char* str ;

    int32_t int_val ;

} CommonInitParam ;

// common argument for all modules.
typedef struct tagCommonArg
{
    int32_t int_param1 ;

    int32_t int_param2 ;

    double dbl_param ;

} CommonArg ;


// the instance of the modules.
typedef struct tagModuleInst
{
    uint32_t identifier ; // unique to each module.

    void* body ; // the instance of each modules.
} ModuleInst ;

// type of the constructor for the instance of the modules.
typedef ModuleInst (*ModuleCreateInst)( const CommonInitParam*) ;

// type of the destructor for the instance of the modules.
typedef void (*ModuleDispose)( const ModuleInst) ;

// type of the methos for the instance of the modules.
typedef void (*ModuleRun)( const ModuleInst, const CommonArg*) ;


// Module interface.
typedef struct tagModuleIF
{
    ModuleCreateInst constructor ; // create an instance.

    ModuleDispose destructor ; // dispose an instance.

    ModuleRun run ; // activate instance.

}  ModuleIF ;

// the list of the modules.
extern const ModuleIF ModuleList[] ;

uint32_t
getNumModules() ;

int32_t
isEndOfModuleList( const ModuleIF* module) ;

#ifdef __cplusplus
}
#endif // __cplusplus

#endif // Modules_H_
