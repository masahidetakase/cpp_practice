#ifndef ModuleABC_H_
#define ModuleABC_H_

#include "Modules.h"

// Module A ------------------------------------------

// the constructor for the instance of the moduleA.
ModuleInst
ModuleACreateInst( const CommonInitParam* init_param) ;

// the destructor for the instance of the moduleA.
void
ModuleADispose( const ModuleInst instance) ;

// the constructor for the instance of the moduleA.
void
ModuleARun( const ModuleInst, const CommonArg*) ;


// Module B ------------------------------------------

// the constructor for the instance of the moduleB.
ModuleInst
ModuleBCreateInst( const CommonInitParam* init_param) ;

// the destructor for the instance of the moduleB.
void
ModuleBDispose( const ModuleInst instance) ;

// the constructor for the instance of the moduleB.
void
ModuleBRun( const ModuleInst, const CommonArg*) ;


// Module C ------------------------------------------

// the constructor for the instance of the moduleC.
ModuleInst
ModuleCCreateInst( const CommonInitParam* init_param) ;

// the destructor for the instance of the moduleC.
void
ModuleCDispose( const ModuleInst instance) ;

// the constructor for the instance of the moduleC.
void
ModuleCRun( const ModuleInst, const CommonArg*) ;


#endif
