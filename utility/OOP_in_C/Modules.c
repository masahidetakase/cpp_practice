#include "Modules.h"
#include "ModuleABC.h"
#include <assert.h>

const ModuleIF ModuleList[] =
    {
        {
            ModuleACreateInst,
            ModuleADispose,
            ModuleARun
        },
        {
            ModuleBCreateInst,
            ModuleBDispose,
            ModuleBRun
        },
        {
            ModuleCCreateInst,
            ModuleCDispose,
            ModuleCRun
        },
        // end of the list
        {
            (ModuleCreateInst)NULL,
            (ModuleDispose)NULL,
            (ModuleRun)NULL
        }
    } ;


int32_t
isEndOfModuleList( const ModuleIF* module)
{
    assert( module != TYPED_NULL(ModuleIF)) ;

    if( module->constructor == (ModuleCreateInst)NULL) {
        return 1 ;
    }

    return 0 ;
}

uint32_t
getNumModules()
{
    return sizeof(ModuleList) / sizeof(ModuleList[0]) - 1 ;
}