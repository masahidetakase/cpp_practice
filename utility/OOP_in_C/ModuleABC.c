#include "ModuleABC.h"
#include <assert.h>
#include <stdio.h>
#include <string.h>

// Module A ------------------------------------------

// the identifier of ModuleA.
static const char Identifier_ModuleA[] = "ModA" ;

#define BUF_LEN (256)

// Instance of ModuleA
typedef struct tagModuleAInst
{
    char str[BUF_LEN] ;

    int32_t n ;

} ModuleAInst ;


// the constructor for the instance of the moduleA.
ModuleInst
ModuleACreateInst( const CommonInitParam* init_param)
{
    ModuleInst instance = { IDENTIFIER(Identifier_ModuleA),  NULL} ;

    ModuleAInst* const modAInst = TYPED_MALLOC(ModuleAInst, 1) ;

    // preconditions
    assert( init_param) ;

    if( modAInst != NULL) {

        strncpy( modAInst->str, init_param->str, BUF_LEN) ;

        modAInst->n = init_param->int_val ;

        modAInst->n = 0 < modAInst->n ? modAInst->n : 1 ;
    }

    instance.body = modAInst ;

    return instance ;
}

// the destructor for the instance of the moduleA.
void
ModuleADispose( const ModuleInst instance)
{
    ModuleAInst* const modAInst = (ModuleAInst*)instance.body ;

    if( IS_VALID_INSTANCE( instance, Identifier_ModuleA)) {
        modAInst->str[0] = '\0' ;
        modAInst->n = 0 ;
        free( modAInst) ;
    }
}

// the constructor for the instance of the moduleA.
void
ModuleARun( const ModuleInst instance, const CommonArg* arg)
{
    ModuleAInst* const modAInst = (ModuleAInst*)instance.body ;

    // preconditions
    assert( arg != NULL) ;

    if( !IS_VALID_INSTANCE( instance, Identifier_ModuleA)) {
        return ;
    }

    {
        int i = 0 ;
        for( i = 0 ; i < arg->int_param1 ; ++i) {
            printf( "Mod A: %s - %d \n", modAInst->str, modAInst->n) ;
        }
    }
}


// Module B ------------------------------------------

// the identifier of ModuleA.
static const char Identifier_ModuleB[] = "ModB" ;

typedef struct tagModuleBInst
{
    double dbl_val ;
} ModuleBInst ;


// the constructor for the instance of the moduleB.
ModuleInst
ModuleBCreateInst( const CommonInitParam* init_param)
{
    ModuleInst instance = { IDENTIFIER(Identifier_ModuleB),  NULL} ;

    ModuleBInst* const modBInst = TYPED_MALLOC(ModuleBInst, 1) ;

    // preconditions
    assert( init_param) ;

    if( modBInst != NULL) {
        modBInst->dbl_val = (double)init_param->int_val ;
    }

    instance.body = modBInst ;

    return instance ;
}

// the destructor for the instance of the moduleB.
void
ModuleBDispose( const ModuleInst instance)
{
    ModuleBInst* const modBInst = (ModuleBInst*)instance.body ;

    if( IS_VALID_INSTANCE( instance, Identifier_ModuleB)) {
        modBInst->dbl_val= 0. ;
        free( modBInst) ;
    }
}

// the constructor for the instance of the moduleB.
void
ModuleBRun( const ModuleInst instance, const CommonArg* arg)
{
    static const char Default_Char_Color[] = "\x1b[39m" ;
    static const char Green_Char_Color[] = "\x1b[32m" ;

    ModuleBInst* const modBInst = (ModuleBInst*)instance.body ;

    // preconditions
    assert( arg != NULL) ;

    if( !IS_VALID_INSTANCE( instance, Identifier_ModuleB)) {
        return ;
    }

    printf( "Mod B : %s%f%s\n",
            Green_Char_Color,
            modBInst->dbl_val / 2,
            Default_Char_Color) ;
}

// Module C ------------------------------------------

// the identifier of ModuleA.
static const char Identifier_ModuleC[] = "ModC" ;

typedef struct tagModuleCInst
{
    uint32_t n ;

    double* matrix ;
} ModuleCInst ;


// the constructor for the instance of the moduleB.
ModuleInst
ModuleCCreateInst( const CommonInitParam* init_param)
{
    ModuleInst instance = { IDENTIFIER(Identifier_ModuleC),  NULL} ;

    ModuleCInst* const modCInst = TYPED_MALLOC(ModuleCInst, 1) ;

    // preconditions
    assert( init_param) ;

    if( modCInst != NULL) {
        modCInst->n = init_param->int_val > 0 ? init_param->int_val : 1 ;

        modCInst->matrix =  TYPED_MALLOC( double, modCInst->n * modCInst->n) ;

        if( modCInst->matrix == TYPED_NULL(double)) {
            goto Error ;
        }

        { // init the matrix with 0.0.
            int row = 0 ;
            int col = 0 ;
            for( row = 0 ; row < modCInst->n ; ++row) {
                for( col = 0 ; col < modCInst->n ; ++col) {
                    modCInst->matrix[ modCInst->n * row + col] = 0. ;
                }
            }
        }
    }

    instance.body = modCInst ;

    return instance ;

    Error:
    {
        if( modCInst != TYPED_NULL(ModuleCInst)) {
            free( modCInst->matrix) ;
        }

        free( modCInst) ;

        instance.body = NULL ;

        return instance ;
    }
}

// the destructor for the instance of the moduleB.
void
ModuleCDispose( const ModuleInst instance)
{
    ModuleCInst* const modCInst = (ModuleCInst*)instance.body ;

    if( IS_VALID_INSTANCE( instance, Identifier_ModuleC)) {
        modCInst->n= 0 ;
        free( modCInst->matrix) ;
        free( modCInst) ;
    }
}

// the constructor for the instance of the moduleB.
void
ModuleCRun( const ModuleInst instance, const CommonArg* arg)
{
    static const char Default_Char_Color[] = "\x1b[39m" ;
    static const char Cyan_Char_Color[] = "\x1b[36m" ;

    ModuleCInst* const modCInst = (ModuleCInst*)instance.body ;

    // preconditions
    assert( arg != NULL) ;

    if( ( !IS_VALID_INSTANCE( instance, Identifier_ModuleC)) ||
        ( modCInst->matrix == TYPED_NULL(double))) {
        return ;
    }

    { // init the matrix with 0.0.
        int srow = arg->int_param1 ;
        int scol = arg->int_param2 ;
        int row = 0 ;
        int col = 0 ;

        if( ( srow < 0) || ( modCInst->n <= srow)) {
            srow = 0 ;
        }

        if( ( scol < 0) || ( modCInst->n <= scol)) {
            scol = 0 ;
        }

        modCInst->matrix[ modCInst->n * srow + scol] = arg->dbl_param ;

        for( row = 0 ; row < modCInst->n ; ++row) {
            printf( "%s|%s", Cyan_Char_Color, Default_Char_Color) ;

            for( col = 0 ; col < modCInst->n ; ++col) {
                printf(" %.4f", modCInst->matrix[ modCInst->n * row + col]) ;
            }

            printf( "%s|%s\n", Cyan_Char_Color, Default_Char_Color) ;
        }
    }
}
