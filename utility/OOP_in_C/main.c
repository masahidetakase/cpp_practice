#include "Modules.h"
#include <stdio.h>

int
main( int argc, char* argv[])
{
    CommonInitParam init_param = { "neko", 5} ;

    CommonArg arg = { 3, 2, 0.82} ;

    ModuleInst* instances = TYPED_MALLOC( ModuleInst, getNumModules()) ;

    if( instances == TYPED_NULL(ModuleInst)) {
        fprintf( stderr, "Fatal Error\n") ;
        return -1 ;
    }

    // initialize the modules.
    {
        uint32_t idx = 0 ;
        for( idx = 0 ; !isEndOfModuleList( &ModuleList[idx]) ; ++idx) {
            instances[idx] = ModuleList[idx].constructor( &init_param) ;
        }
    }

    // run the modules.
    {
        uint32_t idx = 0 ;
        for( idx = 0 ; !isEndOfModuleList( &ModuleList[idx]) ; ++idx) {
            ModuleList[idx].run( instances[idx], &arg) ;
        }
    }

    // close the modules.
    {
        uint32_t idx = 0 ;
        for( idx = 0 ; !isEndOfModuleList( &ModuleList[idx]) ; ++idx) {
            ModuleList[idx].destructor( instances[idx]) ;
        }
    }

    free( instances) ;
}
