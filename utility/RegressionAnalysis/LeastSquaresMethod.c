#include "LeastSquaresMethod.h"
#include <assert.h>

#ifndef TRUE
#define TRUE (1)
#endif

#ifndef FALSE
#define FALSE (0)
#endif

#define LSM_SQUARE(x) ((x)*(x))


// 単回帰分析の結果をクリアする
static void clearLSMResult(LSMResult* const result);

/**
 * @brief 最小二乗法モジュールのインスタンスの初期化
 */
void LSMInitInstance(LSMInstance* const lsmInst, /**< 最小二乗法モジュールのインスタンスへのポインタ */
                     const LSMGetPointFuncType pointPickupFunc /**< 座標（独立変数,従属変数）を取得する関数へのポインタ*/
                     )
{
    //preconditions
    assert(lsmInst != NULL);
    assert( pointPickupFunc != NULL);

    lsmInst->pointPickupFunc = pointPickupFunc;
    lsmInst->numSamplePoints = 0;
    lsmInst->sumIndepVals = 0.;
    lsmInst->sumDepVals = 0.;
    lsmInst->sumIndepSqured = 0.;
    lsmInst->sumDepSqured = 0.;
    lsmInst->sumIndepDep = 0.;
}


/**
 * @brief 最小二乗法モジュールのインスタンスを再利用する。
 * @detail  座標（独立変数,従属変数）を取得する関数へのポインタは維持される
 */
void LSMResetInstance(LSMInstance* const lsmInst /**< 最小二乗法モジュールのインスタンスへのポインタ */
                     )
{
    //preconditions
    assert(lsmInst != NULL);
    assert(lsmInst->pointPickupFunc != NULL);

    lsmInst->numSamplePoints = 0;
    lsmInst->sumIndepVals = 0.;
    lsmInst->sumDepVals = 0.;
    lsmInst->sumIndepSqured = 0.;
    lsmInst->sumDepSqured = 0.;
    lsmInst->sumIndepDep = 0.;
}


/**
 * @brief インスタンスに標本を登録する。
 */
void LSMRegisterSample(LSMInstance* const lsmInst, /**< 最小二乗法モジュールのインスタンスへのポインタ */
                       const void* element /**< 標本のデータへのポインタ*/
                      )
{
    double indepVal ;
    double depVal ;

    //preconditions
    assert(lsmInst != NULL);

    if( element == NULL) {
        return;
    }

    ++lsmInst->numSamplePoints;

    lsmInst->pointPickupFunc(element, &indepVal, & depVal);
    lsmInst->sumIndepVals += indepVal; //∑ x
    lsmInst->sumDepVals += depVal; //∑ y
    lsmInst->sumIndepSqured += LSM_SQUARE(indepVal); //∑ x^2
    lsmInst->sumDepSqured += LSM_SQUARE(depVal); //∑ y^2;
    lsmInst->sumIndepDep += indepVal * depVal; // ∑ xy
}


/**
 * @brief 回帰分析を行う。
 * @retval TRUE 回帰直線の取得に成功した。
 * @retval FALSE 回帰直線の取得に失敗した。
 */
BOOL LSMDoRegressionAnalysis(const LSMInstance* lsmInst, /**< 最小二乗法モジュールのインスタンスへのポインタ */
                             LSMResult* const result /** 回帰直線の係数、R2乗値へのポインタ */
                            )
{
    double denominator = 0.;

    //preconditions
    assert(lsmInst != NULL);
    assert(result != NULL);

    clearLSMResult( result);

    if( lsmInst->numSamplePoints == 0) {
        return FALSE;
    }

    // 分母
    denominator = lsmInst->numSamplePoints * lsmInst->sumIndepSqured - LSM_SQUARE( lsmInst->sumIndepVals);
    
     // 傾き
    result->inclination = lsmInst->numSamplePoints * lsmInst->sumIndepDep - lsmInst->sumIndepVals * lsmInst->sumDepVals;

    // Y切片
    result->intercept = lsmInst->sumIndepSqured * lsmInst->sumDepVals - lsmInst->sumIndepDep * lsmInst->sumIndepVals;

    if( denominator == 0.) {
        return FALSE;
    } else {
        result->inclination /= denominator;
        result->intercept /= denominator;
    }

    // 相関係数を求める
    {
        // 独立変数の平均
        result->indepAverage = lsmInst->sumIndepVals / lsmInst->numSamplePoints ;

        // 従属変数の平均
        result->depAverage = lsmInst->sumDepVals / lsmInst->numSamplePoints ;

        // 共分散
        result->coVariance = lsmInst->sumIndepDep -
                             result->indepAverage * lsmInst->sumDepVals -
                             result->depAverage * lsmInst->sumIndepVals +
                             lsmInst->numSamplePoints * result->indepAverage * result->depAverage ;

        // 独立変数の分散
        result->indepVariance = lsmInst->sumIndepSqured - result->indepAverage * lsmInst->sumIndepVals ;

        // 従属変数の分散
        result->depVariance = lsmInst->sumDepSqured - result->depAverage * lsmInst->sumDepVals ;

        // 相関係数
        result->coRelCoeffSquared = LSM_SQUARE(result->coVariance) / (result->indepVariance * result->depVariance);
    }

    return TRUE;
}

// local functions --------------------------------------------------------------------------*/
// 単回帰分析
static void clearLSMResult(LSMResult* const result)
{
    assert(result != NULL);

    result->inclination = 0.;
    result->intercept = 0.;
    result->coRelCoeffSquared = 0.;
    result->indepAverage = 0.;
    result->depAverage = 0.;
    result->coVariance = 0;
    result->indepVariance = 0.;
    result->depVariance = 0.;
}

-19643.53