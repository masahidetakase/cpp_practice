#ifndef LeastSquaresMethod_H_
#define LeastSquaresMethod_H_

#include <stdint.h>
#include <stddef.h>

typedef int BOOL ;

/**
 * @brief 指定されたポインタから独立変数と従属変数変数を抽出する関数の型。
 * @attention 対象となるデータ型にあった関数を定義すること。
 */
typedef void (*LSMGetPointFuncType)(const void* element, // 抽出対象
                                    double* const indepVal, // 独立変数を格納するポインタ。
                                    double* const depVal // 従属変数を格納するポインタ。
                                   );
/* 例
 void sampleFunc( const void* element, double* const indepVal, double* const depVal)
 {
    assert( element != NULL);
    assert( indepVal != NULL);
    assert( depVal != NULL);

    const Point* point = (Point*)element;
    
    *indepVal = (double)point->x ;
    *depVal = (double)point->y ;
 }
 */

/**
 * @brief 最小二乗法モジュールのインスタンス.
 */
typedef struct tagLSMInstance
{
    // public
    LSMGetPointFuncType pointPickupFunc ; /**< 座標（独立変数,従属変数）を取得する関数へのポインタ */

    // private
    uint32_t numSamplePoints; /**< 標本数 */

    double sumIndepVals; /**< 独立変数の総和 */

    double sumDepVals; /**< 従属変数の総和 */

    double sumIndepSqured; /**< 独立変数の二乗値の総和 */

    double sumDepSqured; /**< 従属変数の二乗値の総和 */

    double sumIndepDep; /**< 独立変数と従属変数の積の総和 */

} LSMInstance;


/**
 * @brief 最小二乗法の結果:傾き、Y切片、相関係数を格納する。
 */
typedef struct tagLSMResult
{
    // public
    double inclination; /**< 傾き*/

    double intercept; /**< Y切片*/

    double coRelCoeffSquared; /**< 相関係数の2乗値*/

    //private
    double indepAverage; /**< 独立変数の平均 */
    double depAverage; /**< 従属変数の平均 */
    double coVariance;  /**< 共分散 */
    double indepVariance; /**< 独立変数の分散 */
    double depVariance ; /**< 従属変数の分散 */

} LSMResult ;


/**
 * @brief 最小二乗法モジュールのインスタンスの初期化
 */
void
LSMInitInstance(LSMInstance* const lsmInst, /**<[in,out] 最小二乗法モジュールのインスタンスへのポインタ */
                const LSMGetPointFuncType pointPickupFunc); /**<[in] 座標（独立変数,従属変数）を取得する関数へのポインタ*/
                     

/**
 * @brief 最小二乗法モジュールのインスタンスを再利用する。
 * @detail  座標（独立変数,従属変数）を取得する関数へのポインタは維持される
 */
void
LSMResetInstance(LSMInstance* const lsmInst); /**<[in,out] 最小二乗法モジュールのインスタンスへのポインタ */

/**
 * @brief インスタンスに標本を登録する。
 */
void
LSMRegisterSample(LSMInstance* const lsmInst, /**<[in,out] 最小二乗法モジュールのインスタンスへのポインタ */
                  const void* element) ; /**<[in] 標本のデータへのポインタ*/

/**
 * @brief 回帰分析を行う。
 * @retval TRUE 回帰直線の取得に成功した。
 * @retval FALSE 回帰直線の取得に失敗した。
 */
BOOL
LSMDoRegressionAnalysis(const LSMInstance* lsmInst, /**<[in] 最小二乗法モジュールのインスタンスへのポインタ */
                        LSMResult* const result); /**<[out] 回帰直線の係数、R2乗値へのポインタ */


#endif // LeastSquaresMethod_H_
