#include "LeastSquaresMethod.h"
#include <stdio.h>
#include <string.h>

typedef  struct tagPoint
{
    int x ;
    int y ;
} Point ;

void getXY( const void* point, double* const x, double* const y)
{
    *x = ((const Point*)point)->x ;
    *y = ((const Point*)point)->y ;
}

typedef struct {
    int i ;
    char str[12] ;
} TestStruct ;

int
main( int argc, char* argv[])
{
    Point data[] =
        {
            {  5, 13},
            {  8, 20},
            {  8, 22},
            {  7, 13},
            {  9, 20},
            { 12, 26}
        } ;

    LSMInstance instance ;

    LSMResult result ;

    LSMInitInstance( &instance, getXY) ;

    size_t idx = 0 ;
    for( idx = 0 ; idx < sizeof(data) / sizeof(data[0]) ; ++idx)
    {
        LSMRegisterSample( &instance, data +idx) ;
    }

    LSMDoRegressionAnalysis( &instance, &result) ;

    printf( " y = Ax + B\n") ;
    printf( " A = %f    B = %f \n", result.inclination, result.intercept) ;
    printf( " R2 = %f \n", result.coRelCoeffSquared) ;

    return 0;
}