#include <wx/regex.h> // WxWidgets
#include <Poco/RegularExpression.h> // POCO
#include <regex> // C++11
#include <string>
#include <iostream>

using namespace std ;


int
main( int argc, char *argv[])
{
    string ascii_str =
        "I am telling you the truth: a grain of wheat "
        "remains no more than a single grain untill it "
        "is dropped into the ground and dies. If it does "
        " die, then it produces many grains." ;

    string utf8_jp_str =
        u8"一粒の麦、地に落ちて死なずば、唯一つにてあらん。"
        u8"もし死なば、多くの実を結ぶべし。" ;

    cout << "対象: " << ascii_str << endl ;

    {
        cout << "冠詞を抽出する。" << endl ;

        cout << "  ･wx::Regex" << endl ;

        wxRegEx wx_regex( wxT("[[:blank:]](an|a|the)[[:blank:]]"), wxRE_EXTENDED) ;

        wxString wx_str( ascii_str) ;

        size_t start_pos = 0, offset = 0,  match_len = 0 ;

        while( wx_regex.Matches( wx_str.Mid( offset))) {
            if( wx_regex.GetMatch( &start_pos, &match_len)) {
                offset += start_pos ;
                //cout << wx_str.SubString( start_pos, start_pos + match_len -1).ToStdString() << endl ;
                cout << "         " <<
                        "pos: " << offset << "  len: " << match_len << "   \"" <<
                        wx_str.SubString( offset, offset + match_len -1).ToStdString() <<
                        "\"" << endl ;
                offset += match_len ;
            }
        }


        cout << "-------------------------------------------" << endl ;
    }

    cout << "対象: " << utf8_jp_str << endl ;

    return 0 ;
}
