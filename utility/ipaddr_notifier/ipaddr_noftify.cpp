// This program notifies the IP address of your machine when it changes or more than 4 days
// passes after the latest notification.
// The data of this program is stored in ".ipaddr_notify.json" just under your home directory.

#include <string>
#include <iostream>
#include <fstream>
#include <Poco/Net/Net.h>
#include <Poco/Net/DialogSocket.h>
#include <Poco/Net/MailMessage.h>
#include <Poco/Net/MailRecipient.h>
#include <Poco/Net/SMTPClientSession.h>
#include <Poco/Net/NetException.h>
#include <Poco/Net/StringPartSource.h>
#include <Poco/JSON/Parser.h>
#include <Poco/Path.h>
#include <Poco/DateTime.h>
#include <Poco/LocalDateTime.h>
#include <Poco/DateTimeFormat.h>
#include <Poco/DateTimeParser.h>
#include <Poco/DateTimeFormatter.h>

using namespace std;

// Log
struct NotificationLog
{
    string own_ipaddress;

    string to_address;

    Poco::DateTime notification_time;
} ;

// Constants
namespace
{
    const std::string Data_File_Path = ".ipaddr_notify_log.json";

    const std::string SMTP_Server_Hostname = "mail.ricoh.co.jp";

    const unsigned short SMTP_Port = 25;

    const std::string Key_Own_Ip_Address = "own_ip";

    const std::string Key_To_Address = "to_addr";

    const std::string Key_Date = "date";
}

// get own ipv4 address.
std::string
getOwnIPAddress()
{
    try {
        // connect the SMTP server as TELNET.
        Poco::Net::SocketAddress smtp_sock_addr( SMTP_Server_Hostname, SMTP_Port);

        Poco::Net::DialogSocket smtp_srv_socket( smtp_sock_addr);

        Poco::Net::SocketAddress own_sock_addr = smtp_srv_socket.address();

        return own_sock_addr.host().toString();
    }
    catch( const Poco::Net::NetException& ex) {
        return string();
    }
}

bool
sendPlainTextMail( const std::string& to_address,
                   const std::string& from_address,
                   const std::string& subject,
                   const std::string& message) 
{
    Poco::Net::MailMessage mail_message;

    mail_message.setSender( from_address);

    mail_message.addRecipient( Poco::Net::MailRecipient(Poco::Net::MailRecipient::PRIMARY_RECIPIENT, to_address));

    mail_message.setSubject( subject);

    mail_message.addContent( new Poco::Net::StringPartSource( message));

    try {
        Poco::Net::SMTPClientSession smtp_srv_session( SMTP_Server_Hostname, SMTP_Port);

        smtp_srv_session.login();

        smtp_srv_session.sendMessage( mail_message);
        
        smtp_srv_session.close();
    }
    catch( const Poco::Net::NetException& ex) {
        cout << "Error: Sending mail failed. details: " << ex.message() << endl;
        return false;
    }

    return true;
}

bool
readLogFile( NotificationLog& log)
{
    using namespace Poco;

    std::string json_log_path = Poco::Path::home() + Data_File_Path;

    ifstream log_file_istream( json_log_path.c_str());

    if( log_file_istream.fail()) {
        return false;
    }

    try {
        JSON::Parser json_log_parser;

        Dynamic::Var parse_result = json_log_parser.parse( log_file_istream);

        JSON::Object::Ptr log_obj = parse_result.extract<JSON::Object::Ptr>();

        log.own_ipaddress = log_obj->get( Key_Own_Ip_Address).convert<string>();

        log.to_address = log_obj->get( Key_To_Address).convert<string>();

        int tzd = LocalDateTime().tzd();

        DateTimeParser::parse( Poco::DateTimeFormat::HTTP_FORMAT,
                               log_obj->get( Key_Date).convert<string>(),
                               log.notification_time,
                               tzd);
    }
    catch( const JSON::JSONException& ex) {
        return false; 
    }

    return true;
}

bool
saveLogFile( const string& own_ipaddress,
             const string& to_address)
{
    using namespace Poco;

    std::string json_log_path = Poco::Path::home() + Data_File_Path;

    ofstream log_file_stream( json_log_path.c_str());

    if( log_file_stream.fail()) {
        return false;
    }

    JSON::Object::Ptr json_obj = new JSON::Object;

    json_obj->set( Key_Own_Ip_Address, own_ipaddress);
    json_obj->set( Key_To_Address, to_address);

    {
        LocalDateTime now;

        string cur_date = DateTimeFormatter::format( now.timestamp(),
                                                     DateTimeFormat::HTTP_FORMAT,
                                                     now.tzd());
        json_obj->set( Key_Date, cur_date);
    }

    json_obj->stringify( log_file_stream) ;

    if( log_file_stream.fail()) {
        return  false;
    }

    return true;
}


void
printUsage()
{
    static const string usage[] =
        {
            "  This program notifies the IP address of your machine via email when it changes or",
            "  more than 2 days have passed since the last notification.",
            " ",
            "   Usage:",
            "      ipaddr_notifier(.exe)  [your e-mail address]  <\"from\" address>",
            "                                    (required)          (optional)   "
        };

    for( int i = 0; i < sizeof(usage) / sizeof(usage[0]); ++i) {
        cout << usage[i] << endl;
    }

    cout << endl;
}

void
printNotificationLog( const NotificationLog& notify_log)
{
    string prev_notification_date =
                Poco::DateTimeFormatter::format( notify_log.notification_time,
                                                 Poco::DateTimeFormat::HTTP_FORMAT,
                                                 Poco::LocalDateTime().tzd());
    cout << "previous notification" << endl <<
            "  IP address: " << notify_log.own_ipaddress << endl <<
            "  last notification: " << prev_notification_date << endl <<
            endl; 
}

int main( int argc, char *argv[])
{
    string to_address ;
    string from_address;
    const string cur_own_ipaddress = getOwnIPAddress();

    {
        if( cur_own_ipaddress.empty()) {
            cerr << "Network error!!!" << endl;
            return -1; 
        }

        if( argc < 2) {
            cerr <<  "First argument must be mail recipient(To:)." << endl << endl;
            printUsage();

            return -1;
        } else {
            to_address = argv[1];
        } 

        // argv[2]: "From" Address.
        if( 2 < argc) {
            from_address = argv[2];
        } else {
            from_address = "sinyokoserver@ricoh.co.jp";
        }
    }

    bool does_notify = false;

    // If the IP address changes or more than 2 days have passed since the last notification,
    // the notification is sent. 
    {
        NotificationLog notify_log;
        
        if( readLogFile( notify_log)) {

            printNotificationLog( notify_log);

            Poco::Timespan passed_time = 
                Poco::LocalDateTime().utc() - notify_log.notification_time;

            if( ( notify_log.own_ipaddress != cur_own_ipaddress) ||
                ( Poco::Timespan( 2, 0, 0, 0, 0) < passed_time)) {
                does_notify = true;
            }
        } else  {
            does_notify = true;
        }
    }

    if( does_notify) {
        string mesg = "IP address of your machine : " + cur_own_ipaddress + "\r\n";

        sendPlainTextMail( to_address,
                           from_address,
                           "Latest IP address of your PC.",
                           mesg);

        saveLogFile( cur_own_ipaddress, to_address);
    } else {
        cout << "No need for notification." << endl;
    }
    
    return 0;
}
