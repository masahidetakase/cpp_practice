#include "StackAllocator.h"
#include <stddef.h>
#include <stdint.h>
#include <assert.h>
#include <string.h>
#if defined(DEBUG) || defined(_DEBUG)
#include <stdio.h>
#endif

/* Constans ------------------------------------ */
#define STACKALLOC_SIGNATURE_LEN (4)
#define STACKALLOC_SIGNATURE_STR ("STAL") /* 4 characters */
#define STACKALLOC_BLOCK_HEADER_LEN ( sizeof(void*) + sizeof(size_t))
#define STACKALLOC_BLOCK_BOUNDARY_LEN ( STACKALLOC_BLOCK_HEADER_LEN + sizeof(void*))


/* Structure of memory blocks
   | pointer to StackAllocator | block size |<-- memory block -->| the pointer to |
   | ADT      (void*)          |   (size_t) |                    | the block size |
                                             0                   block_size (offset)
*/


/**
 * @brief The main ADT of StackAllocator.
 */
typedef struct
{
    uint32_t signature ; /**< signature of instances of StackAllocator. */

    int8_t* memory_pool ; /**< the pointer to the memory pool( the header is not included in it). */

    size_t pool_size ; /**< the size of the memory pool. */

    size_t total_allocated_size ; /**< the size of all of the allocated blocks. */

    size_t num_allocated_blocks ; /**< the number of the allocated blocks. */

    int8_t* latest_block ; /**< the pointer to the latest_block. */

} StackAlloc_Impl ;

typedef StackAlloc_Impl* StackAlloc_Ptr ;


/* Local functions -------------------------------------------- */
/* If the instance is valid, non-zero value is returned. */
static int32_t
isValidAllocator( const StackAlloc_Ptr allocator) ;

/* Get the size of the free area.*/
static size_t
getFreeAreaSize( const StackAlloc_Ptr allocator) ;

/* Shrink the allocated area. */
static void
shrinkAllocatedArea( const StackAlloc_Ptr allocator) ;


/* Exported functions -------------------------------------------- */

/*
 * @brief Create a new instance of memory allocator.
 */
StackAllocator
StackAlloc_create( void* const mem_area,
                   const size_t area_size)
{
    static const size_t Instance_Len = sizeof(StackAlloc_Impl) ;

    StackAlloc_Ptr allocator = (StackAlloc_Ptr)mem_area ;

    if( ( mem_area == NULL) ||
        ( area_size < Instance_Len + STACKALLOC_BLOCK_BOUNDARY_LEN)) {
        return NULL ;
    }

    // set the signature.
    allocator->signature = *(const uint32_t*)STACKALLOC_SIGNATURE_STR ;

    allocator->memory_pool = (int8_t*)( allocator + 1) ;

    allocator->pool_size = area_size - Instance_Len ;

    allocator->total_allocated_size = 0 ;

    allocator->num_allocated_blocks = 0 ;

    allocator->latest_block = NULL ;

    return allocator ;
}


/**
 * Allocate a new memory block.
 */
void*
StackAlloc_alloc( StackAllocator const instance,
                  const size_t block_size)
{
    StackAlloc_Ptr allocator = (StackAlloc_Ptr)instance ;
    size_t free_size = 0 ;

    // check the arguments.
    if( ( instance == NULL) || ( block_size == 0)) {
        return NULL ;
    }

    // check the instance.
    if( isValidAllocator( allocator) == 0) {
        return NULL ;
    }

    // Check the size of free area.
    free_size = getFreeAreaSize( allocator) ;

    if( block_size + STACKALLOC_BLOCK_BOUNDARY_LEN <= free_size) {
        int8_t* cur_loc = allocator->memory_pool ;
        int8_t* block_size_ptr = (int8_t*)NULL ;

        cur_loc += allocator->total_allocated_size ;

        // Make a header.
        *(StackAlloc_Ptr*)cur_loc = allocator ; // set the pointer to the alllocator.
        cur_loc += sizeof( StackAlloc_Ptr) ;

        block_size_ptr = cur_loc ;
        *(size_t*)block_size_ptr = block_size ; // set the block size.
        cur_loc += sizeof( size_t) ;

        allocator->latest_block = cur_loc ; // memory block
        cur_loc += block_size ;

        *(void**)cur_loc = allocator->latest_block ; // the pointer to the block size.

#if defined(DEBUG) || defined(_DEBUG)
        *( allocator->latest_block + block_size - 1) = 'a' ;
        *allocator->latest_block = 'A' ;

#endif

        allocator->total_allocated_size +=
            block_size + STACKALLOC_BLOCK_BOUNDARY_LEN ;

        allocator->num_allocated_blocks += 1 ;

        return allocator->latest_block ;
    }

    return NULL ;
}


/*
  Free an allocated memory block.
 */
void
StackAlloc_free( void* const mem_block)
{
    int8_t* header = NULL ;
    StackAlloc_Ptr allocator = (StackAlloc_Ptr)NULL ;

    if( mem_block == NULL) { return ;}

    // Get the allocator.
    header = (int8_t*)mem_block - STACKALLOC_BLOCK_HEADER_LEN ;
    allocator = *(StackAlloc_Ptr*)header ;

    if( isValidAllocator( allocator) == 0) { return ;}

    *(StackAlloc_Ptr*)header = (StackAlloc_Ptr)NULL ;

#if defined(DEBUG) || defined(_DEBUG)
    // mark the discarded block.
    {
        size_t block_size = *((size_t*)mem_block - 1) ;
        *( (int8_t*)mem_block + block_size) = 'd' ;
        *(int8_t*)mem_block = 'D' ;
    }

#endif

    // shrink the allocated area.
    shrinkAllocatedArea( allocator) ;
}


/**
 * @brief Get the free size of the memory pool.
 *
 * @param instance an instance of StackAllocator.
 */
size_t
StackAlloc_getFreeSize( StackAllocator const instance)
{
    StackAlloc_Ptr allocator = (StackAlloc_Ptr)instance ;

    if( isValidAllocator(allocator))

    assert( allocator !=  NULL) ;

    return getFreeAreaSize( allocator) ;
}


/**
 * @brief Destory the instance of StackAllocator.
 * @detail Destroyed instance can not be reused.
 * @param an instance of StackAllocator.
 */
void
StackAlloc_destroy( StackAllocator const instance)
{
    StackAlloc_Ptr allocator = (StackAlloc_Ptr)instance ;

    // preconditions
    assert( allocator !=  NULL) ;

    if( !isValidAllocator( allocator)) {
        return ;
    }

    memset( allocator, 0, sizeof(StackAlloc_Impl)) ;

    // post-conditions
    assert( !isValidAllocator( allocator)) ;
}



/* If the instance is valid, non-zero value is returned. */
static int32_t
isValidAllocator( const StackAlloc_Ptr allocator)
{
    assert( allocator !=  NULL) ;

    // Check the signature.
    if( allocator->signature == *(const uint32_t*)STACKALLOC_SIGNATURE_STR) {
        return 0 ;
    }

    // check the size of free area.
    if( allocator->pool_size <= allocator->total_allocated_size) {
        return 0 ;
    }

    return 1 ;
}


/* Get the size of the free area.*/
static size_t
getFreeAreaSize( const StackAlloc_Ptr allocator) {

    assert( allocator != NULL) ;

    assert( allocator->total_allocated_size <= allocator->pool_size) ;

    return allocator->pool_size - allocator->total_allocated_size ;
}


/* Shrink the allocated area. */
static void
shrinkAllocatedArea( const StackAlloc_Ptr allocator)
{
    StackAlloc_Ptr* alloc_loc_ptr = NULL ;
    size_t* block_size_ptr = NULL ;

    // preconditions
    assert( allocator != NULL) ;

    {
        int8_t* block = allocator->latest_block ;
        size_t block_idx = 0 ;

        for( block_idx = allocator->num_allocated_blocks ;
             0 < block_idx ;
             --block_idx) {
            // Get the block informations.
            assert( block != NULL) ;
            block_size_ptr = (size_t*)block - 1 ;

            alloc_loc_ptr =
                (StackAlloc_Ptr*)( block - sizeof(size_t) - sizeof(StackAlloc_Ptr)) ;

            if( *alloc_loc_ptr == (StackAlloc_Ptr)NULL) {
                --allocator->num_allocated_blocks ;

                allocator->total_allocated_size -=
                    *block_size_ptr + STACKALLOC_BLOCK_BOUNDARY_LEN ;

                if( 1 < block_idx) {
                    //  Jump to the next block.
                    block = *((int8_t**)alloc_loc_ptr - 1) ;
                } else {
                    block = NULL ;
                }
            } else {
                break ;
            }
        }

        allocator->latest_block = block ;

    }
}


#if defined(DEBUG) || defined(_DEBUG)
/**
 * @brief Display the debug information of the allocator.
 * @param instance an instance of StackAllocator.
 */
void
StackAlloc_debuginfo( StackAllocator const instance)
{
    const StackAlloc_Ptr allocator = (StackAlloc_Ptr)instance ;

    size_t num_blocks = 0 ;
    int8_t* allocated_area_tail = allocator->memory_pool ;
    // preconditions
    assert( instance != NULL) ;

    num_blocks = allocator->num_allocated_blocks ;

    allocated_area_tail += allocator->total_allocated_size - sizeof(size_t*) - 1 ;

    puts(   "\n") ;
    puts(   "------------ Debug information --------------") ;
    printf( "  Instance address    : %p\n", allocator) ;
    printf( "  Instance size       : %u\n", sizeof(*allocator)) ;
    printf( "  memory pool         : %p\n", allocator->memory_pool) ;
    printf( "  memory size         : %u byte\n", allocator->pool_size) ;
    printf( "  num. of blocks      : %u\n", num_blocks) ;
    printf( "  latest block        : %p\n", num_blocks !=0 ? allocator->latest_block: NULL);
    printf( "  latest block size   : %u byte\n", num_blocks !=0 ? *( (size_t*)allocator->latest_block - 1) : 0) ;
    printf( "  latest block mark(b): %c\n", num_blocks != 0 ? *(char*)allocator->latest_block : 'N') ;
    printf( "  latest block mark(e): %c\n", num_blocks != 0 ? *allocated_area_tail : 'N') ;
    printf( "  total_allocated_size: %d\n", allocator->total_allocated_size) ;
    printf( "  free size           : %u byte\n", StackAlloc_getFreeSize( instance)) ;
    puts(   "---------------------------------------------") ;
}
#endif