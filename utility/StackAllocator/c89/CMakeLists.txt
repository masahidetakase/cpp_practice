cmake_minimum_required (VERSION 2.6)

project( stack_allocator C)

add_library( StackAllocator STATIC StackAllocator.c)

add_custom_target( lib DEPENDS StackAllocator)

target_compile_options( StackAllocator PUBLIC -DNDEBUG -O2)


add_executable( test_stack_allocator StackAllocator.c main.c)

target_compile_options( test_stack_allocator PUBLIC -g -O0)

add_custom_target( test DEPENDS test_stack_allocator)

target_compile_definitions( test_stack_allocator PUBLIC DEBUG)


