#include <stdio.h>
#include <stdlib.h>
#include "StackAllocator.h"


int
main( int argc, char* argv[])
{
    static const size_t pool_size_byte = 1 * 1024 * 1024 ;
    void* memory_pool = malloc( pool_size_byte) ;

    StackAllocator allocator =
        StackAlloc_create( memory_pool, pool_size_byte) ;

    StackAlloc_debuginfo( allocator) ;

    char* block1 = (char*)StackAlloc_alloc( allocator, 124) ;

    StackAlloc_debuginfo( allocator) ;

    char* block2 = (char*)StackAlloc_alloc( allocator, 200) ;

    StackAlloc_debuginfo( allocator) ;

    char* block3 = (char*)StackAlloc_alloc( allocator, 2000000) ;

    StackAlloc_debuginfo( allocator) ;

    printf( "block1: %p\n", block1) ;
    printf( "block2: %p\n", block2) ;
    printf( "block3: %p\n", block3) ;

    StackAlloc_free( block1) ;

    StackAlloc_debuginfo( allocator) ;

    StackAlloc_free( block2) ;

    //block2 = (char*)StackAlloc_alloc( allocator, 200) ;

    StackAlloc_debuginfo( allocator) ;

    StackAlloc_destroy( allocator) ;

    free( memory_pool) ;

    return 0 ;
}