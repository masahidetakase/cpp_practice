#ifndef STACKALLOCATOR_H_
#define STACKALLOCATOR_H_

#include <stddef.h>

/**
 * @brief the type of the instance of StackAllocator.
 */
typedef void* StackAllocator ;


/**
 * @brief Create a new instance of memory allocator.
 *
 * @param mem_area the pointer to the free memory area.
 * @param area_size free area size. The usable area is smaller than that.
 *
 * @retval NULL Creation of an instance failed (ex. too small memory area).
 * @retval otherwise a new instance of the allocator.
 */
StackAllocator
StackAlloc_create( void* const mem_area,
                   const size_t area_size) ;

/**
 * @brief Allocate a new memory block.
 *
 * @param an instance an instance of StackAllocator.
 * @param block_size memory block size.
 *
 * @retval NULL Memory allocation failed.
 * @retval otherwise the pointer to the new memory block.
 */
void*
StackAlloc_alloc( StackAllocator const instance,
                  const size_t block_size) ;

/**
 * @brief Free an allocated memory block.
 * @detail Release an allocated memory block and reuse it as free area.
 *
 * @param mem_block the pointer to the memory block released.
 */
void
StackAlloc_free( void* const mem_block) ;


/**
 * @brief Get the free size of the memory pool.
 *
 * @param instance an instance of StackAllocator.
 */
size_t
StackAlloc_getFreeSize( StackAllocator const instance) ;


/**
 * @brief Destory the instance of StackAllocator.
 * @detail Destroyed instance can not be reused.
 * @param an instance of StackAllocator.
 */
void
StackAlloc_destroy( StackAllocator const instance) ;


#if defined(DEBUG) || defined(_DEBUG)
/**
 * @brief Display the debug information of the allocator.
 * @param instance an instance of StackAllocator.
 */
void
StackAlloc_debuginfo( StackAllocator const instance) ;
#endif

#endif /* STACKALLOCATOR_H_ */