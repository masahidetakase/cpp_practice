#include <iostream>
#include <vector>
#include <algorithm>
#include "Delegate.hpp"

using namespace utility ;
using namespace std ;

class Testclass
{
    public:
        Testclass( const int& k) { m_k = k ;}

        void print()
        {
            std::cout << m_k << std::endl ;
        }

        void print_squared()
        {
            std::cout << m_k*m_k << std::endl ;
        }

        ~Testclass() {}

    private:
        int m_k ;

} ;


class Rety {
    public:
        Rety()
        {
            std::fill( m_str, m_str + MAX_ARRAY_SIZE, '@') ;
        }

        void set( const size_t idx, const char value)
        {
            m_str[idx] = value ;
        }

        void print()
        {
            for( size_t idx = 0 ; idx < MAX_ARRAY_SIZE ; ++idx) {
                cout << "Rety[" << idx << "]= " << m_str[idx] << endl ; 
            }
        }

    private:
        static const size_t MAX_ARRAY_SIZE = 10 ;

        char m_str[ MAX_ARRAY_SIZE] ;
} ;


int main( int argc, char *argv[])
{
    Testclass test1(3), test2(11) ;

    delegateFactory(test2, &Testclass::print_squared)->execute() ;

    Delegate<Testclass> Print1( test1, &Testclass::print) ;
    Delegate<Testclass> Print1_sq( test1, &Testclass::print_squared) ;
    Delegate<Testclass> Print2( test2, &Testclass::print) ;
    Delegate<Testclass> Print2_sq( test2, &Testclass::print_squared) ;    

    Print1.execute() ;
    Print1_sq.execute() ;
    Print2.execute() ;
    Print2_sq.execute() ;

    Rety rety ;

    rety.set( 5, 'T') ;
    rety.print() ;
    return 0 ;
}