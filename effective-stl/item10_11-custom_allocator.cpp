//============================================================================
// Name        : custom_allocator.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <memory>
#include <utility>
#include <vector>
#include <algorithm>
#include <cstdlib>
#include <stdexcept>
#include <new>
#include <functional>
#include <cstdlib>
using namespace std;


// Assign or withdraw the fragments of the memory block.
template<typename T>
struct MemBlock
{
    std::vector<bool> m_is_assigned ;
    
    vector<bool>::iterator m_cur_chunk ;
    
    T* m_mem_block ;
    
    
    MemBlock( const size_t& num_objs = )
        : m_is_assigned( num_objs, false),
          m_cur_chunk( m_is_assigned.begin())
    {
        m_mem_block = std::malloc( m_is_assigned.size() * sizeof(T)) ;
        if( m_mem_block == NULL) {
            std::bad_alloc() ;
        }
    }
    
    
     MemBlock( const MemBlock& src)
        : m_is_assigned( src.m_is_assigned),
          m_cur_chunk( m_is_assigned.begin())
    {
        m_mem_block = std::malloc( m_is_assigned.size() * sizeof(T)) ;
        if( m_mem_block == NULL) {
            std::bad_alloc() ;
        }
        
    }
    
    
    ~MemBlock()
    {
        std::free( m_mem_block) ;
    }
    
    
    T* assign( const size_t& numObjs)
    {
        vector<bool>::iterator empty_chunk
            = std::search_n( m_cur_chunk,
                             m_is_assigned.end(),
                             numObjs,
                             false) ;
         
        if( empty_chunk == m_is_assigned.end()) {
            empty_chunk 
                = std::search_n( m_is_assigned.begin(),
                                 m_cur_chunk,
                                 numObjs,
                                 false) ;
        }
        
        if( empty_chunk == m_is_assigned.end()) {
            return nullptr ;
        }
        
        std::fill( empty_chunk, empty_chunk + numObjs, true) ;
        m_cur_chunk = empty_chunk ;
        
        return m_mem_block + ( empty_chunk - m_is_assigned.begin()) ;
    }
    
    
    void withdraw( T* fragment, const size_t& numObj)
    {
        vector<bool>::iterator head 
            = m_is_assigned.begin() + ( m_mem_block - fragment) ;
            
        std::fill( head, head + numObjs, false) ;
    }
    
} ;



template<typename T, const size_t DEF_BLOCK_SIZE = 51200>
class SlabAllocator
{
    public:
    
        SlabAllocator()
        {
            if( DEF_BLOCK_SIZE < sizeof(T)) {
                throw std::bad_alloc() ;
            }
            
            m_mem_block_array.push_back( mem
            
        }
        
        
        T* allocate( const size_t& num_objs)
        {
            T* fragment = nullptr ;
            
            std::vector<MemBlock>::iterator 
                = std::find( m_mem_block_array.begin(),
                             m_mem_block_array.end(),
                             [num_objs, fragmnet]( MemBlock& mem_blk) 
                             { 
                                if( ( fragmnet = mem_blk.assign( num_objs)) == nullptr) {
                                    return false ;
                                } else {
                                    return true ;
                                }
                             }) ;
            
            if( )
                   
        }

    private:
        std::vector<MemBlock> m_mem_block_array ;
        
        
} ;

template<class T, size_t BLOCK_SIZE = 1024>
class CustomAllocator
{
	public:
        typedef size_t    size_type ;
        typedef ptrdiff_t difference_type ;
        typedef T*        pointer ;
        typedef const T*  const_pointer ;
        typedef T&        reference ;
        typedef const T&  const_reference ;
        typedef T         value_type ;

        template<class U>
        struct rebind
        {
            typedef CustomAllocator<U> other ;
        } ;

        pointer address( reference x) const { return &x ; }

        const_pointer address( const_reference x) const { return &x ; }

        // numObj 
        pointer allocate( size_type numObj)
        {

        }
} ;


int main() {
	cout << "!!!Hello World!!!" << endl; // prints !!!Hello World!!!
	return 0;
}
