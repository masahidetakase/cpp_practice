#include <algorithm>
#include <iostream>
#include <iterator>
#include <functional>
#include <list>

using namespace std ;

struct Vector2D {
    double m_x, m_y ;
} ;

// calculate vectsA[N] ･ vectsB[N]
struct calc_inner_product : public binary_function< Vector2D, Vector2D, double>
{
    double operator() ( const Vector2D& vect1, const Vector2D& vect2)
    {
        return vect1.m_x * vect2.m_x + vect1.m_y * vect2.m_y ;
    }

} ;


int main( int argc, char* argv[])
{
    Vector2D vectsA[] = { { 0.,  1.}, {  4., -2.}, { -1, 3.}, {  7.,  8.}, {  1.,  3.}} ;
    Vector2D vectsB[] = { { 3., -5.}, { -1.,  7.}, {  8, 2.}, { -4., -7.}, {  4., -5.}} ;

    list<double> inner_products ;

    // inserter
    transform( vectsA, vectsA + 5,
               vectsB,
               inserter( inner_products, inner_products.end()),
               calc_inner_product()) ;

    cout << "inserter" << endl ;
    copy( inner_products.begin(), inner_products.end(), ostream_iterator<double>( cout, ", ")) ;


    // front inserter( push_front)
    inner_products.clear() ;
    transform( vectsA, vectsA + 5,
               vectsB,
               front_inserter( inner_products),
               calc_inner_product()) ;

    cout << endl << "front_inserter" << endl ;
    copy( inner_products.begin(), inner_products.end(), ostream_iterator<double>( cout, ", ")) ;

    // back inserter( push_back)
    inner_products.clear() ;
    transform( vectsA, vectsA + 5,
               vectsB,
               back_inserter( inner_products),
               calc_inner_product()) ;

    cout << endl << "back_inserter" << endl ;
    copy( inner_products.begin(), inner_products.end(), ostream_iterator<double>( cout, ", ")) ;    
}
