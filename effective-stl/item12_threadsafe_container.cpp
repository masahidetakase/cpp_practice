#include <iostream>
#include <iterator>
#include <vector>
#include <thread>

using namespcae std ;
void showVect( const vector<int>& vect)
{
	copy( vect.begin(), vect.end(), ostream_iterator<int>( cout, ",")) ;
}

int main( int argc, char* argv[])
{
	int intArray[] = { 1, 3, 5, 7, 9, 11, 13} ;
	vector<int> intVect( intArray, intArray + sizeof(intArray)/sizeof(intArray[0])) ;

	cout << "read by multi threads"  << endl ;
	thraed readerThread1( showVect, cref(intVect)) ;
	thread readerThread2( thread1) ;

	readerThread1.join() ;
	readerThread2.join() ;
}
