#include <iostream>
#include <string>
#include <iomanip>
#include <sstream>

/*
 traits（特性）はそれ単独では意味を持たない。
 その特性を利用するアルゴリズム（関数）との組み合わせにより効果を発揮する。

 アルゴリズムを実装する者は、特性の実装詳細を意識せずに本質的な操作の実装に集中できる。
 また、型の実装する者は、アルゴリズムを利用するためだけに既存の型に手を加える必要がない。
*/

// 数値型のtraits。
// 文字列化、加算、乗算という特性を持つ。
template<typename T>
struct NumericTraits
{
    typedef T traits_type ;

    // 文字列化という数値型の本質的ではない機能は、外で定義する。
    static std::string str( const T& obj) ;

    // 二つの数値型の和をとるという特性。
    static T add( const T& lhs, const T& rhs) ;

    // 二つの数値型の積をとるという特性。
    static T multiply( const T& lhs, const T& rhs) ;
} ;


// 複素数
class Complex {
    public:
        Complex( const double& real = 0., const double& imaginary = 0.)
            : m_real( real),
              m_imaginary( imaginary) {}

        Complex( const Complex& src)
            : m_real( src.m_real),
              m_imaginary( src.m_imaginary) {}

        const Complex& operator= ( const Complex& rhs)
        {
            m_real = rhs.m_real ;
            m_imaginary = rhs.m_imaginary ;
            return *this ;   
        }

        const double getReal() const { return m_real ; }

        const double getImaginary() const { return m_imaginary ;}

    private:
        double m_real, m_imaginary ;
} ;


// 3次元ベクトル
class Vector3D {
    public:
        Vector3D( const double& x= 0., const double& y = 0., const double& z = 0.)
            : m_x( x),
              m_y( y),
              m_z( z) {}

        Vector3D( const Vector3D& src)
            : m_x( src.m_x),
              m_y( src.m_y),
              m_z( src.m_z) {}

        const Vector3D& operator= ( const Vector3D& rhs)
        {
            m_x = rhs.m_x ;
            m_y = rhs.m_y ;
            m_z = rhs.m_z ;
            return *this ;
        }

        const double getX() const { return m_x ; }

        const double getY() const { return m_y ; }

        const double getZ() const { return m_z ; }

    private:
        double m_x, m_y, m_z ;
} ;


// NumericTraits　をComplex型に特殊化させる。
template<>
struct NumericTraits<Complex>
{
    static std::string str( const Complex& obj)
    {
         std::stringstream sstrm ;
         sstrm << std::fixed << std::setprecision(3) << obj.getReal() << " + " << obj.getImaginary() << " * i " ;
         return sstrm.str() ;
    }

    static Complex add( const Complex& lhs, const Complex& rhs)
    {
        return Complex( lhs.getReal() + rhs.getReal(), lhs.getImaginary() + rhs.getImaginary()) ;
    }

    static Complex multiply( const Complex& lhs, const Complex& rhs)
    {
        return Complex( lhs.getReal() * rhs.getReal() - lhs.getImaginary() * rhs.getImaginary(), 
                        lhs.getImaginary() * rhs.getReal() + lhs.getReal() * rhs.getImaginary()) ;
    }

} ;


// NumericTraits　をVector3D型に特殊化させる。
template<>
struct NumericTraits<Vector3D>
{
    static std::string str( const Vector3D& obj)
    {
        std::stringstream sstrm ;
        sstrm << std::fixed << std::setprecision(3) << "( " << obj.getX() << ", " << obj.getY() << ", " << obj.getZ() << ")" ;
        return sstrm.str() ;
    }

    static Vector3D add( const Vector3D& lhs, const Vector3D& rhs)
    {
        return Vector3D( lhs.getX() + rhs.getX(), lhs.getY() + rhs.getY(), lhs.getZ() + rhs.getZ()) ;
    }

    static Vector3D multiply( const Vector3D& lhs, const Vector3D& rhs)
    {
        return Vector3D( lhs.getY() * rhs.getZ() - lhs.getZ() * rhs.getY(),
                         lhs.getZ() * rhs.getX() - lhs.getX() * rhs.getZ(),
                         lhs.getX() * rhs.getY() - lhs.getY() * rhs.getX()) ;
    }

} ;


// str(), add(), multiply()の特性が定義された型ならば、以下のアルゴリズム(関数)を適用できる。 

// 型ごとにoperator+、* を定義する必要がない.
template<typename NumericType>
NumericType operator+ ( const NumericType& lhs, const NumericType& rhs) 
{
    return NumericTraits<NumericType>::add( lhs, rhs) ; // NumericTypeが何であろうとadd()という特性を使用すればよい
}

template<typename NumericType>
NumericType operator* ( const NumericType& lhs, const NumericType& rhs) 
{
    return NumericTraits<NumericType>::multiply( lhs, rhs) ;　// NumericTypeが何であろうとmultiply()という特性を使用すればよい
}

// 合計を求めるテンプレート関数。
template<typename NumericType>
NumericType amount( const NumericType* const array, const size_t N)
{
    NumericType ret ;

    for( size_t idx = 0 ; idx < N ; ++idx) {
        ret = ret + array[idx] ;
    } 

    return ret ;
}

// 文字列化
template<typename NumericType>
std::string stringize( const NumericType& obj)
{
    return NumericTraits<NumericType>::str( obj) ;
}


int main( int argc, char *argv[])
{
    // 足し算
    std::cout << stringize( Complex( 1., 2.) + Complex( 3., 4.)) << std::endl ;  
    std::cout << stringize( Vector3D( 5., 4., 3.) + Vector3D( 2., 7., 5.)) << std::endl ;

    // 掛け算
    std::cout << stringize( Complex( 1., 2.) * Complex( 3., 4.)) << std::endl ;  
    std::cout << stringize( Vector3D( 5., 4., 3.) * Vector3D( 2., 7., 5.)) << std::endl ;

    // 複素数の配列の総和
    Complex complex_array[]
        = { Complex( 1., 5.), Complex( 2., 4.), Complex( 3., 3.), Complex( 4., 2.) , Complex( 5., 1.)} ;

    std::cout << stringize( amount(complex_array, 5)) << std::endl ;

    // 3次元ベクトル配列の総和
    Vector3D vect3d_array[]
        = { Vector3D( 1., 1., 1.), Vector3D( 2., 2., 2.), Vector3D( 3., 3., 3.)} ;

    std::cout << stringize( amount( vect3d_array, 3));

    return 0 ;
}