#include <time.h>
#include <stdlib.h>
#include <vector>
#include <iostream>
//const long SIZE = 10000000;
long current_capacity = 0;
int main( int argc, char *argv[])
{
  long SIZE = atol( argv[1]) ;

  clock_t start_time, end_time;
  /******************************************************************
   *1st trial
   ******************************************************************/
  start_time = clock();
  std::vector<int> v1;
  std::cout << "v1's capacity: " << v1.capacity() << std::endl;
  for(int i = 1; i <= SIZE; ++i) {
    current_capacity = v1.capacity();
    v1.push_back(i);
    if(current_capacity != v1.capacity()){
      std::cout << "v1's capacity: " << v1.capacity() << std::endl;
    }
  }
  end_time = clock();
  double erapsed_1 = (double)(end_time - start_time) / CLOCKS_PER_SEC;
  /******************************************************************
   * 2nd trial
   ******************************************************************/
  start_time = clock();
  std::vector<int> v2;
  v2.reserve(SIZE);
  std::cout << "v2's capacity: " << v2.capacity() << std::endl;
  for(int i = 1; i <= SIZE; ++i) {
    current_capacity = v2.capacity();
    v2.push_back(i);
    if(current_capacity != v2.capacity()){
      std::cout << "v2's capacity: " << v2.capacity() << std::endl;
    }
  }
  end_time = clock();
  double erapsed_2 = (double)(end_time - start_time) / CLOCKS_PER_SEC;
  /******************************************************************
   * show result
   ******************************************************************/
  std::cout << "Result time without reserve: " << erapsed_1 << " sec" << std::endl;
  std::cout << "Result time with    reserve: " << erapsed_2 << " sec" << std::endl;
  return 0;
}
