// Command forcompiling. 
// g++ -lstdc++ -std=c++11 -o Item24_map_at_Cpp11.cpp

#include <iostream>
#include <string>
#include <map>
#include <chrono>
#include <stdexcept>

using namespace std ;

int main( int argc, char *argv[])
{
    map<string,int> strIntMap ;

    strIntMap.insert( make_pair( "Inu",  4)) ;
    strIntMap.insert( make_pair( "Saru", 1)) ;
    strIntMap.insert( make_pair( "Kiji", 7)) ;

    try {

        strIntMap[ "Neko"] = -5 ; // OK 
        strIntMap.at( "Usagi") = 10 ; // throw std::out_of_range

    } catch( out_of_range& ex) {
        cout << "throw:" << ex.what() << endl ;
    }
    return 0 ;
}