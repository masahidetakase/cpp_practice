#include <iostream>
#include <set>
#include <string>
#include <algorithm>
#include <iterator>

using namespace std ;

void
printIntVect( const string& msg, const vector<int>& int_vect)
{
    cout << msg << " : " ;
    for( int num : int_vect) { 
        cout <<  num << ", ";
    }
    cout << endl << endl ;   
}

int
main( int argc, char* argv[])
{
    // initializer list in C++11 
    vector<int> set_A = { 1, 2, 3, 4, 5, 6, 7, 8, 9} ;
    vector<int> set_B = { 5, 6, 7, 8, 9, 10, 11, 12, 13} ;

    printIntVect( "Set A", set_A) ;
    printIntVect( "Set B", set_B) ;


    // ソート順を維持した重複を許す融合
    vector<int> merged_set ;
    merge( set_A.begin(), set_A.end(), set_B.begin(), set_B.end(), back_inserter(merged_set)) ;
    printIntVect( "Merge A and B with duplication", merged_set) ;

    // 和集合
    vector<int> union_set ;
    set_union( set_A.begin(), set_A.end(), set_B.begin(), set_B.end(), back_inserter(union_set)) ;
    printIntVect( "Unite A and B without duplication", union_set) ;

    // 差集合 A and not B
    vector<int> diff_set_A ;
    set_difference( set_A.begin(), set_A.end(), set_B.begin(), set_B.end(), back_inserter(diff_set_A)) ;
    printIntVect( " A and not B", diff_set_A) ;

    // 差集合 B and not A
    vector<int> diff_set_B ;
    set_difference( set_B.begin(), set_B.end(), set_A.begin(), set_A.end(), back_inserter(diff_set_B)) ;
    printIntVect( " B and not A", diff_set_B) ;

    //積集合 A and B
    vector<int> intersection_AB ;
    set_intersection( set_A.begin(), set_A.end(), set_B.begin(), set_B.end(), back_inserter(intersection_AB)) ;
    printIntVect( "A and B ", intersection_AB) ;

    // (A or B) - (A and B)
    vector<int> not_AandB ;
    set_symmetric_difference( set_A.begin(), set_A.end(), set_B.begin(), set_B.end(), back_inserter(not_AandB)) ;
    printIntVect( "(A or B) - (A and B)", not_AandB) ;
}
