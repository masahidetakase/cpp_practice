// Compile: g++ -lstdc++ -std=c++11 -o item33_unique_copy item33_unique_copy.cpp

#include <algorithm>
#include <iostream>
#include <vector>
#include <random>
#include <chrono>
#include <iterator>

using namespace std ;

int main( int argc, char* argv[])
{
    // ある範囲の整数からなる乱数列 int_vect を生成
    random_device seed_gen ;
    mt19937 random_src( seed_gen()) ;
    uniform_int_distribution<int> distribution( 1, 4) ;

    vector<int> int_vect, unique_vect ;

    for( size_t idx = 0 ; idx < 20 ; ++idx) {
        int_vect.push_back( distribution( random_src)) ;
    }

    cout << "Original:" << endl ;
    copy( int_vect.begin(), int_vect.end(), ostream_iterator<int>( cout, ", ")) ;
    
    //同じ値が連続している部分を一つに「圧縮」する
    cout << endl << "unique_copy" << endl ;
    unique_copy( int_vect.begin(), int_vect.end(), back_inserter(unique_vect)) ;
    copy( unique_vect.begin(), unique_vect.end(), ostream_iterator<int>( cout, ", ")) ;

    cout << endl << "sort & unique_copy" << endl ;
    unique_vect.clear() ;
    sort( int_vect.begin(), int_vect.end()) ;
    unique_copy( int_vect.begin(), int_vect.end(), back_inserter(unique_vect)) ;
    copy( unique_vect.begin(), unique_vect.end(), ostream_iterator<int>( cout, ", ")) ;
}
