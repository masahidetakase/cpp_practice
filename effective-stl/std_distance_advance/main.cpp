#include <iostream>
#include <vector>
#include <algorithm>

using namespace std ;

int main( int argc, char* argv[])
{

    vector<int> int_array = { 1, 2, 3, 4, 5, 6, 7, 8, 9} ;

    typedef vector<int>::iterator Iter ;
    typedef vector<int>::const_iterator CIter ;

    CIter citer = find( int_array.begin(), int_array.end(), 7) ;

    vector<int>::iterator iter = int_array.begin() ;

    advance( iter, distance<CIter>( int_array.cbegin(), citer));

    cout << "iter indicates " << *iter << endl ;
}
