#include <algorithm>
#include <iostream>
#include <iterator>
#include <functional>

using namespace std ;

int main( int argc, char *argv[])
{
   int first[] = { 1, 2, 3, 4} ;

   int second[] = { 1 , 2, 3, 4};

   transform( first, first + 4, first, bind2nd( multiplies<int>(), -1)) ;

   copy( first, first + 4, ostream_iterator<int>(cout, "\n")) ;

   int gh[][2] = { {1,2}, {3,4}} ;
   
   int kl[][2] = { {1,2}, {3,4}};

   int a[][2] = 
      { {0,0},
        {0,0}
      } ;
   
   for( int row = 0; row < 2 ; ++row)
   {  
      for( int col = 0 ; col < 2 ; ++col)
      {
         a[row][col] += gh[row][0] * kl[0][col] ;
         a[row][col] += gh[row][1] * kl[1][col] ;
      }
   }

   cout << "[0][0]=" << a[0][0] << endl ;
   cout << "[0][1]=" << a[0][1] << endl;
   cout << "[1][0]=" << a[1][0] << endl;
   cout << "[1][1]=" << a[1][1] << endl;
}
