#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <algorithm>
#include <functional>
#include <cassert>

class Tenant
{
    public:
        Tenant( const std::string& name,
                const int& floor,
                const int& room_no)
            : m_name( name),
              m_floor( floor),
              m_room_no( room_no)
        {
            assert( m_name.size() != 0) ;
            assert( 0 < m_room_no && m_room_no < 13) ;
        }

        void showInfo() const 
        {
            std::stringstream ss ;
            ss << "{" ;
            ss << "\"Name\" : \"" << m_name << "\", " ;
            ss << "\"Floor\" : \"" << m_floor << "\", " ;
            ss << "\"Room No.\" : \"" << m_room_no << "\"" ;
            ss << "}" ;

            std::cout << ss.str() << std::endl ;
        }

        void setFloor( const int& floor) 
        {
            m_floor = floor ;
        }

    private:
        std::string m_name ;
        int m_floor ;
        int m_room_no ; 
} ;


int main( int argc, char* argv[])
{
    Tenant tenants[] = { Tenant( "MushMush Cuty Blond", 5, 1),
                         Tenant( "Watanabe store", 3, 2),
                         Tenant( "Drug store Sugi", 4, 1),
                         Tenant( "Kyuushuuya", 6, 3)
                        } ;

    std::vector<Tenant*> vinawalk_tenants ;

    vinawalk_tenants.push_back( tenants) ;
    vinawalk_tenants.push_back( tenants + 1) ;
    vinawalk_tenants.push_back( tenants + 2) ;
    vinawalk_tenants.push_back( tenants + 3) ;

    // mem_fn
    std::cout << "mem_fn" << std::endl ;
    std::for_each( vinawalk_tenants.begin(),
                   vinawalk_tenants.end(),
                   std::mem_fun( &Tenant::showInfo)) ;
    
    // mem_fn_ref
    std::cout << std::endl << "mem_fn_ref" << std::endl ;
    std::for_each( tenants,
                   tenants + 4,
                   std::mem_fun_ref( &Tenant::showInfo)) ;

    // 引数をとるメソッドを呼ぶ場合
    std::cout << std::endl << "mem_fn_ref & bind2nd" << std::endl ;
    std::for_each( tenants,
                   tenants + 4,
                   std::bind2nd( std::mem_fun_ref( &Tenant::setFloor),0)) ; 
                   // mem_fnを介してメンバ関数をファンクタにすることで、STLとの組み合わせが容易になる
    
    std::for_each( tenants,
                   tenants + 4,
                   std::mem_fun_ref( &Tenant::showInfo)) ;  


}
