/*
 * g++ item7_container_with_smartptr.cpp -lstdc++ -std=c++0x -o item7
 */

#include <memory>
#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
#include <functional>
 
using namespace std;

// Base class
class Widget
{
    public:
        Widget() : m_winID( CUR_MAX_ID++) {}
        
        virtual ~Widget()
        {
            cout << "ID: " << getWinID() << " removed." << endl ;   
        }
    
        // Display the information 
        virtual void show() const 
        {
            cout << "Simple Window ID: " << getWinID() << endl ;
        }
        
        inline int getWinID() const
        {
            return m_winID ;
        }
    
    private:
        static int CUR_MAX_ID ;
        
        const int m_winID ; 
} ;

int Widget::CUR_MAX_ID = 0 ;


// Derived class from Widget.
class Button : public Widget
{
    public:
        void show() const 
        {
            cout << "Button ID: " << getWinID() << endl ;
        }
        
        ~Button()
        {
            cout << "ID: "  << getWinID() << " was deleted as Button." << endl  ;
        }
    
} ;

// Derived class from Widget.
class StaticTextBox : public Widget
{
    public:
        StaticTextBox( const string& text) : m_text( text) {}
    
        void show() const 
        {
            cout << "Static text ID: " << getWinID() << " text=\"" << m_text << "\"" << endl ;
        }
        
        ~StaticTextBox()
        {
            cout << "ID: "  << getWinID() << " was deleted as StaticTextBox." << endl  ;
        }
    
    private:
        
        const string m_text ;
} ;


// Display the window information.
template< class PtrType>
struct ShowWidgetPtr {
    void operator() ( const PtrType ptr)
    {
        ptr->show() ;
    }
} ;


// Delete widgets.
template< class T>
struct DeleteObj {
    void operator() ( T* const ptr)
    {
        delete ptr ;
    }
} ;

int main( int argc, char *argv[])
{
    vector<Widget*> widgetArray ;
    
    widgetArray.push_back( new Widget) ;
    widgetArray.push_back( new Button) ;
    widgetArray.push_back( new StaticTextBox( "Hoge is dead.")) ;
    
    for_each( widgetArray.begin(), widgetArray.end(), ShowWidgetPtr<Widget*>()) ;
    cout << endl ;
    // Delete the objects manually. 
    for_each( widgetArray.begin(), widgetArray.end(), DeleteObj<Widget>()) ;
    cout << endl ;
    
  
    // using shared_ptr.
    typedef shared_ptr<Widget> Widget_Sptr ;
    vector<shared_ptr<Widget>> widgetSmartArray ;
    widgetSmartArray.push_back( Widget_Sptr( new Widget)) ;
    widgetSmartArray.push_back( Widget_Sptr( new Button)) ;
    widgetSmartArray.push_back( Widget_Sptr( new StaticTextBox( "Fuga is dead."))) ;
    for_each( widgetSmartArray.begin(), widgetSmartArray.end(), ShowWidgetPtr<Widget_Sptr>()) ;
    
    // The Widgets(ID:3-5) were removed automatically. 
}
