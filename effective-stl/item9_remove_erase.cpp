#include <iostream>
#include <vector>
#include <algorithm>
#include <list>
#include <iterator>
using namespace std ;

int main( int argc, char *argv[])
{
    int intVals[] = { 1, 2, 3, 4, 5, 6, 7, 8} ; 
    vector<int> intArray( intVals, intVals + sizeof(intVals)/sizeof(intVals[0])) ;
    
    remove( intArray.begin(), intArray.end(), 5) ;
    cout << "vector: remove() only   : " ;
    copy( intArray.begin(), intArray.end(), ostream_iterator<int>( cout, ",")) ;
    cout << endl ;
    
    // reset intArray.
    intArray.assign( intVals, intVals + sizeof(intVals)/sizeof(intVals[0])) ;
    intArray.erase( remove( intArray.begin(), intArray.end(), 5)) ;
    cout << "vector: remove() & erase: " ;
    copy( intArray.begin(), intArray.end(), ostream_iterator<int>( cout, ",")) ;
    cout << endl ;
    
    // list
    list<int> intList( intVals, intVals + sizeof(intVals)/sizeof(intVals[0])) ;
    intList.remove( 5) ;
    cout << "list.remove() : " ;
    copy( intList.begin(), intList.end(), ostream_iterator<int>( cout, ",")) ;
    cout << endl ;
}
