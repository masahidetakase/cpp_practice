#include <algorithm>
#include <iostream>
#include <string>
#include <vector> 
#include <map>
#include <functional>
#include <iterator>

using namespace std ; 

struct createPair : public binary_function<int, string, pair<int,string> >
{
    std::pair<int,string> operator() ( const int& key, const string& val)
    {
        return make_pair( key, val) ;
    }
} ;

int main( int argc, char *argv[])
{
    int key[] = { 1, 5, 8, 6, 2, 7, 9, 3} ;
    string val[] = { "a", "b", "c", "d", "e", "f", "g", "h"} ;

    map<int,string> intStrMap ;

    transform( key, key + 8, val, inserter( intStrMap, intStrMap.end()), createPair()) ;

    map<int,string>::iterator iter = intStrMap.begin() ;

    for( ; iter != intStrMap.end(); ++iter) {
        cout << iter->first << " = "  << iter->second << endl ;
    }

    vector<int> k ;

    k.erase( k.end()) ;


    return 0 ;
}
