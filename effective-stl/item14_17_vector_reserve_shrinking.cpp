#include <iostream>
#include <vector>

using namespace std ;

int pushIntVector( vector<int>& intVect, int insInt)
{
    int* prevData = &intVect[0] ;
    intVect.push_back( insInt) ;

    cout << "Inserter: " << insInt ;

    if( prevData != &intVect[0]) {
        cout << "  Memory reallocation!" ;
    }

    cout << endl ;
}


int main( int argc, char *argv[])
{
    vector<int> noReservedVect( 1, -1) ;
    vector<int> reservedVect( 1, -1) ;

    reservedVect.reserve(64) ;

    for( int idx = 0 ; idx < 64 ; ++idx) {
        cout << "No Reservation " ;
        pushIntVector( noReservedVect, idx) ;

        cout << "Reservation " ;
        pushIntVector( reservedVect, idx) ;
    }

    cout << endl << "*************** Shrink the vector ***************" << endl ;

    vector<int> tooLargeVect ;
    tooLargeVect.reserve(128) ;

    tooLargeVect.push_back(1) ;
    tooLargeVect.push_back(2) ;

    cout << "the size     before shrinking: " << tooLargeVect.size()  << endl ;
    cout << "the capacity before shrinking: " << tooLargeVect.capacity() << endl  << endl ;

    vector<int>( tooLargeVect).swap( tooLargeVect) ;

    cout << "the size     after  shrinking: " << tooLargeVect.size() << endl ;
    cout << "the capacity after  shrinking: " << tooLargeVect.capacity() << endl ;

}