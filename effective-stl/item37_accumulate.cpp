#include <iostream>
#include <numeric>
#include <string>
#include <vector>
#include <functional>
#include <sstream>

using namespace std ;

struct Person
{
    string m_name ;
    int m_age ;
    enum Sex { Male, Female} m_sex ;

    Person( const string& name, const int& age, const Sex& sex)
        : m_name( name),
          m_age( age),
          m_sex( sex) {}
} ;

int main( int argc, char* argv[])
{

    Person persons[] = { { "Inuyama Takeo", 24, Person::Male},
                         { "Nekoda Momoko", 14, Person::Female},
                         { "Ushioka Masao", 46, Person::Male} } ;

    auto csv_stringizer = []( const string& str, const Person& person) -> string 
    {
        stringstream ss ;

        string sex ; 
        sex = person.m_sex == Person::Male ? string("\"Male\"") : string("\"Female\"") ; 

        ss << str << 
              "\"" << person.m_name << "\", " <<
              "\"" << person.m_age << "\", " <<
              "\"" << sex <<  
               "\n" ;

        return ss.str() ;  
    } ;

    // JSON: [ {"name":"John Smith", "age":12, "sex": "Male"}, {"name":"Kate Morissete", "age":34, "sex": "Female"}]
    auto json_acm = []( const string str, const Person& person) -> string
    {
        return "" ;
    }



    cout << accumulate( persons, persons + 3, string(""), csv_stringizer) ;

    return 0 ;
}
