#include <set>
#include <map>
#include <functional>
#include <string>
#include <iostream>
#include <cctype>
#include <algorithm>
#include <iterator>

// Multibyte strings are not supported.  
struct caseInsensitiveLess
{
    bool operator() ( const std::string& str1, const std::string& str2)
    {
        std::string::const_iterator iter1 = str1.begin() ;
        std::string::const_iterator iter2 = str2.begin() ;

        for( ; iter1 != str1.end() && iter2 != str2.end() ; ++iter1, ++iter2) {
            if( std::tolower(*iter1) < std::tolower(*iter2)) { return true ;}
        }

        if( ( iter1 == str1.end()) && (iter2 != str2.end())) { return true ;} // when str1 is shorter than str2. 

        return false ;
    }
} ;


int main( int argc, char *argv[])
{
    typedef std::set<std::string,caseInsensitiveLess> CaseInsensitiveSet ;

    CaseInsensitiveSet caseInsensSet ;
    caseInsensSet.insert( "hoge") ;
    caseInsensSet.insert( "Hoge") ;
    caseInsensSet.insert( "fuga") ;
    caseInsensSet.insert( "FUGA") ;
    caseInsensSet.insert( "CAT") ;
    caseInsensSet.insert( "cAT") ;

    // Dump the set container.
    std::copy( caseInsensSet.begin(), caseInsensSet.end(), std::ostream_iterator<std::string>(std::cout, ",")) ;
    std::cout << std::endl << std::endl ;

    // Find by the set::find()・・・等価.
    std::cout << "# set::find()" << std::endl ; 
    CaseInsensitiveSet::iterator foundByLess = caseInsensSet.find( "FUGA") ;
    if( foundByLess != caseInsensSet.end()) {
        std::cout << "Found by less()" << std::endl ;
    } else {
        std::cout << "Not Found by less()" << std::endl ;
    }
    std::cout << std::endl ;

    // Find by the find() algorithm･･･等値.
    std::cout << "# std::find()" << std::endl ;
    CaseInsensitiveSet::iterator foundByEqual = std::find( caseInsensSet.begin(), caseInsensSet.end(), "FUGA") ;
    if( foundByEqual != caseInsensSet.end()) {
        std::cout << "Found by equal_to()" << std::endl ;
    } else {
        std::cout << "Not Found by equal_to()" << std::endl ;
    }
    std::cout << std::endl ;

    return 0 ;
}
