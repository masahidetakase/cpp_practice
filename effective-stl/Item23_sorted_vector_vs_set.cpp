// Command forcompiling. 
// g++ -lstdc++ -std=c++11 -o Item20_sorted_vector_vs_set Item20_sorted_vector_vs_set.cpp

#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <random>
#include <algorithm>
#include <chrono>

using namespace std ;

int main( int argc, char *argv[])
{
    set<int> intSet ;
    vector<int> intVect ;
    const int max_container_size = 10000000 ;

    // create the test data.
    random_device seed_gen ;
    mt19937 random_src( seed_gen()) ;
    uniform_int_distribution<int> distribution( 0, max_container_size -1) ;
    for( size_t idx = 0 ; idx < max_container_size ; ++idx) {
        intSet.insert( distribution( random_src)) ;
    }

    intVect.assign( intSet.begin(), intSet.end()) ; // sorted vector

    // find values
    random_src.seed( seed_gen()) ;
    for( size_t idx = 0 ; idx < 200 ; ++idx) {
        int searchedValue = distribution( random_src) ;

        // The performance of std::set.
        auto startTime = chrono::high_resolution_clock::now() ;
        intSet.find( searchedValue) ; 
        auto endTime = chrono::high_resolution_clock::now() ;
        cout << "intSet.....found:" << searchedValue << " : " << chrono::duration_cast<chrono::microseconds>( endTime - startTime).count() << endl ;

        // The performance of the sorted std::vector.
        startTime = chrono::high_resolution_clock::now() ;
        binary_search( intVect.begin(), intVect.end(), searchedValue) ; // binary search
        endTime = chrono::high_resolution_clock::now() ;
        cout << "intVect....found:" << searchedValue << " : " << chrono::duration_cast<chrono::microseconds>( endTime - startTime).count() << endl ;
    }

    return 0 ;
}  