#include <iostream>
#include <string>


using namespace std ;

// the function to return the argument with no modification.
string noModify( string str)
{
    cout << "noModify's arg. :" << str.c_str() << " at " << (void*)str.data() << endl ;

    return str ;
}


int main( int argc, char *argv[])
{
    string originalStr( "hoge fuga") ;
    cout << "original string:" << originalStr.c_str() << " at " << (void*)originalStr.data() << endl ;

    string copiedStr( originalStr) ;
    cout << "copied string:" << copiedStr.c_str() << " at " << (void*)copiedStr.data() << endl ;

    string returnedStr1( noModify(originalStr)) ;
    cout << "returned string from noModify():" << returnedStr1.c_str() << " at " << (void*)returnedStr1.data() << endl ;

    // Modify the copied string. 
    copiedStr[0] = 'A' ;
    cout << "modified copied string:" << copiedStr.c_str() << " at " << (void*)copiedStr.data() << endl ;

    return 0 ;
}