#include "Location.hpp"

Location::Location( const Location::NumType& x,
                    const Location::NumType& y)
    : m_x( x),
      m_y( y) {}


Location::Location( const Location& src)
    : m_x( src.m_x),
      m_y( src.m_y) {}


Location::~Location() {}


const Location&
Location::operator = ( const Location& src)
{
    m_x = src.m_x ;
    m_y = src.m_y ;

    return *this ;
}


const Location&
Location::operator += ( const Location& rhs)
{
    m_x += rhs.m_x ;
    m_y += rhs.m_y ;

    return *this ;
}

const Location&
Location::operator -= ( const Location& rhs)
{
    m_x -= rhs.m_x ;
    m_y -= rhs.m_y ;

    return *this ;
}



