#pragma once
#include <cstddef>
#include <cassert>
#include <stdexcept>

namespace common
{

class SquareMatrix2D
{
    public:
        typedef double NumType ;

        /// Identity matrix.
        static const SquareMatrix2D Identity ;

        /// Create a zero matrix.
        SquareMatrix2D() ;

        /** 
         * Create a matrix with initial values.
         * @param[in] a1_1  the element at (row, col) = (1,1).
         * @param[in] a1_2  the element at (row, col) = (1,2).
         * @param[in] a2_1  the element at (row, col) = (2,1).
         * @param[in] a2_2  the element at (row, col) = (2,2).
         */ 
        SquareMatrix2D( const NumType& a1_1, const NumType& a1_2,
                        const NumType& a2_1, const NumType& a2_2) ;

        /**
         * Copy constructor.
         * @param[in] src  the source matrix cloned.
         */
        SquareMatrix2D( const SquareMatrix2D& src) ;

        /**
         * Do nothing.
         */
        virtual ~SquareMatrix2D() {}

        /**
         * The operator for substitution.
         * @param[in] rhs  the source matrix cloned.
         */
        const SquareMatrix2D& operator = ( const SquareMatrix2D& rhs) ;

        /**
         * The accessor to the element of the matrix.
         * @param[in] row  the index number of the row.
         * @param[in] col  the index number of the col.
         * @return         the reference to the element of the matrix.
         */
        NumType& operator () ( const size_t& row, const size_t& col) // 
        {
            assert( row < 2 && col < 2) ;
            return m_element[row][col] ;
        }

        /**
         * The comparison operator.
         * "A == B" means "A equals to B".
         * @param[in] rhs  the right value.
         * @return         if A is equal to B, true is returned.
         */
        bool operator == ( const SquareMatrix2D& rhs) ;

        /**
         * The comparison operator.
         * "A != B" means "A is not equal to B".
         * @param[in] rhs  the right value.
         * @return         if A is not equal to B, true is returned.
         */
        bool operator != ( const SquareMatrix2D& rhs) ;

        /**
         * The addition assignment operator.
         * "A += B" means "A = A + B".
         * @param[in] rhs  the right value.
         * @return         the constant reference to the left value.
         */
        const SquareMatrix2D& operator += ( const SquareMatrix2D& rhs) ;

        /**
         * The subtraction assignment operator.
         * "A -= B" means "A = A - B".
         * @param[in] rhs  the right value.
         * @return         the constant reference to the left value.
         */
        const SquareMatrix2D& operator -= ( const SquareMatrix2D& rhs) ;

        /**
         * The multiplication assignment operator.
         * "A *= B" means "A = A * B".
         * @param[in] rhs  the right value as real number.
         * @return         the constant reference to the left value.
         */
        const SquareMatrix2D& operator *= ( const NumType& rhs) ;

        /**
         * The multiplication assignment operator.
         * "A *= B" means "A = A * B".
         * @param[in] rhs  the right value as matrix.
         * @return         the constant reference to the left value.
         */
        const SquareMatrix2D& operator *= ( const SquareMatrix2D& rhs) ;

        /**
         * The Division assignment operator.
         * "A /= B" means "A * 1/B".
         * @param[in] rhs  the right value as real number.
         * @return         the constant reference to the left value.
         */
        const SquareMatrix2D& operator /= ( const NumType& rhs) throw (std::domain_error);

        /**
         * The binary operator for addition(A + B)
         * @param[in] op2  the matrix to add.
         * @return         the result of addition.
         */
        SquareMatrix2D operator + ( const SquareMatrix2D& op2) ;

        /**
         * The binary operator for subtraction(A - B)
         * @param[in] op2  the matrix to subtract.
         * @return         the result of subtraction.
         */
        SquareMatrix2D operator - ( const SquareMatrix2D& op2) ;

        /**
         * The binary operator for multiplication(A * B)
         * @param[in] op2  the real number to multiply.
         * @return         the result of multiplication.
         */
        SquareMatrix2D operator * ( const NumType& op2) ;

        /**
         * The binary operator for multiplication(A * B)
         * @param[in] op2  the matrix to multiply.
         * @return         the result of multiplication.
         */
        SquareMatrix2D operator * ( const SquareMatrix2D& op2) ;

        /**
         * The binary operator for division(A / B)
         * @param[in] op2  the real number to divide each element by.
         * @return         the result of division.
         */
        SquareMatrix2D operator / ( const NumType& op2) throw (std::domain_error);

        /**
         * Calculate the determinant.
         * @return  the determinant.
         */
        NumType det() ;

        /**
         * Calculate the inverse matrix.
         * @return  the inverse matrix.
         */
        SquareMatrix2D inverse() throw (std::domain_error);

    private:
        /// the elements of the matirx. [row][column].
        NumType m_element[2][2] ;
} ;

/**
 * The binary operator for multiplication(A * B)
 * @param[in] op1  the real number to multiply to each element of OP2.
 * @param[in] op2  the real number to be multiplied.
 * @return         the result of multiplication.
 */
SquareMatrix2D
operator * ( const SquareMatrix2D::NumType& op1, const SquareMatrix2D& op2) ;

/**
 * The binary operator for multiplication(A * B)
 * @param[in] op1  the integer number to multiply to each element of OP2.
 * @param[in] op2  the real number to be multiplied.
 * @return         the result of multiplication.
 */
SquareMatrix2D
operator * ( const int& op1, const SquareMatrix2D& op2) ;

} // the end of common namespace
