#pragma once

#include "Location.hpp"

/**
 * GuidanceSystem
 */
class GuidanceSystemBase
{
    public:
        /**
         * @param[in] init_loc  initial location.  
         */
        GuidanceSystemBase( const Location& init_loc)
            : m_cur_location( init_loc),
              m_prev_location( init_loc) {} 

        virtual ~GuidanceSystemBase() {}

        /**
         * Move constructor
         */
        void move( const Location& dest_loc)
        {
            m_prev_location = m_cur_location ;
            move_impl() ;
        }

        const Location& getLocation() const
        {
            return m_cur_location ;
        }

        const Location getVector() 
        {
            return m_cur_location( m_cur_location.getX() - m_prev_location.getX(),
                                   m_cur_location.getY() - m_prev_location.getY())  ;
        }

    private:
        Location m_prev_location ;

        Location m_cur_location ;

        virtual void moveImpl( const Location& dest_loc) = 0 {}
} ;