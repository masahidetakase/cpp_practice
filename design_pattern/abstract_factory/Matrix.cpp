#include "Matrix.hpp"
#include <algorithm>
#include <functional>

namespace common
{

const SquareMatrix2D SquareMatrix2D::Identity ;

using namespace std ;

SquareMatrix2D::SquareMatrix2D()
{
    fill( (NumType*)m_element, (NumType*)m_element + 4, 0.) ;
}


SquareMatrix2D::SquareMatrix2D( const SquareMatrix2D::NumType& a1_1,
                                const SquareMatrix2D::NumType& a1_2,
                                const SquareMatrix2D::NumType& a2_1,
                                const SquareMatrix2D::NumType& a2_2)
{
    m_element[0][0] = a1_1 ;
    m_element[0][1] = a1_2 ;
    m_element[1][0] = a2_1 ;
    m_element[1][1] = a2_2 ;
}


SquareMatrix2D::SquareMatrix2D( const SquareMatrix2D& src)
{
    assert( this != &src) ;

    copy( (NumType*)src.m_element,
          (NumType*)src.m_element + 4,
          (NumType*)m_element) ;
}


const SquareMatrix2D&
SquareMatrix2D::operator = ( const SquareMatrix2D& src)
{
    assert( this != &src) ;

    copy( (NumType*)src.m_element,
          (NumType*)src.m_element + 4,
          (NumType*)m_element) ;
    return *this ;
}


bool
SquareMatrix2D::operator == ( const SquareMatrix2D& rhs)
{
    return ( m_element[0][0] == rhs.m_element[0][0]) &&
           ( m_element[0][1] == rhs.m_element[0][1]) &&
           ( m_element[1][0] == rhs.m_element[1][0]) &&
           ( m_element[1][1] == rhs.m_element[1][1]) ; 
}


bool
SquareMatrix2D::operator != ( const SquareMatrix2D& rhs)
{
    return !(*this == rhs) ; 
}


const SquareMatrix2D&
SquareMatrix2D::operator += ( const SquareMatrix2D& rhs)
{
    transform( (NumType*)m_element,
               (NumType*)m_element + 4,
               (NumType*)rhs.m_element,
               (NumType*)m_element,
               plus<NumType>()) ;

    return *this ;
}


const SquareMatrix2D&
SquareMatrix2D::operator -= ( const SquareMatrix2D& rhs)
{
    transform( (NumType*)m_element,
               (NumType*)m_element + 4,
               (NumType*)rhs.m_element,
               (NumType*)m_element,
               minus<NumType>()) ;

    return *this ;
}


const SquareMatrix2D&
SquareMatrix2D::operator *= ( const SquareMatrix2D::NumType& rhs)
{
    transform( (NumType*)m_element,
               (NumType*)m_element + 4,
               (NumType*)m_element,
               bind2nd( multiplies<NumType>(), rhs)) ;

    return *this ;
}


const SquareMatrix2D&
SquareMatrix2D::operator *= ( const SquareMatrix2D& rhs)
{
    SquareMatrix2D ret ;    

    for( size_t row = 0 ; row < 2 ; ++row) {
        for( size_t col = 0 ; col < 2 ; ++col) {
            ret.m_element[row][col] += m_element[row][0] * rhs.m_element[0][col] ;
            ret.m_element[row][col] += m_element[row][1] * rhs.m_element[1][col] ;
        }
    }

    *this = ret ;

    return *this ;
}


const SquareMatrix2D&
SquareMatrix2D::operator /= ( const SquareMatrix2D::NumType& rhs) throw (std::domain_error)
{
    if( rhs == (SquareMatrix2D::NumType)0)
    {
      throw domain_error( "divide by zero.") ;
    }

    transform( (NumType*)m_element,
               (NumType*)m_element + 4,
               (NumType*)m_element,
               bind2nd( divides<NumType>(), rhs)) ;

    return *this ;
}


SquareMatrix2D
SquareMatrix2D::operator + ( const SquareMatrix2D& op2)
{
    SquareMatrix2D ret ;

    transform( (NumType*)m_element,
               (NumType*)m_element + 4,
               (NumType*)op2.m_element,
               (NumType*)ret.m_element,
               plus<NumType>()) ;

    return ret ;
}


SquareMatrix2D
SquareMatrix2D::operator - ( const SquareMatrix2D& op2)
{
    SquareMatrix2D ret ;

    transform( (NumType*)m_element,
               (NumType*)m_element + 4,
               (NumType*)op2.m_element,
               (NumType*)ret.m_element,
               minus<NumType>()) ;

    return ret ;
}


SquareMatrix2D
SquareMatrix2D::operator * ( const SquareMatrix2D::NumType& op2)
{
    SquareMatrix2D ret ;    

    transform( (NumType*)m_element,
               (NumType*)m_element + 4,
               (NumType*)ret.m_element,
               bind2nd( multiplies<NumType>(), op2)) ;

    return ret ;
}


SquareMatrix2D
SquareMatrix2D::operator * ( const SquareMatrix2D& op2)
{
    SquareMatrix2D ret ;    

    for( size_t row = 0 ; row < 2 ; ++row) {
        for( size_t col = 0 ; col < 2 ; ++col) {
            ret.m_element[row][col] += m_element[row][0] * op2.m_element[0][col] ;
            ret.m_element[row][col] += m_element[row][1] * op2.m_element[1][col] ;
        }
    }

    return ret ;
}


SquareMatrix2D
SquareMatrix2D::operator / ( const SquareMatrix2D::NumType& op2) throw (std::domain_error)
{
    if( op2 == (NumType)0) {
      throw domain_error( "divide by zero.") ;
    }

    SquareMatrix2D ret ;

    transform( (NumType*)m_element,
               (NumType*)m_element + 4,
               (NumType*)ret.m_element,
               bind2nd( divides<NumType>(),op2)) ;

    return ret ;
}


SquareMatrix2D::NumType
SquareMatrix2D::det()
{
    return m_element[0][0] * m_element[1][1] - m_element[0][1] * m_element[1][0] ;
}


SquareMatrix2D
SquareMatrix2D::inverse() throw (std::domain_error)
{
    NumType detA = det() ;
    
    if( detA == (SquareMatrix2D::NumType)0) {
      throw std::domain_error( "no inverse matrix") ;
    }

    SquareMatrix2D ret (  m_element[1][1], -m_element[0][1],
                         -m_element[1][0],  m_element[0][0]) ;

    return ret / detA ; 
}


SquareMatrix2D
operator * ( const SquareMatrix2D::NumType& op1, const SquareMatrix2D& op2)
{
    SquareMatrix2D ret( op2) ;

    return ret *= op1 ;
}


SquareMatrix2D
operator * ( const int& op1, const SquareMatrix2D& op2)
{
    SquareMatrix2D ret( op2) ;

    return ret *= (SquareMatrix2D::NumType)op1 ;
}

} // the end of common namespace
