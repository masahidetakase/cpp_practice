#include <gtest/gtest.h>
#include "../Location.hpp"

TEST( Location, constructor_1)
{
    Location loc ;

    ASSERT_EQ( 0., loc.getX()) ;
    ASSERT_EQ( 0., loc.getY()) ;
}

TEST( Location, constructor_2)
{
    Location loc( 0.1, 0.2) ;

    ASSERT_EQ( 0.1, loc.getX()) ;
    ASSERT_EQ( 0.2, loc.getY()) ;
}

TEST( Location, copy_constructor)
{
    Location loc( 0.1, 0.2) ;
    Location copied_loc( loc) ;

    ASSERT_EQ( 0.1, copied_loc.getX()) ;
    ASSERT_EQ( 0.2, copied_loc.getY()) ;
}

TEST( Location, getX_getY)
{
    Location loc( 0.1, 0.2) ;

    ASSERT_EQ( 0.1, loc.getX()) ;
    ASSERT_EQ( 0.2, loc.getY()) ;
}

TEST( Location, operator_plus_equal)
{
    Location loc( 0.1, 0.2) ;
    Location rhs( 0.2, 0.1) ;

    loc += rhs ;

    ASSERT_EQ( 0.1 + 0.2, loc.getX()) ;
    ASSERT_EQ( 0.2 + 0.1, loc.getY()) ;
}

TEST( Location, operator_minus_equal)
{
    Location loc( 0.1, 0.2) ;
    Location rhs( 0.0, 0.1) ;

    loc -= rhs ;

    ASSERT_EQ( 0.1, loc.getX()) ;
    ASSERT_EQ( 0.2 - 0.1, loc.getY()) ;
}

