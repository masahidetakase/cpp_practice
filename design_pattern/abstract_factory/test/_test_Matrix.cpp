#include "../Matrix.hpp"
#include <gtest/gtest.h>

using namespace common ;

TEST( SquareMatrix2D, constructor_no_arg)
{
    SquareMatrix2D mat2d ;

    ASSERT_EQ( 0., mat2d( 0, 0)) ;
    ASSERT_EQ( 0., mat2d( 0, 1)) ;
    ASSERT_EQ( 0., mat2d( 1, 0)) ;
    ASSERT_EQ( 0., mat2d( 1, 1)) ;
}

TEST( SquareMatrix2D, constructor_with_arg)
{
    SquareMatrix2D mat2d( 1., -2., 3., -4.) ;

    ASSERT_EQ(  1., mat2d( 0, 0)) ;
    ASSERT_EQ( -2., mat2d( 0, 1)) ;
    ASSERT_EQ(  3., mat2d( 1, 0)) ;
    ASSERT_EQ( -4., mat2d( 1, 1)) ;
}

TEST( SquareMatrix2D, copy_constructor)
{
    SquareMatrix2D src_mtx( 1., -2., 3., -4.) ;

    SquareMatrix2D copied_mtx( src_mtx) ;    

    ASSERT_EQ( src_mtx( 0, 0), copied_mtx( 0, 0)) ;
    ASSERT_EQ( src_mtx( 0, 1), copied_mtx( 0, 1)) ;
    ASSERT_EQ( src_mtx( 1, 0), copied_mtx( 1, 0)) ;
    ASSERT_EQ( src_mtx( 1, 1), copied_mtx( 1, 1)) ;
}

TEST( SquareMatrix2D, operator_equal)
{
    SquareMatrix2D src_mtx( 1., -2., 3., -4.) ;

    SquareMatrix2D copied_mtx ;

    copied_mtx = src_mtx ;    

    ASSERT_EQ( src_mtx( 0, 0), copied_mtx( 0, 0)) ;
    ASSERT_EQ( src_mtx( 0, 1), copied_mtx( 0, 1)) ;
    ASSERT_EQ( src_mtx( 1, 0), copied_mtx( 1, 0)) ;
    ASSERT_EQ( src_mtx( 1, 1), copied_mtx( 1, 1)) ;
}

TEST( SquareMatrix2D, operator_equal_to)
{
    SquareMatrix2D mtx2d( 1., 2., 3., 4.) ;

    SquareMatrix2D rhs_mtx_1( 1., 2., 3., 4.) ;

    SquareMatrix2D rhs_mtx_2( 1., 2., 5., 4.) ;

    EXPECT_TRUE( mtx2d == rhs_mtx_1) ;
    EXPECT_FALSE( mtx2d == rhs_mtx_2) ;    
}

TEST( SquareMatrix2D, operator_not_equal_to)
{
    SquareMatrix2D mtx2d( 1., 2., 3., 4.) ;

    SquareMatrix2D rhs_mtx_1( 1., 2., 3., 4.) ;

    SquareMatrix2D rhs_mtx_2( 1., 2., 5., 4.) ;

    EXPECT_FALSE( mtx2d != rhs_mtx_1) ;
    EXPECT_TRUE( mtx2d != rhs_mtx_2) ;    
}

TEST( SquareMatrix2D, operator_plus_equal)
{
    SquareMatrix2D mtx2d( 4., 3., 2., 1.) ;

    SquareMatrix2D rhs_mtx( 1., 2., 3., 4.) ;

    mtx2d += rhs_mtx ;    

    EXPECT_EQ( 5., mtx2d( 0, 0)) ;
    EXPECT_EQ( 5., mtx2d( 0, 1)) ;
    EXPECT_EQ( 5., mtx2d( 1, 0)) ;
    EXPECT_EQ( 5., mtx2d( 1, 1)) ;
}

TEST( SquareMatrix2D, operator_minus_equal)
{
    SquareMatrix2D mtx2d( 4., 3., 2., 1.) ;

    SquareMatrix2D rhs_mtx( 3., 2., 1., 0.) ;

    mtx2d -= rhs_mtx ;    

    EXPECT_EQ( 1., mtx2d( 0, 0)) ;
    EXPECT_EQ( 1., mtx2d( 0, 1)) ;
    EXPECT_EQ( 1., mtx2d( 1, 0)) ;
    EXPECT_EQ( 1., mtx2d( 1, 1)) ;
}

TEST( SquareMatrix2D, operator_multiply_equal_1)
{
    SquareMatrix2D mtx2d( 4., 3., 2., 1.) ;

    mtx2d *= 2. ;

    EXPECT_EQ( 8., mtx2d( 0, 0)) ;
    EXPECT_EQ( 6., mtx2d( 0, 1)) ;
    EXPECT_EQ( 4., mtx2d( 1, 0)) ;
    EXPECT_EQ( 2., mtx2d( 1, 1)) ;
}

TEST( SquareMatrix2D, operator_multiply_equal_2)
{
    SquareMatrix2D mtx2d( 1., 2., 3., 4.) ;

    SquareMatrix2D rhs_mtx( 5., 6., 7., 8.) ;

    mtx2d *= rhs_mtx ;    

    EXPECT_EQ( 19., mtx2d( 0, 0)) ;
    EXPECT_EQ( 22., mtx2d( 0, 1)) ;
    EXPECT_EQ( 43., mtx2d( 1, 0)) ;
    EXPECT_EQ( 50., mtx2d( 1, 1)) ;
}

TEST( SquareMatrix2D, operator_divide_equal)
{
    SquareMatrix2D mtx2d( 1., 2., 3., 4.) ;

    mtx2d /= 0.1 ;    

    EXPECT_EQ( 10., mtx2d( 0, 0)) ;
    EXPECT_EQ( 20., mtx2d( 0, 1)) ;
    EXPECT_EQ( 30., mtx2d( 1, 0)) ;
    EXPECT_EQ( 40., mtx2d( 1, 1)) ;
}

TEST( SquareMatrix2D, operator_divide_equal_by_zero)
{
    SquareMatrix2D mtx2d( 1., 2., 3., 4.) ;

    ASSERT_THROW( mtx2d /= 0., std::domain_error) ;
}

TEST( SquareMatrix2D, operator_plus)
{
    SquareMatrix2D op1_mtx( 1., 2., 3., 4.) ;
    SquareMatrix2D op2_mtx( 4., 3., 2., 1.) ;
    
    SquareMatrix2D result_mtx = op1_mtx + op2_mtx ;

    EXPECT_EQ( 5., result_mtx( 0, 0)) ;
    EXPECT_EQ( 5., result_mtx( 0, 1)) ;
    EXPECT_EQ( 5., result_mtx( 1, 0)) ;
    EXPECT_EQ( 5., result_mtx( 1, 1)) ;
}

TEST( SquareMatrix2D, operator_minus)
{
    SquareMatrix2D op1_mtx( 1., 2., 3., 4.) ;
    SquareMatrix2D op2_mtx( 0., 1., 2., 3.) ;

    SquareMatrix2D result_mtx = op1_mtx - op2_mtx ;

    EXPECT_EQ( 1., result_mtx( 0, 0)) ;
    EXPECT_EQ( 1., result_mtx( 0, 1)) ;
    EXPECT_EQ( 1., result_mtx( 1, 0)) ;
    EXPECT_EQ( 1., result_mtx( 1, 1)) ;
}

TEST( SquareMatrix2D, operator_multiply_1)
{
    SquareMatrix2D op1_mtx( 1., 2., 3., 4.) ;

    SquareMatrix2D result_mtx = op1_mtx * 0.2 ;    

    EXPECT_EQ( 1. * 0.2, result_mtx( 0, 0)) ;
    EXPECT_EQ( 2. * 0.2, result_mtx( 0, 1)) ;
    EXPECT_EQ( 3. * 0.2, result_mtx( 1, 0)) ;
    EXPECT_EQ( 4. * 0.2, result_mtx( 1, 1)) ;
}

TEST( SquareMatrix2D, operator_multiply_2)
{
    SquareMatrix2D op1_mtx( 1., 2., 3., 4.) ;

    SquareMatrix2D op2_mtx( 5., 6., 7., 8.) ;

    SquareMatrix2D result_mtx = op1_mtx * op2_mtx ;    

    EXPECT_EQ( 19., result_mtx( 0, 0)) ;
    EXPECT_EQ( 22., result_mtx( 0, 1)) ;
    EXPECT_EQ( 43., result_mtx( 1, 0)) ;
    EXPECT_EQ( 50., result_mtx( 1, 1)) ;
}

TEST( SquareMatrix2D, operator_divide)
{
    SquareMatrix2D op1_mtx( 1., 2., 3., 4.) ;

    SquareMatrix2D result_mtx = op1_mtx / 2. ;    

    EXPECT_EQ( 0.5, result_mtx( 0, 0)) ;
    EXPECT_EQ( 1.0, result_mtx( 0, 1)) ;
    EXPECT_EQ( 1.5, result_mtx( 1, 0)) ;
    EXPECT_EQ( 2.0, result_mtx( 1, 1)) ;
}

TEST( SquareMatrix2D, operator_divide_by_zero)
{
    SquareMatrix2D op1_mtx( 1., 2., 3., 4.) ;
    ASSERT_THROW( op1_mtx / 0., std::domain_error) ;
}

TEST( SquareMatrix2D, determinant)
{
    SquareMatrix2D mtx2d( 1., 2., 3., 4.) ;

    EXPECT_EQ( -2., mtx2d.det()) ;
}

TEST( SquareMatrix2D, inverse)
{
    SquareMatrix2D mtx2d( 2., 5., 1., 3.) ;

    SquareMatrix2D mtx2d_inverse( mtx2d.inverse()) ;

    EXPECT_EQ(  3., mtx2d_inverse( 0, 0)) ;
    EXPECT_EQ( -5., mtx2d_inverse( 0, 1)) ;
    EXPECT_EQ( -1., mtx2d_inverse( 1, 0)) ;
    EXPECT_EQ(  2., mtx2d_inverse( 1, 1)) ;
}

TEST( SquareMatrix2D, inverse_not_exist)
{
    SquareMatrix2D mtx2d( 2., 1., 2., 1.) ;
    ASSERT_THROW( mtx2d.inverse(), std::domain_error) ;
}

TEST( SquareMatrix2D, operator_multiply_double_matrix)
{
    SquareMatrix2D mtx2d( 1., 2., 3., 4.) ;

    SquareMatrix2D result_mtx = 2. * mtx2d ;

    EXPECT_EQ( 2., result_mtx( 0, 0)) ;
    EXPECT_EQ( 4., result_mtx( 0, 1)) ;
    EXPECT_EQ( 6., result_mtx( 1, 0)) ;
    EXPECT_EQ( 8., result_mtx( 1, 1)) ;
}

TEST( SquareMatrix2D, operator_multiply_int_matrix)
{
    SquareMatrix2D mtx2d( 1., 2., 3., 4.) ;

    SquareMatrix2D result_mtx = 2 * mtx2d ;

    EXPECT_EQ( 2., result_mtx( 0, 0)) ;
    EXPECT_EQ( 4., result_mtx( 0, 1)) ;
    EXPECT_EQ( 6., result_mtx( 1, 0)) ;
    EXPECT_EQ( 8., result_mtx( 1, 1)) ;
}
