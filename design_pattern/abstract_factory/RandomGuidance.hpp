#include "GuidanceSystemBase.hpp"
#include <Random>

class RandomGuidance : public GuidanceSystemBase
{
    public:
        RandomGuidance( const Location& init_loc,
                        const double& speed = 1.) ;

        virtual ~RandomGuidance() ;

    private:
        virtual void moveImpl( const Location& dest_loc) ;

        double periodicFunc( const double& x) ;

        static const double MAX_STEER = 30. ;

        std::random_device m_rand_dev ;

        std::uniform_real_distribution<double> m_steer ;

        double m_speed ;

        double m_move_distance_online ;
} ;


