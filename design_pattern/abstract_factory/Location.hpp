#pragma once

class Location
{
    public:
        typedef double NumType ;

        Location( const NumType& x = 0., const NumType& y = 0.) ;
       
        virtual ~Location() ;

        Location( const Location& src) ;

        const Location& operator = ( const Location& src) ;

        const NumType& getX() const { return m_x ;}
        
        const NumType& getY() const { return m_y ;}

        const Location& operator += ( const Location& rhs) ;

        const Location& operator -= ( const Location& rhs) ;

    private:
        double m_x, m_y ;
} ;
