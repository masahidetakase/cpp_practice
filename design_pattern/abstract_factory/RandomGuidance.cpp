#include "RandomGuidance.hpp"
#include <random>
#include <chrono>

RandomGuidance::RandomGuidance( const Location& inti_loc,
                                const double& speed)
    : GuidanceSystemBase( init_loc),
      m_steer( 0, MAX_STEER),
      m_speed( speed)
      m_move_distance_online( 0.)
{
}


void
RandomGuidance::moveImpl()
{
    m_move_distance_online += speed ;
}


double
RandomGuidance::periodicFunc( const double& x)
{
    static const int half_period = 50 ;

    double p_x = x % half_period ;
    
    if( p_x < 25)
}