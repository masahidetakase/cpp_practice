#pragma once

#include "PixelIteratorBase.hpp"

// 24bitRGB画像用イテレーター
class PixelIterator24bitRGB : public PixelIteratorBase
{
    public:
        PixelIterator24bitRGB( const BitmapImage& bitmap_img,
                               const ROI& roi = ROI()) ;

        PixelIterator24bitRGB( const PixelIterator24bitRGB& src) ;

        PixelIterator24bitRGB( const PixelIterator24bitRGB&& src) ;

        ~PixelIterator24bitRGB() ;

        PixelIterator24bitRGB&
        operator = ( const PixelIterator24bitRGB& src) ;

        PixelIterator24bitRGB&
        operator ++ () ;

        PixelIterator24bitRGB&
        operator -- () ;

        PixelIterator24bitRGB&
        operator += ( const unsigned int& step) ;

        PixelIterator24bitRGB&
        operator -= ( const unsigned int & step) ;

        Pixel
        operator * () const ;

        const size_t
        getBpp() const { return BIT_PER_PIXEL ;}

        void
        set( const Pixel& val) ;

        bool
        isValid() const ;

    private:
        const size_t m_line_size_byte ;
        std::uint8_t* const m_roi_begin;
        std::uint8_t* const m_roi_end ;

        std::uint8_t* m_cur_pixel ;

        std::uint8_t* m_cur_line_begin ;
        std::uint8_t* m_cur_line_end ;

        bool m_is_valid ;

        static const size_t BIT_PER_PIXEL = 24 ;

        static const size_t BYTE_PER_PIXEL = 3 ;

        static const std::uint16_t MAX_CHANNEL_VALUE = 255 ; // 8bit, 0x00FF

} ;
