#include "BitmapImage.hpp"
#include "my_utility.hpp"
#include <string>
#include <utility>
#include <stdexcept>
#include <cstring>
#include <cstdint>

using namespace std ;
using namespace my_utility ;

const unsigned int
PxlFormat2BppTable[] = {
    24U, // RGB24BIT,
    36U, // RGB36BIT,
    48U, // RGB48BIT,

    0U   // terminus
} ;


BitmapImage::BitmapImage( const unsigned int& width,
                          const unsigned int& height,
                          const PixelFormat& pxl_format,
                          void* const raw_data)
    : m_width( width),
     m_height( height),
     m_pxl_format( pxl_format),
     m_bit_per_pixel( PxlFormat2BppTable[ (size_t)pxl_format]),
     m_line_length_byte( ( width * m_bit_per_pixel + 7) / 8)
{
    //preconditions
    expect_invariant( ( 0 < m_width) && ( 0 < m_height), "Invalid geometry") ;
    expect_invariant( m_pxl_format < PixelFormat::NUM_FORMATS, "Unknown format") ;

    if( raw_data != nullptr) {
        m_raw_data.reset( static_cast<uint8_t*>(raw_data), dumy_delete<uint8_t[]>()) ;
    } else {
        m_raw_data.reset( new uint8_t[ m_line_length_byte * height], default_delete<uint8_t[]>()) ;
        memset( m_raw_data.get(), 0, m_line_length_byte * height) ;
    }
}


BitmapImage::BitmapImage( const BitmapImage& src)
    : m_width( src.m_width),
      m_height( src.m_height),
      m_pxl_format( src.m_pxl_format),
      m_bit_per_pixel( src.m_bit_per_pixel),
      m_line_length_byte( src.m_line_length_byte),
      m_raw_data( src.m_raw_data) {}


BitmapImage::BitmapImage( BitmapImage&& src)
    : m_width( src.m_width),
      m_height( src.m_height),
      m_pxl_format( src.m_pxl_format),
      m_bit_per_pixel( src.m_bit_per_pixel),
      m_line_length_byte( src.m_line_length_byte),
      m_raw_data( src.m_raw_data) {}


BitmapImage::~BitmapImage() {}


size_t
BitmapImage::getRawDataSize() const
{
    return m_line_length_byte * m_height ;
}


BitmapImage
BitmapImage::clone() const
{
    BitmapImage cloned_image( m_height, m_width, m_pxl_format) ;

    memcpy( cloned_image.m_raw_data.get(), m_raw_data.get(), getRawDataSize()) ;

    return std::move( cloned_image) ;
}
