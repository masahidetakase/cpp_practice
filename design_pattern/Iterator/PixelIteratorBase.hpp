#pragma once
#include "BitmapImage.hpp"
#include <stdexcept>
#include <utility>
#include <cassert>

// Invalid coordinate value.
static const unsigned int INVALID_COORDINATE = (unsigned int)-1 ;

// A point in the image.
struct Point
{
    Point( unsigned int _x = INVALID_COORDINATE,
           unsigned int _y = INVALID_COORDINATE)
        : x( _x),
          y( _y) {}

    Point&
    operator += ( const Point& rhs)
    {
        x += rhs.x ;
        y += rhs.y ;
        return  *this ;
    }

    Point&
    operator -= ( const Point& rhs)
    {
        assert( ( rhs.x <= x) && ( rhs.y <= y)) ;
        x -= rhs.x ;
        y -= rhs.y ;
        return  *this ;
    }

    Point
    operator + ( const Point& rhs)
    {
        return  Point( x + rhs.x, y + rhs.y) ;
    }

    Point
    operator - ( const Point& rhs)
    {
        assert( ( rhs.x <= x) && ( rhs.y <= y)) ;
        return Point( x - rhs.x, y - rhs.y) ;
    }

    unsigned int x, y ;
} ;


// Region of interest.(関心領域)
struct ROI
{
    public:
    ROI( const Point& _top_left, const Point& _bottom_right)
        : top_left( _top_left),
          bottom_right( _bottom_right)
    {
        assert(  top_left.x <= bottom_right.x) ;
        assert(  top_left.y <= bottom_right.y) ;
    }

    ROI()
        : top_left( INVALID_COORDINATE, INVALID_COORDINATE),
          bottom_right( INVALID_COORDINATE, INVALID_COORDINATE) {}

    unsigned int
    getWidth() const
    {
        return bottom_right.x - top_left.x + 1 ;
    }

    unsigned int
    getHeight() const
    {
        return bottom_right.y - top_left.y + 1 ;
    }

    Point top_left, bottom_right ;
} ;


// 関心領域内を走査するイテレーターの抽象クラス
class PixelIteratorBase
{
    public:
        // コンストラクタ。対象画像とその関心領域（ROI）を設定する。
        PixelIteratorBase( const BitmapImage& bitmap_img, const ROI& roi = ROI())
            : m_bitmap_img( bitmap_img),
              m_raw_data( bitmap_img.getRawData().lock()),
              m_roi( roi)
        {
            if( m_roi.top_left.x == INVALID_COORDINATE) {
                m_roi.top_left.x     = 0 ;
                m_roi.top_left.y     = 0 ;
                m_roi.bottom_right.x = m_bitmap_img.getWidth() -1 ;
                m_roi.bottom_right.y = m_bitmap_img.getHeight() -1 ;
            } else if( ( m_bitmap_img.getWidth() <= m_roi.top_left.x) ||
                       ( m_bitmap_img.getHeight() <= m_roi.top_left.x) ||
                       ( m_bitmap_img.getWidth() <= m_roi.bottom_right.x) ||
                       ( m_bitmap_img.getHeight() <= m_roi.bottom_right.y))
            {
                throw std::domain_error( "PixelIteratorBase: ROI is not included in the image area") ;
            }
        }


        PixelIteratorBase( const PixelIteratorBase& src)
            : m_bitmap_img( src.m_bitmap_img),
              m_raw_data( src.m_raw_data),
              m_roi( src.m_roi) {}


        PixelIteratorBase( const PixelIteratorBase&& src)
            : m_bitmap_img( src.m_bitmap_img),
              m_raw_data( src.m_bitmap_img.getRawData().lock()),
              m_roi( src.m_roi) {}

        // コピーコンストラクタ
        PixelIteratorBase&
        operator = ( const PixelIteratorBase& src)
        {
            this->m_bitmap_img = src.m_bitmap_img ;
            this->m_raw_data = src.m_raw_data ;
            this->m_roi = src.m_roi ;

            return *this ;
        }


        virtual ~PixelIteratorBase() {}

        //イテレーターを1つ前進させる
        virtual PixelIteratorBase&
        operator ++ () = 0;

        // イテレーターを1つ後退させる
        virtual PixelIteratorBase&
        operator -- () = 0;

        // イテレーターを複数回前進させる
        virtual PixelIteratorBase&
        operator += ( const unsigned int& step) = 0 ;

        // イテレーターを複数回後退させる
        virtual PixelIteratorBase&
        operator -= ( const unsigned int& step) = 0 ;

        // 画素値を取得する。
        virtual Pixel
        operator * () const = 0 ;

        // ビット深度を取得する
        virtual const size_t
        getBpp() const = 0 ;

        // 画素値をセットする。
        virtual void
        set( const Pixel& val) = 0 ;

        // このイテレーターは有効か？
        virtual bool
        isValid() const = 0 ;

    protected:
        BitmapImage m_bitmap_img ;

        std::shared_ptr<std::uint8_t> m_raw_data ;

        ROI m_roi ;
} ;
