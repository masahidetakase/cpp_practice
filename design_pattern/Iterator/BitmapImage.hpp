#pragma once
#include <memory>
#include <cstdint>
#include <utility>

// ピクセルのフォーマット
enum struct PixelFormat : unsigned int
{
    //RGB
    RGB24BIT = 0,
    RGB36BIT,
    RGB48BIT, // イテレーターは未実装。やってみましょう。

    NUM_FORMATS
} ;


// The conversion table from PixelFormat to bit per pixel(bpp).
extern const unsigned int PxlFormat2BppTable[] ;


// Pixel value
struct Pixel
{
    std::uint16_t red   ;
    std::uint16_t green ;
    std::uint16_t blue  ;

    Pixel()
        : red( 0),
          green( 0),
          blue( 0) {}

    Pixel( const std::uint16_t& red_val,
           const std::uint16_t& green_val,
           const std::uint16_t& blue_val)
        : red( red_val),
          green( green_val),
          blue( blue_val) {}
} ;


/* RGB画像を保持するクラス。コピーコンストラクタ、代入演算子でのコピーは画像データを
 * 複製しないshallow copy。Deep copy はclone()メソッドを用いて明示的に行う。 */
class  BitmapImage
{
    public:

        BitmapImage( const unsigned int& width,
                     const unsigned int& height,
                     const PixelFormat& pxl_format,
                     void* const raw_data = nullptr) ;

        BitmapImage( const BitmapImage& src) ;

        // Move constructor
        BitmapImage( BitmapImage&& src) ;

        virtual ~BitmapImage() ;

        BitmapImage&
        operator = ( const BitmapImage& src)
        {
            BitmapImage temp( src) ;

            return *this = std::move( temp) ;
        }

        const unsigned int&
        getWidth() const { return m_width ;}

        const unsigned int&
        getHeight() const { return m_height ;}

        const PixelFormat&
        getFormat() const { return m_pxl_format ;}

        std::weak_ptr<std::uint8_t>
        getRawData() const {
            return std::weak_ptr<std::uint8_t>( m_raw_data) ;
        }

        size_t
        getRawDataSize() const ;

        // Unlike copy the operator equal,
        BitmapImage clone() const ;

    private:
        unsigned int m_width, m_height ; // the geometry

        PixelFormat m_pxl_format ; // Image formats.

        unsigned int m_bit_per_pixel ; // 24, 36 or 48 bit RGB

        size_t m_line_length_byte ;

        std::shared_ptr<std::uint8_t> m_raw_data ; // the raw bitmap data of the image
} ;
