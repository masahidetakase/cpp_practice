#pragma once

#include "PixelIteratorBase.hpp"

class PixelIterator36bitRGB : public PixelIteratorBase
{
    public:
        PixelIterator36bitRGB( const BitmapImage& bitmap_img,
                               const ROI& roi = ROI()) ;

        PixelIterator36bitRGB( const PixelIterator36bitRGB& src) ;

        PixelIterator36bitRGB( const PixelIterator36bitRGB&& src) ;

        ~PixelIterator36bitRGB() ;

        PixelIterator36bitRGB&
        operator = ( const PixelIterator36bitRGB& src) ;

        PixelIterator36bitRGB&
        operator ++ () ;

        PixelIterator36bitRGB&
        operator -- () ;

        PixelIterator36bitRGB&
        operator += ( const unsigned int& step) ;

        PixelIterator36bitRGB&
        operator -= ( const unsigned int & step) ;

        Pixel
        operator * () const ;

        const size_t
        getBpp() const { return BIT_PER_PIXEL ;}

        virtual void
        set( const Pixel& val) ;

        bool
        isValid() const ;

    private:

        Point m_cur_loc ;

        bool m_is_valid ;

        const size_t m_line_size_byte ;

        static const size_t BIT_PER_PIXEL = 36 ;

        static const size_t PIXEL_PACK_SIZE_BYTE = 9 ;

        static const size_t NUM_PIXEL_PER_PACK = 2 ;

        static const std::uint16_t MAX_CHANNEL_VALUE = 4095 ; // 12bit, 0x0FFF

} ;