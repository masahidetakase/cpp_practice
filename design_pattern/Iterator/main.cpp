#include "PixelIterator24bitRGB.hpp"
#include "PixelIterator36bitRGB.hpp"
#include <fstream>
#include <sstream>
#include <iostream>
#include <string>
#include <memory>

using namespace std ;

/* RAW形式の画像を読み込み、BitmapImageオブジェクトを生成する。 */
static BitmapImage
loadRawImage( const unsigned int& width,
              const unsigned int& height,
              const unsigned int& bit_per_px,
              const string& raw_file_path) ;


/* BitmapImageオブジェクトとROI（関心領域）から、画素アクセス用のイテレーターを作成する。*/
static shared_ptr<PixelIteratorBase>
createPixelIterator( const BitmapImage& bitmap_img,
                     const ROI& roi) ;

/*イテレーターを先に進めながら階調を反転する。*/
static void
reverseImage( PixelIteratorBase& pxl_iter) ;

/* BitmapImageをRAW形式でファイルに保存する。 */
static void
saveRawImage( const BitmapImage& bitmap_img,
              const string& raw_file_path) ;


/*
 * argv[1]: source raw image file.
 * argv[2]: width
 * argv[3]: height
 * argv[4]: bit per pixel（24,36 only）
 * argv[5]: output raw image file
 */
int main( int argc, char *argv[])
{
    if( argc != 6) {
        cerr << "Error: invalid options." << std::endl ;
        return -1 ;
    }

    // コマンドラインから画像ファイル名、サイズ、ピクセル深度を取得。
    string raw_img_path( argv[1]) ;
    unsigned int width = stoul( argv[2]) ;
    unsigned int height = stoul( argv[3]) ;
    unsigned int bit_per_pxl = stoul( argv[4]) ;
    string output_img_path( argv[5]) ;

    // 画像を読み込む。
    BitmapImage src_img =
        loadRawImage( width, height, bit_per_pxl, raw_img_path) ;

    // 画像中央に関心領域を設定。
    ROI roi( Point( width / 4, height / 4),
             Point( width * 3 / 4, height * 3 / 4)) ;

    // 対象画像のROI内のみを操作するイテレーターを作成する。
    shared_ptr<PixelIteratorBase> pxl_iter =
        createPixelIterator( src_img, roi) ;

    // イテレーターを進めながら、それが指す画素を反転する。
    reverseImage( *pxl_iter) ;

    // 画層を保存する
    saveRawImage( src_img, output_img_path) ;

    return 0 ;
}

/* RAW形式の画像を読み込み、BitmapImageオブジェクトを生成する。 */
static BitmapImage
loadRawImage( const unsigned int& width,
              const unsigned int& height,
              const unsigned int& bit_per_pxl,
              const string& raw_file_path)
{
    PixelFormat pxl_format ;

    if( bit_per_pxl == 24) {
        pxl_format = PixelFormat::RGB24BIT ;
    } else if( bit_per_pxl == 36) {
        pxl_format = PixelFormat::RGB36BIT ;
    } else {
        pxl_format = PixelFormat::RGB24BIT ;
    }

    fstream rawimg_stream( raw_file_path.c_str(),
                           fstream::in | fstream::binary) ;

    BitmapImage bitmap_img( width, height, pxl_format) ;

    char* rawimg_buf = reinterpret_cast<char*>( bitmap_img.getRawData().lock().get()) ;

    rawimg_stream.read( rawimg_buf, bitmap_img.getRawDataSize()) ;

    return bitmap_img ;
}


/* BitmapImageオブジェクトとROI（関心領域）から、画素アクセス用のイテレーターを作成する。*/
static shared_ptr<PixelIteratorBase>
createPixelIterator( const BitmapImage& bitmap_img,
                     const ROI& roi)
{
    shared_ptr<PixelIteratorBase> pxl_iter ;

    if( bitmap_img.getFormat() == PixelFormat::RGB24BIT) {
        pxl_iter.reset( new PixelIterator24bitRGB( bitmap_img, roi)) ;
    } else if( bitmap_img.getFormat() == PixelFormat::RGB36BIT) {
        pxl_iter.reset( new PixelIterator36bitRGB( bitmap_img, roi)) ;
    } else {
        pxl_iter.reset( new PixelIterator24bitRGB( bitmap_img, roi)) ;
    }

    return pxl_iter ;
}


/*イテレーターを先に進めながら階調を反転する。*/
static void
reverseImage( PixelIteratorBase& pxl_iter)
{
    Pixel pxl_val ;

    const uint16_t max_pxl_val = ((uint16_t)-1) >> ( 16 - pxl_iter.getBpp()/3) ;

    for( ; pxl_iter.isValid() ; ++pxl_iter) {
        pxl_val = *pxl_iter ;

         // inversing tone.
        pxl_val.red ^= max_pxl_val ;
        pxl_val.green ^= max_pxl_val ;
        pxl_val.blue ^= max_pxl_val ;

        pxl_iter.set( pxl_val) ;
    }
}

/* BitmapImageをRAW形式でファイルに保存する。 */
static void
saveRawImage( const BitmapImage& bitmap_img,
              const string& raw_file_path)
{
    fstream rawimg_stream( raw_file_path.c_str(),
                           fstream::out | fstream::binary) ;

    rawimg_stream.write( (char*)( bitmap_img.getRawData().lock().get()),
                          bitmap_img.getRawDataSize()) ;

    rawimg_stream.close() ;

}
