#pragma once
#include <string>
#include <stdexcept>
namespace my_utility
{

inline void
expect_invariant( const bool& expression, const std::string& err_msg = "")
{
    if( !expression) {
        throw std::logic_error( err_msg) ;
    }
}

template<class T>
struct dumy_delete
{
    dumy_delete() {}

    void operator () (T* ptr) {}  // do nothing.
} ;

template<class T>
struct dumy_delete<T[]>
{
    dumy_delete() {}

    void operator () (T* array) {}  // do nothing.
} ;


} // the end of my_utility namespace