#include "PixelIterator24bitRGB.hpp"
#include <stdexcept>
#include <cassert>

using namespace std ;

PixelIterator24bitRGB::PixelIterator24bitRGB( const BitmapImage& bitmap_img,
                                              const ROI& roi)
    : PixelIteratorBase( bitmap_img, roi),
      m_line_size_byte( bitmap_img.getWidth() * BYTE_PER_PIXEL),
      m_roi_begin( m_raw_data.get() +
                   BYTE_PER_PIXEL * m_roi.top_left.x +
                 m_line_size_byte * m_roi.top_left.y),
      m_roi_end( m_raw_data.get() +
                 BYTE_PER_PIXEL * m_roi.bottom_right.x +
                 m_line_size_byte * m_roi.bottom_right.y + BYTE_PER_PIXEL),
      m_cur_pixel( m_roi_begin),
      m_cur_line_begin( m_roi_begin),
      m_cur_line_end( m_raw_data.get() +
                      BYTE_PER_PIXEL * m_roi.bottom_right.x +
                      m_line_size_byte * m_roi.top_left.y + BYTE_PER_PIXEL),
      m_is_valid( true) {}


PixelIterator24bitRGB::PixelIterator24bitRGB( const PixelIterator24bitRGB& src)
    : PixelIteratorBase( src.m_bitmap_img, src.m_roi),
      m_line_size_byte( src.m_line_size_byte),
      m_roi_begin( src.m_roi_begin),
      m_roi_end( src.m_roi_end),
      m_cur_pixel( src.m_cur_pixel),
      m_cur_line_begin( m_roi_begin),
      m_cur_line_end( src.m_cur_line_begin),
      m_is_valid( src.m_cur_line_end) {}


PixelIterator24bitRGB::PixelIterator24bitRGB( const PixelIterator24bitRGB&& src)
    : PixelIteratorBase( src.m_bitmap_img, src.m_roi),
      m_line_size_byte( src.m_line_size_byte),
      m_roi_begin( src.m_roi_begin),
      m_roi_end( src.m_roi_end),
      m_cur_pixel( src.m_cur_pixel),
      m_cur_line_begin( m_roi_begin),
      m_cur_line_end( src.m_cur_line_begin),
      m_is_valid( src.m_cur_line_end) {}


PixelIterator24bitRGB::~PixelIterator24bitRGB() {}


PixelIterator24bitRGB&
PixelIterator24bitRGB::operator = ( const PixelIterator24bitRGB& src)
{
    PixelIterator24bitRGB temp( src) ;

    return *this = std::move( temp) ;
}


PixelIterator24bitRGB&
PixelIterator24bitRGB::operator ++ ()
{
    if( !m_is_valid) { return *this ;}

    m_cur_pixel += BYTE_PER_PIXEL ;
    if( m_cur_pixel == m_cur_line_end) {
        m_cur_line_begin += m_line_size_byte ;
        m_cur_line_end += m_line_size_byte ;
        m_cur_pixel = m_cur_line_begin ;
    }

    if( m_roi_end <= m_cur_pixel) {
        m_is_valid = false ;
    }

    return *this ;
}


PixelIterator24bitRGB&
PixelIterator24bitRGB::operator -- ()
{
    if( !m_is_valid) { return *this ;}

    if( m_cur_pixel == m_cur_line_begin) {
        if( m_cur_line_begin == m_roi_begin) {
            m_is_valid = false ;
        } else {
            m_cur_line_begin -= m_line_size_byte ;
            m_cur_line_end -= m_line_size_byte ;
            m_cur_pixel = m_cur_line_end - BYTE_PER_PIXEL ;
        }
    } else {
        m_cur_pixel -= BYTE_PER_PIXEL ;
    }

    return *this ;
}


PixelIterator24bitRGB&
PixelIterator24bitRGB::operator += ( const unsigned int& step)
{
    if( !m_is_valid) { return *this ;}

    for( unsigned int sidx = 0 ; sidx < step ; ++sidx) {
        m_cur_pixel += BYTE_PER_PIXEL ;
        if( m_cur_pixel == m_cur_line_end) {
            m_cur_line_begin += m_line_size_byte ;
            m_cur_line_end += m_line_size_byte ;
            m_cur_pixel = m_cur_line_begin ;
        }

        if( m_roi_end <= m_cur_pixel) {
            m_is_valid = false ;
            break ;
        }
    }

    return *this ;
}


PixelIterator24bitRGB&
PixelIterator24bitRGB::operator -= ( const unsigned int& step)
{
    if( !m_is_valid) { return *this ;}

    for( unsigned int sidx = 0 ; sidx < step ; ++sidx) {
        if( m_cur_pixel == m_cur_line_begin) {
            if( m_cur_line_begin == m_roi_begin) {
                m_is_valid = false ;
                break ;
            } else {
                m_cur_line_begin -= m_line_size_byte ;
                m_cur_line_end -= m_line_size_byte ;
                m_cur_pixel = m_cur_line_end - BYTE_PER_PIXEL ;
            }
        } else {
            m_cur_pixel -= BYTE_PER_PIXEL ;
        }
    }

    return *this ;
}


Pixel
PixelIterator24bitRGB::operator * () const
{
    if( !m_is_valid) {
        throw std::domain_error( "PixelIterator24bitRGB: Invalid iterator") ;
    }

    Pixel ret ;

    ret.red   = *m_cur_pixel ;
    ret.green = *( m_cur_pixel + 1) ;
    ret.blue  = *( m_cur_pixel + 2) ;

    return std::move( ret) ;
}


void
PixelIterator24bitRGB::set( const Pixel& val)
{
    //preconditions
    assert( val.red   <= MAX_CHANNEL_VALUE) ;
    assert( val.green <= MAX_CHANNEL_VALUE) ;
    assert( val.blue  <= MAX_CHANNEL_VALUE) ;

    if( !m_is_valid) {
        throw std::domain_error( "PixelIterator24bitRGB: Invalid iterator") ;
    }

    *m_cur_pixel = val.red ;
    *( m_cur_pixel + 1) = val.green ;
    *( m_cur_pixel + 2) = val.blue ;
}


bool
PixelIterator24bitRGB::isValid() const
{
    return m_is_valid ;
}

