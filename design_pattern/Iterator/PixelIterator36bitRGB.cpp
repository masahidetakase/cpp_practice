#include "PixelIterator36bitRGB.hpp"
#include <stdexcept>
#include <utility>
#include <cassert>

using namespace std ;

PixelIterator36bitRGB::PixelIterator36bitRGB( const BitmapImage& bitmap_img,
                                              const ROI& roi)
    : PixelIteratorBase( bitmap_img, roi),
      m_cur_loc( m_roi.top_left),
      m_is_valid( true),
      m_line_size_byte( ( bitmap_img.getWidth() * BIT_PER_PIXEL + 8 - 1) / 8) {}


PixelIterator36bitRGB::PixelIterator36bitRGB( const PixelIterator36bitRGB& src)
    : PixelIteratorBase( src.m_bitmap_img, src.m_roi),
      m_cur_loc( src.m_roi.top_left),
      m_is_valid( src.m_is_valid),
      m_line_size_byte( src.m_line_size_byte) {}


PixelIterator36bitRGB::PixelIterator36bitRGB( const PixelIterator36bitRGB&& src)
    : PixelIteratorBase( src.m_bitmap_img, src.m_roi),
      m_cur_loc( src.m_roi.top_left),
      m_is_valid( src.m_is_valid),
      m_line_size_byte( src.m_line_size_byte) {}


PixelIterator36bitRGB::~PixelIterator36bitRGB() {}


PixelIterator36bitRGB&
PixelIterator36bitRGB::operator = ( const PixelIterator36bitRGB& src)
{
    PixelIterator36bitRGB temp( src) ;

    return *this = std::move( temp) ;
}


PixelIterator36bitRGB&
PixelIterator36bitRGB::operator ++ ()
{
    if( !m_is_valid) { return *this ;}

    if( m_roi.bottom_right.x < ++m_cur_loc.x) {
        ++m_cur_loc.y ;
        m_cur_loc.x = m_roi.top_left.x ;
    }

    if( m_roi.bottom_right.y < m_cur_loc.y) {
        m_is_valid = false ;
    }

    return *this ;
}


PixelIterator36bitRGB&
PixelIterator36bitRGB::operator -- ()
{
    if( !m_is_valid) { return *this ;}

    // Relative location
    Point rel_cur_loc( m_cur_loc - m_roi.top_left) ;

    // Linearizing the relative locatoion.
    unsigned int rel_cur_linear_loc =
        rel_cur_loc.x + m_roi.getWidth() * rel_cur_loc.y ;

    if( rel_cur_linear_loc == 0) {
        m_is_valid = false ;
        return *this ;
    }

    --rel_cur_linear_loc ;

    rel_cur_loc.y = rel_cur_linear_loc / m_roi.getWidth() ;
    rel_cur_loc.x = rel_cur_linear_loc - rel_cur_loc.y * m_roi.getWidth() ;

    m_cur_loc += rel_cur_loc ;

    return *this ;
}


PixelIterator36bitRGB&
PixelIterator36bitRGB::operator += ( const unsigned int& step)
{
    if( !m_is_valid) { return *this ;}

    // Relative location
    Point rel_cur_loc( m_cur_loc - m_roi.top_left) ;

    // Linearizing the relative locatoion.
    unsigned int rel_cur_linear_loc =
        rel_cur_loc.x + m_roi.getWidth() * rel_cur_loc.y ;

    rel_cur_linear_loc += step ;

    rel_cur_loc.y = rel_cur_linear_loc / m_roi.getWidth() ;
    rel_cur_loc.x = rel_cur_linear_loc - rel_cur_loc.y * m_roi.getWidth() ;

    m_cur_loc = m_roi.top_left + rel_cur_loc ;

    if( m_roi.bottom_right.y < m_cur_loc.y) {
        m_is_valid = false ;
    }

    return *this ;
}


PixelIterator36bitRGB&
PixelIterator36bitRGB::operator -= ( const unsigned int& step)
{
    if( !m_is_valid) { return *this ;}

    // Relative location
    Point rel_cur_loc( m_cur_loc - m_roi.top_left) ;

    // Linearizing the relative locatoion.
    unsigned int rel_cur_linear_loc =
        rel_cur_loc.x + m_roi.getWidth() * rel_cur_loc.y ;

    if( rel_cur_linear_loc < step) {
        m_is_valid = false ;
    } else {
        rel_cur_linear_loc -= step ;
    }

    rel_cur_loc.y = rel_cur_linear_loc / m_roi.getWidth() ;
    rel_cur_loc.x = rel_cur_linear_loc - rel_cur_loc.y * m_roi.getWidth() ;

    m_cur_loc = m_roi.top_left + rel_cur_loc ;

    return *this ;
}


Pixel
PixelIterator36bitRGB::operator * () const
{
    if( !m_is_valid) {
        throw std::domain_error( "PixelIterator36bitRGB: Invalid iterator") ;
    }

    std::uint8_t* cur_pxl_pack = m_raw_data.get() + m_line_size_byte * m_cur_loc.y ;
    cur_pxl_pack += ( m_cur_loc.x / NUM_PIXEL_PER_PACK) * PIXEL_PACK_SIZE_BYTE ;

    Pixel ret ;

    if( ( m_cur_loc.x & 0x01u) == 0) {
        ret.red   = ( *(uint32_t*)cur_pxl_pack & 0x00000FFFu) ;
        ret.green = ( *(uint32_t*)cur_pxl_pack & 0x00FFF000u) >> 12 ;
        cur_pxl_pack += 2 ;
        ret.blue  = ( *(uint32_t*)cur_pxl_pack & 0x000FFF00u) >>  8 ;
    } else {
        cur_pxl_pack += 2 ;
        ret.red   = ( *(uint32_t*)cur_pxl_pack & 0xFFF00000u) >> 20 ;
        cur_pxl_pack += 3 ;
        ret.green = ( *(uint32_t*)cur_pxl_pack & 0x000FFF00u) >>  8 ;
        ret.blue  = ( *(uint32_t*)cur_pxl_pack & 0xFFF00000u) >> 20 ;
    }

    return std::move( ret) ;
}


void
PixelIterator36bitRGB::set( const Pixel& val)
{
    //preconditions
    assert( val.red   <= MAX_CHANNEL_VALUE) ;
    assert( val.green <= MAX_CHANNEL_VALUE) ;
    assert( val.blue  <= MAX_CHANNEL_VALUE) ;

    if( !m_is_valid) {
        throw std::domain_error( "PixelIterator36bitRGB: Invalid iterator") ;
    }

    std::uint8_t* cur_pxl_pack = m_raw_data.get() + m_line_size_byte * m_cur_loc.y ;
    cur_pxl_pack += ( m_cur_loc.x / NUM_PIXEL_PER_PACK) * PIXEL_PACK_SIZE_BYTE ;


    if( ( m_cur_loc.x & 0x01u) == 0) {
        *(uint16_t*)cur_pxl_pack &= 0xF000u ;
        *(uint16_t*)cur_pxl_pack += val.red ;

        cur_pxl_pack += 1 ;
        *(uint16_t*)cur_pxl_pack &= 0x000Fu ;
        *(uint16_t*)cur_pxl_pack += val.green << 8 ;

        cur_pxl_pack += 2 ;
        *(uint16_t*)cur_pxl_pack &= 0xF000u ;
        *(uint16_t*)cur_pxl_pack += val.blue ;
    } else {
        cur_pxl_pack += 4 ;
        *(uint16_t*)cur_pxl_pack &= 0x000Fu ;
        *(uint16_t*)cur_pxl_pack += val.red << 8 ;

        cur_pxl_pack += 2 ;
        *(uint16_t*)cur_pxl_pack &= 0xF000u ;
        *(uint16_t*)cur_pxl_pack += val.green ;

        cur_pxl_pack += 1 ;
        *(uint16_t*)cur_pxl_pack &= 0x000Fu ;
        *(uint16_t*)cur_pxl_pack += val.blue << 8 ;
    }
}


bool
PixelIterator36bitRGB::isValid() const
{
    return m_is_valid ;
}

