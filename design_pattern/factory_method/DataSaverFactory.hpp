#pragma once
#include "DataSaver.hpp"
#include <memory>

class DataSaverFactory
{
    public:
        DataSaverFactory() {}

        virtual ~DataSaverFactory() {} 

        virtual std::shared_ptr<DataSaver>
        createSaver() = 0 ;
} ;

