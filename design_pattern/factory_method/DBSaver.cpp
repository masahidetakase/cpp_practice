#include "DBSaver.hpp"
#include <iostream>
#include <iterator>

using namespace std ;

DBSaver::DBSaver( const std::string& db_url)
    : m_db_url( db_url)
{
    // データベースに接続したつもり
    open() ;
}


DBSaver::~DBSaver()
{
    // データベースとの接続を切ったつもり
    close() ;
}


bool
DBSaver::save( const std::vector<int>& int_array)
{
    //DBにクエリを発行しているつもりになればいいのよ。
    cout << "Send data:" << endl;
    int idx = 0 ;
    for( const int& int_val : int_array) {
        cout << "[" << idx << "]: " << int_val << endl ;
    }

    return true ;
}


DBSaver::DBConnect
DBSaver::open()
{
    //DBに接続したつもり
    cout << "Conncet: " << m_db_url << endl ;
    return 1 ;
}


void
DBSaver::close()
{
    //DBとの接続を切ったつもり
    cout << "Cloase: " << m_db_url << endl ;
}