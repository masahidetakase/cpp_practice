#pragma once

#include "DataSaver.hpp"
#include <string>


class DBSaver : public DataSaver
{
    public:
        DBSaver( const std::string& db_url) ;

        virtual ~DBSaver() ;

        bool
        save( const std::vector<int>& int_array) ;

    private:
        typedef int DBConnect ;

        std::string m_db_url ;

        DBConnect open() ;

        void close() ;
} ;

