#pragma once
/*もうファイル分けるの面倒になってきた*/

#include "DataSaverFactory.hpp"
#include "LocalStorageSaver.hpp"
#include "DBSaver.hpp"

class LocalStorageSaverFactory : public DataSaverFactory
{
    public:
        LocalStorageSaverFactory( const std::string& file_path)
            : m_file_path( file_path) {}

        virtual ~LocalStorageSaverFactory() {}

        std::shared_ptr<DataSaver>
        createSaver()
        {
            auto ret = new LocalStorageSaver( m_file_path) ;
            return std::shared_ptr<DataSaver>( ret) ;
        }

    private:
        std::string m_file_path ;
} ;


class DBSaverFactory : public DataSaverFactory
{
    public:
        DBSaverFactory( const std::string& db_url)
            : m_db_url( db_url) {}

        virtual ~DBSaverFactory() {}

        std::shared_ptr<DataSaver>
        createSaver()
        {
            auto ret = new DBSaver( m_db_url) ;
            return std::shared_ptr<DataSaver>( ret) ;
        }

    private:
        std::string m_db_url ;
} ;
