#include "LocalStorageSaver.hpp"
#include <fstream>
#include <algorithm>
#include <iterator>
#include <stdexcept>

using namespace std ;

LocalStorageSaver::LocalStorageSaver( const std::string& data_file_path)
    : m_data_file_path( data_file_path),
      m_csv_fstream( m_data_file_path, fstream::out)
{
    if( !m_csv_fstream.is_open()) {
        throw domain_error( "I/O Error") ;
    }
}


bool
LocalStorageSaver::save( const std::vector<int>& int_array)
{
    ofstream csv_stream( m_data_file_path, fstream::out) ;

    if( !csv_stream.is_open()) {
        return false ;
    }

    copy( int_array.begin(),
          int_array.end(),
          ostream_iterator<int>( csv_stream, ",")) ;

    return true ;
}
