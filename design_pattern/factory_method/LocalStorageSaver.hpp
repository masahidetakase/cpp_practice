#pragma once

#include "DataSaver.hpp"
#include <string>
#include <fstream>

// int ha
class LocalStorageSaver : public DataSaver
{
    public:
        LocalStorageSaver( const std::string& data_file_path) ;

        virtual ~LocalStorageSaver() {}

        bool
        save( const std::vector<int>& int_array) ;

    private:
        std::string m_data_file_path ;

        std::ofstream m_csv_fstream ;
} ;
