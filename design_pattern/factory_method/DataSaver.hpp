#pragma once

#include <vector>

class DataSaver
{
    public:
        DataSaver() {}

        virtual ~DataSaver() {}

        virtual bool
        save( const std::vector<int>& int_array) = 0 ;
} ;
