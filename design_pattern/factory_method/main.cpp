#include "ConcreteDataSaverFactory.hpp"
#include <memory>
#include <vector>

using namespace std;

class DataLogger
{
    public:
        DataLogger() {}

        virtual ~DataLogger() {}

        void
        storeData( const int& value)
        {
            m_data.push_back( value) ;
        }

        void
        save()
        {
            // 必要なときにだけデータ保存オブジェクトを生成すればよい。
            shared_ptr<DataSaver> saver = m_saver_factory->createSaver() ;
            saver->save( m_data) ;
        }

        // データの保存方法の切り替え
        void
        setSavingMethod( DataSaverFactory* const saver_factory)
        {
            m_saver_factory.reset( saver_factory) ;
        }

        private:
            std::vector<int> m_data ;

            // データ保存オブジェクトのファクトリ
            shared_ptr<DataSaverFactory> m_saver_factory ;
} ;


int main( int argc, char* argv[])
{
    DataLogger data_logger ;

    data_logger.storeData( 1) ;
    data_logger.storeData( 4) ;
    data_logger.storeData( -9) ;

    // ローカルに保存するとき
    data_logger.setSavingMethod( new LocalStorageSaverFactory( "data.csv")) ;
    data_logger.save() ;

    // データベースに保存したいとき
    data_logger.setSavingMethod( new DBSaverFactory( "pgsql://hogefuga.com:3456/")) ;
    data_logger.save() ;

    return 0 ;
}
