#include "BuggySoilSensor.hpp"
#include <limits>

BuggySoilSensor::BuggySoilSensor()
    : m_is_working( false),
      m_moist_distrib( -40., 5.),
      m_temp_distrib( 15., 2.),
      m_ph_distrib( 11.5129, 4.) {}


BuggySoilSensor::~BuggySoilSensor() {}


void
BuggySoilSensor::sendStartCmd()
{
    m_is_working = !m_is_working ;
}


double
BuggySoilSensor::getMoisture()
{
    if( !m_is_working) {
        return -std::numeric_limits<double>::max() ;
    }

    return m_moist_distrib( m_rand_gen) ;
}


double
BuggySoilSensor::getTemperature()
{
    if( !m_is_working) {
        return -std::numeric_limits<double>::max() ;
    }

    return m_temp_distrib( m_rand_gen) ;
}


double
BuggySoilSensor::getPH()
{
    if( !m_is_working) {
        return -std::numeric_limits<double>::max() ;
    }

    return m_ph_distrib( m_rand_gen) ;
}
