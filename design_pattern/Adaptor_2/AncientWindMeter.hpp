#pragma once
#include <cstdint>
#include <random>

/* 旧式の風力計
   　　風向計の出力は12時を北とする時計盤表記
   　　風速計は5回計測したら、停止して再起動する必要あり
*/
class AncientWindMeter
{
    public:
        AncientWindMeter() ;

        virtual ~AncientWindMeter() ;

        void start() ;

        void stop() ;

        double getVelocity() ; // unit: m/s

        int getDirection() ; // clock dial system. 12:north, 3:east, 6:south, 9:west.

    private:
        static const std::int16_t MAX_READ_COUNT ;
        bool m_is_working ;
        int m_read_count ;

        std::random_device m_rand_gen ;
        std::uniform_int_distribution<int> m_direct_distrib ;
        std::uniform_real_distribution<float> m_velocity_distrib ;
} ;

