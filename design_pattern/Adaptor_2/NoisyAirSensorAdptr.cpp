#include "NoisyAirSensorAdptr.hpp"

using namespace std ;

const int NoisyAirSensorAdptr::Num_Sampling_Times = 4;

void
NoisyAirSensorAdptr::start()
{
    NoisyAirSensor::start() ;
}

void
NoisyAirSensorAdptr::stop()
{
    NoisyAirSensor::stop() ;
}


std::vector<MeasureItemID>
NoisyAirSensorAdptr::getItemList()
{
    return std::vector<MeasureItemID> {
        MeasureItemID::Air_temperature_deg,
        MeasureItemID::Air_pressure_pa,
        MeasureItemID::Air_rel_humidity_pct} ;
}


std::map<MeasureItemID,double>
NoisyAirSensorAdptr::getResult()
{
    using namespace std::placeholders ;

    double values[3] = { 0., 0., 0.} ;
    double average_val[3] = { 0., 0., 0.} ;

    for( int i = 0 ; i < Num_Sampling_Times ; ++i) {
        measure( values) ;
        transform( values,
                   values + 3,
                   average_val,
                   average_val,
                   std::plus<double>()) ;
    }

    transform( average_val,
               average_val + 3,
               average_val,
               std::bind( std::divides<double>(), _1, Num_Sampling_Times)) ;

    map<MeasureItemID,double> ret ;
    ret[ MeasureItemID::Air_temperature_deg] = average_val[0] ;
    ret[ MeasureItemID::Air_rel_humidity_pct] = average_val[1] ;
    ret[ MeasureItemID::Air_pressure_pa] = average_val[2] ;

    return ret ;
}
