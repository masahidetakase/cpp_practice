#include "IntegratedSensor.hpp"
#include <algorithm>
#include <functional>

using namespace std ;

IntegratedSensor::IntegratedSensor()
    : m_is_working( false) {}


IntegratedSensor::~IntegratedSensor() {}


void
IntegratedSensor::activate()
{
    if( !m_is_working) {
        m_is_working = true ;

        auto invoke_start =
        []( const std::shared_ptr<MeasureDeviceIF> dev)
        {
            dev->start() ;
        } ;

        for_each( m_device_list.begin(),
                  m_device_list.end(),
                  invoke_start) ;
    }
}


void
IntegratedSensor::inactivate()
{
    if( m_is_working) {
        m_is_working = false ;

        auto invoke_stop =
        []( const std::shared_ptr<MeasureDeviceIF> dev)
        {
            dev->stop() ;
        } ;

        for_each( m_device_list.begin(),
                  m_device_list.end(),
                  invoke_stop) ;
    }
}


void
IntegratedSensor::addDevice( MeasureDeviceIF* const device)
{
    if( device == NULL) { return ;}

    m_device_list.insert( shared_ptr<MeasureDeviceIF>(device)) ;
}


std::map<MeasureItemID, double>
IntegratedSensor::getResult()
{
    std::map<MeasureItemID, double> ret ;

    if( m_is_working) {
        for( auto device : m_device_list) {
            auto results = device->getResult() ;
            ret.insert( results.begin(), results.end()) ;
        }
    } else {
        ret[MeasureItemID::None] =
            -std::numeric_limits<double_t>::max() ;
    }

    return ret ;
}


