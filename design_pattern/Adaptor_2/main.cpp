#include "AncientWindMeterAdptr.hpp"
#include "BuggySoilSensorAdptr.hpp"
#include "IntegratedSensor.hpp"
#include "NoisyAirSensorAdptr.hpp"
#include <iostream>

using namespace std ;

int
main( int argc, char *argv[])
{
    IntegratedSensor integrated_sensor ;

    integrated_sensor.addDevice( new AncientWindMeterAdptr()) ;
    integrated_sensor.addDevice( new BuggySoilSensorAdptr()) ;
    integrated_sensor.addDevice( new NoisyAirSensorAdptr()) ;

    integrated_sensor.activate() ;

    auto measure_results = integrated_sensor.getResult() ;
    for( auto result : measure_results)
    {
       cout << "Item: " << MeasureDeviceIF::convDevID2Str( result.first)
            << "  Value: " << result.second
            << endl ;
    }

    return 0 ;
}