#include "NoisyAirSensor.hpp"
#include <cassert>
#include <limits>

using namespace std ;

NoisyAirSensor::NoisyAirSensor()
    : m_temp_distrib( 17., 3.),
      m_rel_humidity_distrib( 40., 6.),
      m_pressure_distrib( 101325., 20000.) {}


void
NoisyAirSensor::measure( double* const results)
{
    assert( results != nullptr) ;

    if( !m_is_working) {
        results[0] = -std::numeric_limits<double_t>::max() ;
        results[1] = -std::numeric_limits<double_t>::max() ;
        results[2] = -std::numeric_limits<double_t>::max() ;
    }

    results[0] =  m_temp_distrib( m_rand_gen) ;
    results[1] =  m_rel_humidity_distrib( m_rand_gen) ;
    results[2] =  m_pressure_distrib( m_rand_gen) ;
}
