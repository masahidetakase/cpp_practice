#pragma once

#include <cstdint>
#include <random>

/* 時間分解能が高く、気温、湿度、気圧を同時に測定可能。
　　　ただ、信号にノイズが多く、測定値のばらつきが大きいため、連続で4回測定し、その平均値を取る必要あり。
 */
class NoisyAirSensor
{
    public:
        NoisyAirSensor() ;

        ~NoisyAirSensor() {}

        void  start() { m_is_working = true ;}

        void stop() { m_is_working = false ;}

        // 読み出した値が、引数のポインタの先頭から、気温（deg.）、相対湿度(%)、気圧(hPa)
        void measure( double* const results) ;

    private:
        bool m_is_working ;

        std::random_device m_rand_gen ;
        std::normal_distribution<double> m_temp_distrib ;
        std::normal_distribution<double> m_rel_humidity_distrib ;
        std::normal_distribution<double> m_pressure_distrib ;
} ;
