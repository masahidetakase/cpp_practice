#include "AncientWindMeter.hpp"
#include <limits>

using namespace std ;

const std::int16_t AncientWindMeter::MAX_READ_COUNT = 5 ;

AncientWindMeter::AncientWindMeter()
    : m_is_working( false),
      m_read_count( 0),
      m_rand_gen(),
      m_direct_distrib( 1, 12),
      m_velocity_distrib( 2, 8) {}


AncientWindMeter::~AncientWindMeter() {}


void
AncientWindMeter::start()
{
    m_is_working = true ;
    m_read_count = 0 ;
}


void
AncientWindMeter::stop()
{
    m_is_working = false ;
}


double
AncientWindMeter::getVelocity()
{
    if( ( !m_is_working) ||
        ( MAX_READ_COUNT < m_read_count)) {
        return -std::numeric_limits<double>::max() ;
    }

    ++m_read_count ;
    return m_velocity_distrib( m_rand_gen) ;
}


int
AncientWindMeter::getDirection()
{
    if( ( !m_is_working) ||
        ( MAX_READ_COUNT < m_read_count)) {
        return std::numeric_limits<int>::min() ;
    }

    ++m_read_count ;
    return m_direct_distrib( m_rand_gen) ;
}
