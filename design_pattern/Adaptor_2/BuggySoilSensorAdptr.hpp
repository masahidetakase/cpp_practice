#pragma once

#include "BuggySoilSensor.hpp"
#include "MeasureDeviceIF.hpp"
#include <cmath>

class BuggySoilSensorAdptr
    : public MeasureDeviceIF,
      private BuggySoilSensor
{
    public:
        BuggySoilSensorAdptr() {}

        ~BuggySoilSensorAdptr()
        {
            stop() ;
        }

        void
        start()
        {
            if( !isActive())
            {
                sendStartCmd() ;
            }
        }

        void
        stop()
        {
            if( isActive())
            {
                sendStartCmd() ;
            }
        }

        std::vector<MeasureItemID>
        getItemList()
        {
            std::vector<MeasureItemID> ret ;

            ret.push_back( MeasureItemID::Soil_temperature_deg) ;
            ret.push_back( MeasureItemID::Soil_moisture_kpa) ;
            ret.push_back( MeasureItemID::Soil_ph) ;

            return ret ;
        }


        std::map<MeasureItemID,double>
        getResult()
        {
            std::map<MeasureItemID,double> ret ;

            ret[MeasureItemID::Soil_temperature_deg] =
                getTemperature() ;

            ret[MeasureItemID::Soil_moisture_kpa] =
                getMoisture() ;

            ret[MeasureItemID::Soil_ph] =
                getPH() * log10( exp(1)) ;

            return ret ;
        }
} ;
