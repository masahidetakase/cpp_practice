#pragma once

#include "AncientWindMeter.hpp"
#include "MeasureDeviceIF.hpp"

class AncientWindMeterAdptr
    : public MeasureDeviceIF,
      private AncientWindMeter
{
    public:
        AncientWindMeterAdptr() {}

        ~AncientWindMeterAdptr()
        {
            stop() ;
        }

        void
        start() { AncientWindMeter::start() ;}

        void
        stop() { AncientWindMeter::stop() ;}

        std::vector<MeasureItemID>
        getItemList()
        {
            std::vector<MeasureItemID> ret ;

            ret.push_back( MeasureItemID::Wind_velocity_m_per_s) ;
            ret.push_back( MeasureItemID::Wind_direction_deg) ;

            return ret ;
        }

        std::map<MeasureItemID,double>
        getResult()
        {
            std::map<MeasureItemID,double> ret ;

            ret[MeasureItemID::Wind_velocity_m_per_s] =
                getVelocity() ;

            ret[MeasureItemID::Wind_direction_deg] =
                getDirection() /12. * 360. ;

            return ret ;
        }
} ;

