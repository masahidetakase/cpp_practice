#include "MeasureDeviceIF.hpp"

using namespace std ;

const std::string&
MeasureDeviceIF::convDevID2Str( const MeasureItemID& dev_id)
{
    static struct __InitTable
    {
        __InitTable()
        {
            devID2str[MeasureItemID::None] = "no device" ;
            devID2str[MeasureItemID::Air_temperature_deg] = "air temp.(deg.)" ;
            devID2str[MeasureItemID::Air_pressure_pa] = "air pressure(Pa)" ;
            devID2str[MeasureItemID::Air_rel_humidity_pct] = "relative air humidity(%)" ;
            devID2str[MeasureItemID::Air_COconc_ppm] = "CO conc.(ppm)" ;
            devID2str[MeasureItemID::Air_CO2conc_ppm] = "CO2 conc.(ppm)" ;
            devID2str[MeasureItemID::Air_NOXconc_ppm] = "NOX conc.(ppm)" ;
            devID2str[MeasureItemID::Air_SOXconc_ppm] = "SOX conc.(ppm)" ;
            devID2str[MeasureItemID::Wind_direction_deg] = "wind direction(deg.)" ;
            devID2str[MeasureItemID::Wind_velocity_m_per_s] = "wind velocity(m/sec)" ;

            devID2str[MeasureItemID::Radiation_kw_per_m2] = "solar radiation(kW/m2)" ;
            devID2str[MeasureItemID::Solar_altitude_deg] = "solar altitude(deg.)" ;

            devID2str[MeasureItemID::Soil_temperature_deg] = "soil temp.(deg.)" ;
            devID2str[MeasureItemID::Soil_moisture_kpa] = "soil moisture(kPa)" ;
            devID2str[MeasureItemID::Soil_ph] = "soil PH" ;

            devID2str[MeasureItemID::End_Item] = "Unregistered Device" ;
        }

        map<MeasureItemID,string> devID2str ;
    } InitTable ;

    if( MeasureItemID::End_Item <= dev_id) {
        return InitTable.devID2str[MeasureItemID::End_Item] ;
    }

    return InitTable.devID2str[dev_id] ;
}

