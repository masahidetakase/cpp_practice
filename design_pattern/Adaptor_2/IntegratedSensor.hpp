#pragma once
#include "MeasureDeviceIF.hpp"
#include <set>
#include <map>
#include <memory>

class IntegratedSensor
{
    public:
        IntegratedSensor() ;

        ~IntegratedSensor() ;

        void
        activate() ;

        void
        inactivate() ;

        void
        addDevice( MeasureDeviceIF* const device) ;

        std::map<MeasureItemID, double>
        getResult() ;

    private:
        std::set<std::shared_ptr<MeasureDeviceIF>> m_device_list ;

        bool m_is_working ;
} ;
