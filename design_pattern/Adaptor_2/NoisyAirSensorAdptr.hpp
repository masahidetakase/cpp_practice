#pragma once
#include "NoisyAirSensor.hpp"
#include "MeasureDeviceIF.hpp"
#include <algorithm>
#include <functional>

class NoisyAirSensorAdptr
    : public MeasureDeviceIF,
      private NoisyAirSensor
{
    public:
        NoisyAirSensorAdptr() {}

        ~NoisyAirSensorAdptr() {}

        void start() ;

        void stop() ;

        std::vector<MeasureItemID>
        getItemList() ;

        std::map<MeasureItemID,double>
        getResult() ;

    private:
        static const int Num_Sampling_Times ;
} ;
