#pragma once
#include <cstdint>
#include <string>
#include <map>

enum struct  MeasureItemID : std::int16_t
{
    None = -1, // unused

    // Air
    Air_temperature_deg = 0,
    Air_pressure_pa,
    Air_rel_humidity_pct,
    Air_COconc,
    Air_CO2conc_ppm,
    Air_NOXconc_ppm,
    Air_SOXconc_ppm,
    Wind_direction_deg, // North:0, East:90, South:180, West: 270
    Wind_velocity_m_per_s,

    // light
    Radiation_kw_per_m2 = 50,
    Solar_altitude_deg,

    // Soil
    Soil_temperature_deg = 100,
    Soil_moisture_kpa,
    Soil_ph
} ;

extern const std::map<MeasureItemID,std::string>
MeasureItemID2StrTable ;