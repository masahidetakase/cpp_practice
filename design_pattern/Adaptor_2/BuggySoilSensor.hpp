#pragma once
#include <cstdint>
#include <random>


/* 土壌水分活性（kPa）、土中温度（度）、pHを測定する計測器。
 * 残念なことに、pH算出のコードで、log10()ではなくlog()を用いてしまったため、pHの値が異常になっている。
 * さらに、装置を停止させるときは、動作中に計測開始コマンドを実行する。
 */
class BuggySoilSensor
{
    public:
        BuggySoilSensor() ;

        virtual ~BuggySoilSensor() ;

        void sendStartCmd() ;

        bool isActive() const { return m_is_working ;}

        double getMoisture() ;

        double getTemperature() ;

        double getPH() ;

    private:
        bool m_is_working ;

        std::random_device m_rand_gen ;
        std::normal_distribution<double> m_moist_distrib ;
        std::normal_distribution<double> m_temp_distrib ;
        std::normal_distribution<double> m_ph_distrib ;
} ;
