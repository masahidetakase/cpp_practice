#pragma once
#include <cstdint>
#include <vector>
#include <map>
#include <string>

enum struct  MeasureItemID : std::int16_t
{
    None = -1, // unused

    // Air
    Air_temperature_deg = 0,
    Air_pressure_pa,
    Air_rel_humidity_pct,
    Air_COconc_ppm,
    Air_CO2conc_ppm,
    Air_NOXconc_ppm,
    Air_SOXconc_ppm,
    Wind_direction_deg, // North:0, East:90, South:180, West: 270
    Wind_velocity_m_per_s,

    // light
    Radiation_kw_per_m2 = 50,
    Solar_altitude_deg,

    // Soil
    Soil_temperature_deg = 100,
    Soil_moisture_kpa,
    Soil_ph,

    End_Item // end
} ;



// Abstract class for measurement devices.
class MeasureDeviceIF
{
    public:
        // Start device
        virtual void
        start() = 0 ;

        // Stop device
        virtual void
        stop() = 0 ;

        // Get measurement item list.
        virtual std::vector<MeasureItemID>
        getItemList() = 0 ;

        // Get measurement results.
        virtual std::map<MeasureItemID,double>
        getResult()  = 0 ;

        static const std::string&
        convDevID2Str( const MeasureItemID& dev_id) ;
} ;
