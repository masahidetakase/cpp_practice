#pragma once
#include <list>
#include <cstddef>

namespace mem_alloc
{

/*
  <Data structure>
  |<------------------------ Boundary block ------------------->|
  |<-------- Boundary tag ------->|              |size of header |
  |           (header)            |              | & data        |
  |isUsed|data_size|*data|*end_tag|<--- Data --->|end_tag(size_t)|
                    |     |        ^              ^
                    |     |        |              |
                    |     + -------|--------------+
                    +--------------+
                             ( the prefix of "*" means an pointer.)
 */
struct BoundaryTagBlock
{
    bool isUsed ; // Free or used
    size_t data_size ; // the size of data except the header ans end mark.
    void* data ; //  the pointer to the data.
    size_t* end_tag ; // the pointer to the data that store the size of the header and data.

    BoundaryTagBlock* prev_block ; // the pointer to the previous block (order of registration)
    BoundaryTagBlock* next_block ; // // the pointer to the next block (order of registration)

#ifdef DEBUG
    static size_t DEBUG_Num_Efficient_Block = 0 ;
#endif

    BoundaryTagBlock() ;

    BoundaryTagBlock( const size_t& block_size) ;

    /**
     * @brief insert a block.
     * @details insert a block(inserter) before pos.
     * @param pos the position where a new block is inserted.
     * @param insertion inserted block.
     * @return the pointer to the insertion
     */
    BoundaryTagBlock*
    insert( BoundaryTagBlock* const insertion) ;

    /**
     * @brief Remove a node from the list.
     */
    void
    leave() ;
} ;

} /* the end of mem_alloc namespace */