#pragma once
#include <mutex>

/**
 * @brief the single thread model unique to class T.
 * @details SingleThreadModel can not be instantiated.
 * @tparam T types
 */
class SingleThreadModel
{
    public:
        SingleThreadModel() {}

        class Lock
        {
            public:
                Lock( SingleThreadModel* const host) {}

                virtual ~Lock() {}

            private:
                // prohibition against cloning or swapping.
                Lock( const Lock&) ;
                Lock( const Lock&&) ;
                const Lock& operator = ( const Lock&);
                const Lock& operator = ( const Lock&&);
        } ;

    protected:
        ~SingleThreadModel() ;
} ;


/**
 * @brief the multi thread model unique to class T.
 * @details MultiThreadModel cannot be instantiated.
 * @tparam T [description]
 */
class MultiThreadModel
{
    public:
        MultiThreadModel() {}

        class Lock
        {
            public:
                Lock( MultiThreadModel* const host)
                {
                    m_host->m_mutex.lock() ;
                }

                virtual ~Lock()
                {
                    m_host->m_mutex.unlock() ;
                }

            private:
                // prohibition against cloning or swapping.
                Lock( const Lock&) ;
                Lock( const Lock&&) ;
                const Lock& operator = ( const Lock&);
                const Lock& operator = ( const Lock&&);

                MultiThreadModel* m_host ;
        } ;

    protected:
        // protected destructor prohibit
        ~MultiThreadModel() {}

    private:
        std::mutex m_mutex;
} ;
