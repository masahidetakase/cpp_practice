/**
 * @file TLSFMemoryAllocator.hpp
 * @brief The memory allcator for TLSF
 */

#pragma once
#include <cassert>
#include <cstddef>
#include <cstring>
#include <new>
#include "ThreadModel.hpp"
#include "BoundaryTagBlock.hpp"

namespace mem_alloc
{

/**
 * @brief TLSF memory allocator.
 * @detail the memory allcator that is fast but not so efficient in space.
 * http://www.gii.upv.es/tlsf/
 * @tparam ThreadModel the type provides a lock guard.
 * @tparam Block_Size_Min_byte it must be 2^N.
 */
template<class ThreadModel, size_t Block_Size_Min_byte = 32>
class TLSFMemoryAllocator : public ThreadModel
{
    public:
        /**
         * @brief the status of the allocator.
         */
        struct status
        {
            size_t pool_size_byte ; /// the pool size
            size_t used_area_size_byte ;
            size_t num_blocks ;
        } ;

        /**
         * @brief Initialize the allcator.
         * @param[in] pool_size the size of required memory pool in heap. When the param omitted, 16KB of heap is allocated.
         * @param[in] signature the signature of the allocator.
         * @exception std::bad_alloc Memory allocation failed.
         */
        static void
        init( const size_t& pool_size_byte = Default_Pool_Size_byte)
        {
            // memory pool must be larger than Default_Pool_Size_byte.
            const size_t pool_size =
                ( Default_Pool_Size_byte < pool_size_byte) ?
                pool_size_byte :
                Default_Pool_Size_byte ;

            getInstance( pool_size) ;
        }


        static TLSFMemoryAllocator&
        getInstance( const size_t& pool_size_byte)
        {
            // In c++11, a local static variables is initialized in thread-safety.
            static TLSFMemoryAllocator instance( pool_size_byte) ;

            return instance ;
        }

        /**
         * @brief Get the status of the allocator.
         * @details the pool size, used area size and number of the memory blocks.
         * @return the status of the allcator.
         */
        static status
        getPoolStatus() ;

        ~TLSFMemoryAllocator()
        {
            releaseManagementArea() ;
        }


    private:
        // Constants
        static const size_t Default_Pool_Size_byte = 16 * 1024 ; /// Default memory pool size.
        static const size_t Word_Len_bit = sizeof(size_t) * 8 ; /// WORD length of the CPU.
        static const size_t Block_Tag_Size_byte = sizeof(BoundaryTagBlock) + sizeof(size_t) ;
        static const size_t End_Tag_Size_byte = sizeof(size_t) ;
        const size_t Sli_Bits_Mask ;
        const size_t Num_Sli_Bits ;
        size_t Fli_Max ; /// Max FLI.
        size_t Fli_Max_Sli_Max ; /// Max SLI in the largest FLI.
        size_t Free_Block_List_Idx_Max ;
        size_t Init_Block_Size_byte ; /// initial memory block size.

        void* m_mem_pool ;

        size_t m_pool_size_byte ;

        BoundaryTagBlock* m_free_block_list_array ;
        BoundaryTagBlock m_active_block_list ;

        size_t* m_free_block_list_bit ;


        /**
         * @brief Private constructor that is called only by init().
         * @param pool_size_byte memory pool size.
         */
        TLSFMemoryAllocator( const size_t& pool_size_byte)
            : Sli_Bits_Mask( Block_Size_Min_byte - 1),
              Num_Sli_Bits( getMSB( Sli_Bits_Mask)),
              Fli_Max( 0),
              Fli_Max_Sli_Max( 0.),
              Free_Block_List_Idx_Max( 0),
              Init_Block_Size_byte( 0),
              m_mem_pool(nullptr),
              m_pool_size_byte( pool_size_byte),
              m_free_block_list_array( nullptr),
              m_free_block_list_bit( nullptr)
        {
            //preconditions
            assert( Block_Size_Min_byte < Word_Len_bit) ;
            assert( Block_Tag_Size_byte + Block_Size_Min_byte < pool_size_byte) ;

            const size_t expected_num_blocks =
                pool_size_byte / ( Block_Size_Min_byte + Block_Tag_Size_byte) ;

            calcGoodFitIndex( pool_size_byte - expected_num_blocks * Block_Tag_Size_byte,
                              &Fli_Max,
                              &Fli_Max_Sli_Max) ;

            Free_Block_List_Idx_Max = Fli_Max * Block_Size_Min_byte + Fli_Max_Sli_Max ;

            Init_Block_Size_byte =
                ( 1 << Fli_Max) + ( Fli_Max_Sli_Max << ( Fli_Max - Num_Sli_Bits)) ;

            m_pool_size_byte =
                Init_Block_Size_byte + expected_num_blocks * Block_Tag_Size_byte ;

            try
            {
                allocManagementArea() ;
            }
            catch(...)
            {
                releaseManagementArea() ;

                std::bad_alloc() ;
            }
        }


        /**
         * @brief Allocate a memory block.
         * @details A new memory block is allocated.
         * @param required_size memory block size.
         * @retval nullptr Allocation failed.
         * @retval otherwise the pointer to the new memory block.
         */
        void*
        allocate( const size_t& required_size_byte)
        {
            assert( required_size_byte < Init_Block_Size_byte) ;

            // Find a free block.
            size_t fli, sli ;
            calcGoodFitIndex( required_size_byte, &fli, &sli) ;

            BoundaryTagBlock* free_block = nullptr ;

            if( ( m_free_block_list_bit[ fli] & ( 1 << sli)) != 0)
            {
                free_block = m_free_block_list_array[ Free_Block_List_Idx_Max].next_block ;
                free_block->isUsed = true ;
                free_block->leave() ;
                m_active_block_list.insert( free_block) ;

                return free_block->data ;
            }

            fli += (++sli) >> Num_Sli_Bits ;
            sli &= Sli_Bits_Mask ;

            for( ; fli <= Fli_Max ; ++fli) {
                    if( m_free_block_list_bit[ fli] != 0) {

                    }
                }
            }

            for( ; m_free_block_list_bit[ fli] != 0)

            for( ; ( m_free_block_list_bit[ fli] & ( 1 << sli)) == 0 ; ++sli) {
                fli += sli >> Num_Sli_Bits ;
                sli &= Sli_Bits_Mask ;

                // When a free block not found,
                if( ( fli == Fli_Max) && ( Fli_Max_Sli_Max == sli))
                {

                }
            }

            size_t actual_required_size_byte = calcBlockSizeFromIndex( fli, sli) ;

            BoundaryTagBlock* free_block =
                m_free_block_list_array[ fli * Block_Size_Min_byte + sli] ;

            // divide a block.
            size_t remained_block_size = Block_Tag_Size_byte
            if( ( Block_Tag_Size_byte + Block_Size_Min_byte <= free_block->data_size - actual_required_size_byte) {
                free_block->isUsed = true ;
                return free_block
            }


        }


        void
        allocManagementArea()
        {
            // allocate the memory pool in heap.
            m_mem_pool = new char[ m_pool_size_byte] ;

            // initialize the array of the free block lists.
            m_free_block_list_array =
                new BoundaryTagBlock[ Free_Block_List_Idx_Max + 1] ;

            // initialize active block list.
            m_active_block_list = new BoundaryTagBlock ;

            // initialize free block bit.
            m_free_block_list_bit = new size_t[Fli_Max +1] ;
            memset( m_free_block_list_bit, 0, sizeof(size_t) * (Fli_Max +1)) ;

            // initialize the first free block, which includes the whole pool.
            m_free_block_list_bit[ Fli_Max] = 1 << Fli_Max_Sli_Max ;
            BoundaryTagBlock* init_free_block = new( m_mem_pool) BoundaryTagBlock( Init_Block_Size_byte) ;
            m_free_block_list_array[ Free_Block_List_Idx_Max].insert( init_free_block) ;
        }


        void
        releaseManagementArea()
        {
            delete m_active_block_list ;
            delete[] m_mem_pool ;
            delete[] m_free_block_list_array ;
            delete[] m_free_block_list_bit ;
        }


        /**
         * @brief get zero-based MSB (ex.13(10)=1101(2) -> MSB=3).
         * @param n unsigned integer.
         * @return zero-based MSB.
         */
        inline size_t
        getMSB( const size_t& n)
        {
            if( n == 0) { return 0 ;}

            return sizeof(size_t) * 8 - __builtin_clzl( Block_Size_Min_byte) - 1 ;
        }


        inline void
        calcGoodFitIndex( const size_t& required_size_byte,
                         size_t* const fli,
                         size_t* const sli)
        {
            //preconditions
            assert( fli != NULL) ;
            assert( sli != NULL) ;

            // block size must be larger than the Block_Size_Min_byte.
            size_t actual_required_size =
                ( Block_Size_Min_byte < required_size_byte) ?
                required_size_byte :
                Block_Size_Min_byte ;

            *fli = getMSB( actual_required_size) ; // first index level
            *sli = ( actual_required_size >> ( *fli - Num_Sli_Bits)) & Sli_Bits_Mask ;
        }


        inline size_t
        calcBlockSizeFromIndex( const size_t& fli, const size_t& sli)
        {
            return ( 1 << fli) + ( ( sli + 1) << ( fli - Num_Sli_Bits)) ;
        }



        inline BoundaryTagBlock*
        extractNewBlock( BoundaryTagBlock* const src_block,
                         const size_t& new_block_size_byte)
        {
            // preconditions.
            assert( src_block != nullptr) ;
            assert( Block_Size_Min_byte <= new_block_size_byte) ;
            assert( new_block_size_byte <= src_block->data_size) ;

            if( src_block->data_size == new_block_size_byte) {
                return src_block ;
            } else {
                src_block->leave() ;
                size_t new_src_block_size =
                    src_block->data_size - new_block_size_byte - Block_Tag_Size_byte ;


                src_block->data_size = new_block_size_byte ;
                src_block->end_tag = src_block->
                BoundaryTagBlock* new_block
                    = new( src_block->end_tag + sizeof(size_t)) BoundaryTagBlock( new_block_size_byte) ;


                src_block->data_size = new_block_size_byte ;
                *(src_block->end_tag) =
            }
        }


        // prohibition against cloning.
        TLSFMemoryAllocator( const TLSFMemoryAllocator&) ;

        TLSFMemoryAllocator( const TLSFMemoryAllocator&&) ;

        const TLSFMemoryAllocator&
        operator = ( const TLSFMemoryAllocator&) ;

        const TLSFMemoryAllocator&
        operator = ( const TLSFMemoryAllocator&&) ;
} ;

} // the end of my_util namespace