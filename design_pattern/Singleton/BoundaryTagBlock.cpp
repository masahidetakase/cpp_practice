#include "BoundaryTagBlock.hpp"
#include <cstdint>
#include <cassert>

namespace mem_alloc
{

using namespace std ;

typedef uint8_t byte ;

BoundaryTagBlock::BoundaryTagBlock()
    : isUsed( false),
      data_size( 0),
      data( nullptr),
      end_tag( nullptr),
      prev_block( nullptr),
      next_block( nullptr) {}


BoundaryTagBlock::BoundaryTagBlock( const size_t& block_size)
        : isUsed( false),
          data_size( block_size),
          data( (byte*)this + sizeof(BoundaryTagBlock)),
          end_tag( (size_t*)( (byte*)this + sizeof(BoundaryTagBlock) + block_size)),
          prev_block( nullptr),
          next_block( nullptr)
{
    // preconditions.
    assert( 0 < block_size) ;

    *end_tag = block_size ;

#ifdef DEBUG
    data[0] = 'T' ;
    data[1] = 'L' ;
    data[2] = 'F' ;
    data[3] = 'S' ;
    data[4] = 'D' ;
    data[5] = 'E' ;
    data[6] = 'B' ;
    data[7] = 'U' ;
    data[8] = 'G' ;
    data[9] = '=' ;
    data[10] = 0x0A ; // LF

    ++DEBUG_Num_Efficient_Block ;
#endif

}


BoundaryTagBlock*
BoundaryTagBlock::insert( BoundaryTagBlock* const insertion)
{
    //preconditions.
    // A new block must be always inserted just after the dummy head of the list.
    assert( ( prev_block == nullptr) && ( data_size == 0) &&
            ( data == nullptr) && ( end_tag == nullptr)) ;
    assert( insertion != nullptr) ;

    insertion->prev_block = this ;
    insertion->next_block = next_block ;

    if( next_block != nullptr) {
        next_block->prev_block = insertion ;
    }

    next_block = insertion ;

    return next_block ;
}


void
BoundaryTagBlock::leave()
{
    //preconditions.
    // A released block must not be the dummy head of the list.
    assert( ( prev_block != nullptr) && ( data_size != 0) &&
            ( data != nullptr) && ( end_tag != nullptr)) ;

    prev_block->next_block = next_block ;

    if( next_block != nullptr) {
        next_block->prev_block = prev_block ;
    }
}






} /* the end of mem_alloc namespace */