#pragma once

#include "PacketDecoder.hpp"
#include <vector>

/**
 * Packet format:
 *  |<SOH>|<-'CRYPTO'(6byte)->|<-string length(4byte)->|<STX>|<-XORed string without NULL termination->|<NUL>|<ETX>|
 */
class EncryptedPacketDecoder : public PacketDecoder
{
    public:
         EncryptedPacketDecoder( const std::int8_t& decode_key)
            : m_decode_key( decode_key) {}

        virtual ~EncryptedPacketDecoder() {}

    private:
        static const char Identifier[] ;
        static const size_t Packet_Length_Min = 14 ;
        static const size_t Offset_Payload_Len = 7 ;
        static const size_t Offset_Payload_Body = 12 ;

        const std::int8_t m_decode_key ;

        bool
        identify() ;

        bool
        verify() ;

        std::string
        extractStr() ;
} ;


std::vector<std::int8_t>
createEncryptedPacket( const std::string& plain_str,
                       const std::int8_t& xor_key) ;