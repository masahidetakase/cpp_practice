#include "EncryptedPacketDecoder.hpp"
#include <algorithm>
#include <functional>
#include <iterator>
#include <utility>
#include "AsciiCode.hpp"

using namespace std ;

const char EncryptedPacketDecoder::Identifier[] =
    { 0x01, 'C', 'R', 'Y', 'P', 'T', 'O'}  ; // 0x01 = SOH

bool
EncryptedPacketDecoder::identify()
{
    // length
    if( m_packet_len < Packet_Length_Min) {
        return false ;
    }

    auto iter_pair = mismatch( m_packet,
                               m_packet + sizeof(Identifier),
                               Identifier) ;

    // check header
    if( iter_pair.first != m_packet + sizeof(Identifier)) {
        return false ;
    }

    // check payload size.
    m_payload_len = *(uint32_t*)( m_packet + Offset_Payload_Len) ;
    if( m_payload_len + Packet_Length_Min != m_packet_len) {
        return false ;
    }

    // Get payload pointer.
    m_payload = m_packet + Offset_Payload_Body ;

    // find <STX>
    if( m_packet[ Offset_Payload_Body - 1] != AsciiCode::STX) {
        return false ;
    }

    // find <NUL>
    if( m_packet[ m_packet_len - 2] != AsciiCode::NUL) {
        return false ;
    }

    // find <ETX> at the end of the packet.
    if( m_packet[ m_packet_len - 1] != AsciiCode::ETX) {
        return false ;
    }

    return true ;
}


bool
EncryptedPacketDecoder::verify()
{
    return true ;
}


string
EncryptedPacketDecoder::extractStr()
{
    string ret ;

    auto decodeChar =
        [&]( const int8_t& ch)-> char
        {
            return ch ^ m_decode_key ;
        } ;


    transform( m_payload,
               m_payload + m_payload_len,
               back_inserter( ret),
               decodeChar) ;

    return move( ret) ;
}


vector<int8_t>
createEncryptedPacket( const string& plain_str,
                       const int8_t& xor_key)
{
    //preconditions
    assert( 0 < plain_str.length()) ;
    assert( !plain_str.empty()) ;

    vector<int8_t> ret =
        { AsciiCode::SOH,
          'C', 'R', 'Y', 'P', 'T', 'O',
          ' ', ' ', ' ', ' ',
          AsciiCode::STX} ;

    std::uint32_t str_len = plain_str.length() ;

    copy( (int8_t*)&str_len,
          (int8_t*)&str_len + sizeof(str_len),
          ret.begin() + 7) ;

    auto encodeChar =
        [&]( const char& ch)->char
        {
            return ch ^ xor_key ;
        } ;

    transform( plain_str.begin(),
               plain_str.end(),
               back_inserter( ret),
               encodeChar) ;

    ret.push_back( AsciiCode::NUL) ;
    ret.push_back( AsciiCode::ETX) ;

    return move( ret) ;
}
