#pragma once

#include <cstdint>
#include <cstdio>

namespace my_util
{

std::uint32_t
crc32Calc( const std::uint8_t* data, const size_t& len) ;

}

