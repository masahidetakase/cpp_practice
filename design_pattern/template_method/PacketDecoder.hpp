#pragma once

#include <cstdint>
#include <string>
#include <cassert>
#include <stdexcept>

class PacketDecoder
{
    public:
        PacketDecoder()
            : m_packet( nullptr),
              m_payload( nullptr),
              m_packet_len(0),
              m_payload_len(0) {}

        virtual ~PacketDecoder() {}

        std::string
        decode( const std::int8_t* packet,
                const size_t& packet_len)
        {
            //preconditions
            assert( packet != nullptr) ;
            assert( 0 < packet_len) ;

            if( Max_String_Len <= packet_len) {
                return std::string() ;
            }

            m_packet = packet ;
            m_packet_len = packet_len ;

            if( !identify()) {
                return std::string() ;
            }

            if( !verify()) {
                std::domain_error( "Decode error") ;
            }

            return extractStr() ;
        }

    private:
        virtual bool
        identify() = 0 ;

        virtual bool
        verify() = 0 ;

        virtual std::string
        extractStr() = 0 ;

    protected:
        static const size_t Max_String_Len = 1024 ;

        const std::int8_t* m_packet ;
        const std::int8_t* m_payload ;
        size_t m_packet_len ;
        size_t m_payload_len ;

} ;

