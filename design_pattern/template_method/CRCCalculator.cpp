#include "CRCCalculator.hpp"
#include <array>

namespace my_util
{

using namespace std ;

uint32_t
crc32Calc( const uint8_t* data, const size_t& len)
{
    struct _CRCTable
    {
        array<uint32_t,256> crc_table ;

        _CRCTable()
        {
            for( uint32_t i = 0 ; i < 256; ++i) {
                uint32_t c = i ;
                for ( int j = 0; j < 8; ++j) {
                    c = (c & 1) ? (0xEDB88320 ^ (c >> 1)) : (c >> 1) ;
                }
                crc_table[i] = c;
            }
        }

        inline uint32_t
        operator [] ( const size_t& idx) {
            return crc_table[idx] ;
        }
    } ;

    static _CRCTable crc_table ;

    uint32_t c = 0xFFFFFFFF;

    for( size_t i = 0 ; i < len ; ++i) {
        c = crc_table[(c ^ data[i]) & 0xFF] ^ (c >> 8) ;
    }

    return c ^ 0xFFFFFFFF ;
}

}
