
#include "PlainPacketDecoder.hpp"
#include "AsciiCode.hpp"
#include "CRCCalculator.hpp"
#include <algorithm>
#include <iterator>

using namespace std ;

const std::int8_t PlainPacketDecoder::Identifier[] = { '!', 'P', 'C', 'T', 0x1E} ; //"!PCT"<RS>


bool
PlainPacketDecoder::identify()
{
    // length
    if( m_packet_len < Packet_Length_Min) {
        return false ;
    }

    auto iter_pair = mismatch( m_packet,
                               m_packet + sizeof(Identifier),
                               Identifier) ;

    // check header
    if( iter_pair.first != m_packet + sizeof(Identifier)) {
        return false ;
    }

    // check payload size.
    m_payload_len = *(uint32_t*)( m_packet + Offset_Payload_Len) ;
    if( m_payload_len + Packet_Length_Min != m_packet_len) {
        return false ;
    }

    // Get payload pointer.
    m_payload = m_packet + Offset_Payload_Body ;

    // find <LF>
    if( m_payload[ m_payload_len] != AsciiCode::LF) {
        return false ;
    }

    // find <DEL> at the end of the packet.
    if( m_packet[ m_packet_len - 1] != AsciiCode::DEL) {
        return false ;
    }

    return true ;
}


bool
PlainPacketDecoder::verify()
{
    std::uint32_t crc_value = *(uint32_t*)( m_packet + ( m_packet_len - 5)) ;
    if( crc_value != my_util::crc32Calc( (uint8_t*)m_payload, m_payload_len)) {
        return false ;
    }

    return true ;
}


string
PlainPacketDecoder::extractStr()
{
    return string( (char*)m_payload, m_payload_len) ;
}


vector<int8_t>
createPlainPacket( const string& str)
{
    vector<int8_t> ret =
        { '!', 'P', 'C', 'T', AsciiCode::RS,
          ' ', ' ', ' ', ' '} ;

    std::uint32_t str_len = str.length() ;

    copy( (int8_t*)&str_len,
          (int8_t*)&str_len + sizeof(str_len),
          ret.begin() + 5) ;

    copy( str.begin(),
          str.end(),
          back_inserter( ret)) ;

    ret.push_back( AsciiCode::LF) ;

    uint32_t crc_value = my_util::crc32Calc( (uint8_t*)str.c_str(), str.length()) ;

    copy( (int8_t*)&crc_value,
          (int8_t*)&crc_value + sizeof(crc_value),
          back_inserter( ret)) ;

    ret.push_back( AsciiCode::DEL) ;

    return move( ret) ;
}
