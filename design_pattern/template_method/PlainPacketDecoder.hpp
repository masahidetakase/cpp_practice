#pragma once
#include <vector>
#include <string>

/**
 * Packet format:
 *   |'!PCT'(4byte)|<RS>|<-string length(4byte)->|<-string without NULL termination->|<LF>|<-CRC(4byte)->|<DEL>|
 */

#include "PacketDecoder.hpp"

class PlainPacketDecoder : public PacketDecoder
{
    private:

        static const std::int8_t Identifier[] ; // = '!PCT" ;
        static const size_t Packet_Length_Min = 15 ;
        static const size_t Offset_Payload_Len = 5 ;
        static const size_t Offset_Payload_Body = 9 ;

        bool
        identify() ;

        bool
        verify() ;

        std::string
        extractStr() ;

} ;


std::vector<std::int8_t>
createPlainPacket( const std::string& str) ;
