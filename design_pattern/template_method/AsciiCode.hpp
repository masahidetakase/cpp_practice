#pragma once

#include <cstdint>

namespace AsciiCode
{
    static const std::int8_t NUL = 0x00 ;
    static const std::int8_t SOH = 0x01 ;
    static const std::int8_t STX = 0x02 ;
    static const std::int8_t ETX = 0x03 ;
    static const std::int8_t EOT = 0x04 ;
    static const std::int8_t LF  = 0x0A ;
    static const std::int8_t CR  = 0x0D ;
    static const std::int8_t ETB = 0x17 ;
    static const std::int8_t FS  = 0x1C ;
    static const std::int8_t GS  = 0x1D ;
    static const std::int8_t RS  = 0x1E ;
    static const std::int8_t US  = 0x1F ;
    static const std::int8_t SPC = 0x20 ;
    static const std::int8_t DEL = 0x7F ;
}

