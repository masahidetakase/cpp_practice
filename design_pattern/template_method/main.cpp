#include "PlainPacketDecoder.hpp"
#include "EncryptedPacketDecoder.hpp"
#include <iostream>
#include <algorithm>
#include <iterator>

using namespace std ;

void
dumpPacket( const vector<int8_t> packet)
{
    cout << hex ;
    copy( packet.begin(), packet.end(), ostream_iterator<int32_t>( cout, " ")) ;
    cout << endl ;
    cout << dec ;
    cout << endl ;
}

int
main( int argc, char* argv[])
{
    string payload_str( "Out, out, brief candle! Life's but a walking shadow.") ;
    char encode_key = '=' ;

    cout << "Payload sentence: " << payload_str << endl << endl ;

    // encode strings.
    auto plain_packet = createPlainPacket( payload_str) ;
    auto encrypted_packet = createEncryptedPacket( payload_str, encode_key) ;

    // Show packets.
    dumpPacket( plain_packet) ;
    dumpPacket( encrypted_packet) ;


    // decode
    PacketDecoder* plain_packet_decoder =
        new PlainPacketDecoder() ;

    PacketDecoder* encrypted_packet_decoder =
        new EncryptedPacketDecoder( encode_key) ;

    string decoded_str1 =
        plain_packet_decoder->decode( plain_packet.data(),
                                      plain_packet.size()) ;

    string decoded_str2 =
        encrypted_packet_decoder->decode( encrypted_packet.data(),
                                          encrypted_packet.size()) ;

    // Show decoded strings.
    cout << "String in plain packet: " << decoded_str1 << endl << endl ;
    cout << "String in encrypted packet: " << decoded_str2 << endl << endl ;

    delete plain_packet_decoder ;
    delete encrypted_packet_decoder ;

    return 0 ;
}
