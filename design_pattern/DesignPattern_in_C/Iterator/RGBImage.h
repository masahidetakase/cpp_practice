/**
 * @file
 * 24bit RGB Image
 */
#ifndef RGBImage_H_
#define RGBImage_H_

#include <stdint.h>
#include <stddef.h>
#ifdef __cplusplus
extern "C" {
#endif

// Constants --------------------------
extern const size_t RGBImage_Bytes_Per_Pixel; // = 3 (24 bit RGB)

typedef void* RGBImage;

/**
 * @brief Create RGBImage obj.
 * @param width Image width.
 * @param height Image height.
 * @param raw_file_path If it is not NULL, RGB Raw image is loaded.
 * @retval non-NULL RGBImage object.
 * @retval NULL Creating object is Failed.
 */
RGBImage
RGBImage_create( const uint32_t width,
                 const uint32_t height,
                 const char* raw_file_path);
/**
 * @brief Delete an RGBImage obj.
 * @param rgb_image RGBImage obj.
 */
void
RGBImage_dispose( const RGBImage rgb_image);

/**
 * @brief validate the RGBImage obj.
 * @param rgb_image RGBImage obj.
 */
bool
RGBImage_isValid( const RGBImage rgb_image);

/**
 * @brief Get the image width.
 * @param rgb_image RGBImage obj.
 * @return the image width.
 */
uint32_t
RGBImage_getWidth( const RGBImage rgb_image);

/**
 * @brief Get the image height.
 * @param rgb_image RGBImage obj.
 * @return the image height.
 */
uint32_t
RGBImage_getHeight( const RGBImage rgb_image);

uint8_t*
RGBImage_getPixel( const RGBImage rgb_image,
                   const uint32_t x,
                   const uint32_t y);

void
RGBImage_setPixel( const RGBImage rgb_image,
                   const uint32_t x,
                   const uint32_t y,
                   const uint8_t red,
                   const uint8_t green,
                   const uint8_t blue);

#ifdef __cplusplus
}
#endif

#endif // RGBImage_H_