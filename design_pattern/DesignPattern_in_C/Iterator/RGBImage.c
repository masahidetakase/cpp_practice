/**
 * @file
 * 24bit RGB Image
 */
#include "RGBImage.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <assert.h>

// The type ID of RGBImage.
static const uint32_t TYPE_ID = 38424752; // "RGB8"

// Bytes per pixel
const size_t RGBImage_Bytes_Per_Pixel = 3; // 24 bit RGB

// The ADT of this object.
 typedef struct
 {
    uint32_t type_id; // Type ID

    uint32_t width;
    uint32_t height;

    uint8_t* image;

 } RGBImageInst;


// Local functions.------------------------------------------
static bool
loadRawImageFile( RGBImageInst* const rgb_image,
                  const char* raw_file_path);

// Implementations of public methods. ------------------------

RGBImage
RGBImage_create( const uint32_t width,
                 const uint32_t height,
                 const char* raw_file_path)
{
    // preconditions.
    assert( 0 < width);
    assert( 0 < height);

    RGBImageInst* image_obj = (RGBImageInst*)malloc( sizeof(RGBImageInst));

    if( image_obj == (RGBImageInst*)NULL) {
        goto Error;
    }

    image_obj->type_id = 0;
    image_obj->width = width;
    image_obj->height = height;

    image_obj->image =  (uint8_t*)calloc( 1, width * height * 3);
    if( image_obj->image == (uint8_t*)NULL) {
        goto Error;
    }

    // Load the image file.
    if( raw_file_path != NULL) {
        if( loadRawImageFile( image_obj, raw_file_path)) {
            goto Error;
        }
    }

    // set type ID.
    image_obj->type_id = TYPE_ID;

    return (RGBImage)image_obj;

Error:
    if( image_obj != NULL) {
        image_obj->type_id = 0;
        free( image_obj->image);
    }

    free( image_obj);

    return NULL;
}

void
RGBImage_dispose( const RGBImage rgb_image)
{
    RGBImageInst* const image_obj = (RGBImageInst*)rgb_image;

    // preconditions
    assert( rgb_image != (RGBImage)NULL);
    assert( image_obj->type_id == TYPE_ID);

    free( image_obj->image);
    image_obj->type_id = 0;
    image_obj->image = (uint8_t*)NULL;
    free( image_obj);
}


bool
RGBImage_isValid( const RGBImage rgb_image)
{
    RGBImageInst* const image_obj = (RGBImageInst*)rgb_image;

    // preconditions
    assert( rgb_image != (RGBImage)NULL);

    return ( image_obj->type_id == TYPE_ID) &&
           ( 0 < image_obj->width) &&
           ( 0 < image_obj->height) &&
           ( image_obj->image != (uint8_t*)NULL);
}


uint32_t
RGBImage_getWidth( const RGBImage rgb_image)
{
    RGBImageInst* const image_obj = (RGBImageInst*)rgb_image;

    // preconditions
    assert( rgb_image != (RGBImage)NULL);
    assert( image_obj->type_id == TYPE_ID);

    return image_obj->width;
}

uint32_t
RGBImage_getHeight( const RGBImage rgb_image)
{
    RGBImageInst* const image_obj = (RGBImageInst*)rgb_image;

    // preconditions
    assert( rgb_image != (RGBImage)NULL);
    assert( image_obj->type_id == TYPE_ID);

    return image_obj->height;
}

uint8_t*
RGBImage_getPixel( const RGBImage rgb_image,
                   const uint32_t x,
                   const uint32_t y)
{
    RGBImageInst* const image_obj = (RGBImageInst*)rgb_image;

    // preconditions
    assert( rgb_image != (RGBImage)NULL);
    assert( image_obj->type_id == TYPE_ID);
    assert( x < image_obj->width);
    assert( y < image_obj->height);

    return image_obj->image + 3 * ( image_obj->width * y + x);
}

void
RGBImage_setPixel( const RGBImage rgb_image,
                   const uint32_t x,
                   const uint32_t y,
                   const uint8_t red,
                   const uint8_t green,
                   const uint8_t blue)
{
    RGBImageInst* const image_obj = (RGBImageInst*)rgb_image;

    // preconditions
    assert( rgb_image != (RGBImage)NULL);
    assert( image_obj->type_id == TYPE_ID);
    assert( x < image_obj->width);
    assert( y < image_obj->height);

    uint8_t* pixel = image_obj->image + 3 * ( image_obj->width * y + x);

    *(pixel++) = red;
    *(pixel++) = green;
    *pixel = blue;
}



// Local functions. --------------------------------------------
static bool
loadRawImageFile( RGBImageInst* const image_obj,
                  const char* raw_file_path)
{
    // preconditions
    assert( image_obj != (RGBImageInst*)NULL);
    assert( 0 < image_obj->width);
    assert( 0 < image_obj->height);
    assert( raw_file_path != (char*)NULL);

    FILE* fp = fopen( raw_file_path, "rb");
    if( fp == NULL) { return false;}

    size_t read_line_count =
        fread( image_obj->image, 3 * image_obj->width, image_obj->height, fp);

    fclose( fp);

    if( read_line_count != image_obj->height) {
        return false;
    }

    return true;
}
