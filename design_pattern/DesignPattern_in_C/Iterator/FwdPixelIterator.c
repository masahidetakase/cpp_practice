/**
 * @file
 * Forward iterators to 24 bit RGB Pixel.
 */
#include "FwdPixelIterator.h"
#include <stdlib.h>
#include <assert.h>

// Forward Pixel iterator ---------------

// Properties of forward pixel iterator.
typedef struct
{
    uint8_t* begin;
    uint8_t* end;
    uint8_t* current;
} FwdPixelIterProp;

bool
FwdPixelIter_moveNext( PixelIteratorIF* const iter)
{
    // preconditions
    assert( iter != (PixelIteratorIF*)NULL);
    FwdPixelIterProp* prop = (FwdPixelIterProp*)iter->properties;
    assert( prop != (FwdPixelIterProp*)NULL);
    assert( prop->begin != (uint8_t*)NULL);
    assert( prop->end != (uint8_t*)NULL);
    assert( prop->begin < prop->end);

    if( prop->current == (uint8_t*)NULL) {
        prop->current = prop->begin;
    } else if( prop->current < prop->end) {
        prop->current += RGBImage_Bytes_Per_Pixel;
    } else {
        prop->current = prop->end;
    }

    return prop->current != prop->end;
}

uint8_t*
FwdPixelIter_current( PixelIteratorIF* const iter)
{
    // preconditions
    assert( iter != ( PixelIteratorIF*)NULL);
    FwdPixelIterProp* prop = (FwdPixelIterProp*)iter->properties;
    assert( prop != (FwdPixelIterProp*)NULL);
    assert( prop->begin != (uint8_t*)NULL);
    assert( prop->end != (uint8_t*)NULL);
    assert( prop->begin < prop->end);
    // current() must be called  moveNext().
    assert( prop->current != (uint8_t*)NULL);

    if( prop->current < prop->end) {
        return prop->current;
    } else {
        return (uint8_t*)NULL;
    }
}

void
FwdPixelIter_dispose( PixelIteratorIF* const iter)
{
    if( iter != (PixelIteratorIF*)NULL) {
        free( iter->properties);
    }

    free( iter);
}

void
FwdPixelIter_reset( PixelIteratorIF* const iter)
{
    // preconditions
    assert( iter != ( PixelIteratorIF*)NULL);
    FwdPixelIterProp* prop = (FwdPixelIterProp*)iter->properties;
    assert( prop->begin != (uint8_t*)NULL);
    assert( prop->end != (uint8_t*)NULL);
    assert( prop->begin < prop->end);

    prop->current = (uint8_t*)NULL;
}

static void
initFwdPixelIterProp( FwdPixelIterProp* const prop,
                      const RGBImage rgb_image)
{
    // preconditions
    assert( prop != (FwdPixelIterProp*)NULL);

    prop->begin = RGBImage_getPixel( rgb_image, 0, 0);

    prop->end = RGBImage_getPixel( rgb_image,
                                   RGBImage_getWidth( rgb_image) - 1,
                                   RGBImage_getHeight (rgb_image) - 1);

    prop->end += RGBImage_Bytes_Per_Pixel;

    prop->current = (uint8_t*)NULL;
}

PixelIteratorIF*
FwdPixelIter_create( const RGBImage rgb_image)
{
    PixelIteratorIF* iter = (PixelIteratorIF*)NULL; // iterator
    FwdPixelIterProp* prop = (FwdPixelIterProp*)NULL; // properties of iterator.

    // preconditions
    assert( rgb_image != (RGBImage)NULL);
    assert( RGBImage_isValid( rgb_image));

    // Allocate memory for iterator.
    {
        iter = (PixelIteratorIF*)malloc( sizeof(PixelIteratorIF));
        if( iter == (PixelIteratorIF*)NULL) { goto Error;}

        prop = (FwdPixelIterProp*)malloc( sizeof(FwdPixelIterProp));
        if( prop == (FwdPixelIterProp*)NULL) { goto Error;}
    }

    initFwdPixelIterProp( prop, rgb_image);

    iter->properties = prop;
    iter->moveNext = FwdPixelIter_moveNext;
    iter->current = FwdPixelIter_current;
    iter->reset = FwdPixelIter_reset;

    return iter;
Error:
    free( prop);
    free( iter);

    return (PixelIteratorIF*)NULL;
}

