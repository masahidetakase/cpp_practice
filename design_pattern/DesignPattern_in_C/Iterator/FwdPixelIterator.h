/**
 * @file
 * Forward iterators to 24 bit RGB Pixel.
 */
#ifndef FwdPixelIterator_H_
#define FwdPixelIterator_H_

#include "IteratorIF.h"
#include "RGBImage.h"

#ifdef __cplusplus
extern "C" {
#endif

// Region of interest.
typedef struct
{
    uint32_t left;
    uint32_t top;
    uint32_t right;
    uint32_t bottom;
} ROI;

// Declaration of 24bit RGB pixel iterator interface.
IteratorIF_Declare(uint8_t)

// The alias of the interface.
typedef IteratorIF_Type(uint8_t) PixelIteratorIF;

// Factory for forward iterator
PixelIteratorIF*
FwdPixelIter_create( const RGBImage rgb_image);

// Factory for forward iterator in ROIs.
PixelIteratorIF*
RoiPixelIter_create( const RGBImage rgb_image,
                     const ROI* rois,
                     const size_t num_rois);

// Factory for random sampling iterator.
PixelIteratorIF*
RandomSamplePixelIter_create( const RGBImage rgb_image);

#ifdef __cplusplus
}
#endif

#endif // FwdPixelIterator_H_