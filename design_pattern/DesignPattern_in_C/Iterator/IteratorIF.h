/**
 * @file
 * Interface of iterator.
 */
#ifndef IteratorIF_H_
#define IteratorIF_H_

#include <stdbool.h>
#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Declare the interface of the iterator indicating "TypeName" object.
 * @details Declared interface type name is "TypeName"_IteratorIF.
 */
#define IteratorIF_Declare( TypeName) \
typedef struct tag_ ## TypeName ## _IteratorIF \
{ \
    void* properties; \
    \
    bool (*moveNext)( struct tag_ ## TypeName ## _IteratorIF* const iter); \
    \
    TypeName* (*current)( struct tag_ ## TypeName ## _IteratorIF* const iter); \
    \
    void (*reset)( struct tag_ ## TypeName ## _IteratorIF* const iter); \
    \
    void (*dispose)( struct tag_ ## TypeName ## _IteratorIF* const iter);\
    \
} TypeName ## _IteratorIF;

#define IteratorIF_Type( TypeName) TypeName ## _IteratorIF

// Move forward to the next element.
#define Iterator_moveNext( iter) ((iter)->moveNext( (iter)))

// Get the current element.
#define Iterator_current( iter) ((iter)->current( (iter)))

// Dispose the iterator.
#define Iterator_dispose( iter) ((iter)->dispose( (iter)))

#ifdef __cplusplus
}
#endif

#endif // IteratorIF_H_
