#include <iostream>
#include "FileStorage.hpp"
#include "CachedStorage.hpp"
#include "DataRepository.hpp"
#include "ThreadSafeDataRepository.hpp"

using namespace std ;

int main( int argc, char* argv[])
{
    DataRepository data_repository( new FileStorage( "record.dat")) ;

    data_repository.store( "neko", 12) ;

    data_repository.store( "usagi", -2) ;

    data_repository.store( "buta", 256) ;

    int32_t val = data_repository.find( "neko") ;

    cout << "value:" << val << endl ;

    cout << "ThreadsafeRepository" << endl ;

    ThreadSafeDataRepository threadsafeRepo( new FileStorage( "record2.dat")) ;

    threadsafeRepo.store( "ushi", 12) ;

    cout << "key: ushi  value: " << threadsafeRepo.find( "ushi") << endl ; 

    return 0 ;
}
