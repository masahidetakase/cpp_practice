#include "CachedStorage.hpp"
#include <algorithm>
#include <iostream>

using namespace std ;

CachedStorage::CachedStorage( const string& data_file_path)
    : FileStorage( data_file_path) {}


CachedStorage::~CachedStorage() {}


bool
CachedStorage::store( const string& key, const int32_t& value)
{
    if( key.empty() || ( key.length() == 0)) {
        return false ;
    }

    pushCache( key, value) ;

    return FileStorage::store( key, value) ;
}

bool
CachedStorage::find( const string& key, int32_t* const value)
{
    if( key.empty() || ( key.length() == 0) || ( value == nullptr)) {
        return false ;
    }

    vector<CachedRecord>::iterator iter = findInCache( key) ;
    if( iter != m_cache.end()) {
#ifdef DEBUG
        cerr << "Cache hit" << endl ;
#endif
        *value = iter->value ;
        return true ;
    }

    if( FileStorage::find( key, value)) {
        pushCache( key, *value) ;
        return true ;
    }

    if( Cache_Refresh_Freq <= ++m_reference_times) {
        resetCacheRefCounter() ;
    }

    return false ;
}


vector<CachedStorage::CachedRecord>::iterator
CachedStorage::findInCache( const std::string& key)
{
    vector<CachedRecord>::iterator iter ;

    for( iter = m_cache.begin() ;
         iter != m_cache.end() ;
         ++iter) {
        if( iter->key == key) {
            iter->nextGen() ;
            break ;
        }
    }

    return iter ;
}


void
CachedStorage::pushCache( const string& key, const int& value)
{
    struct equalByKey
    {
        equalByKey( const string& key)
            : _key( key) {}

        bool
        operator() ( const CachedRecord& op)
        {
            return op.key == _key ;
        }

        string _key ;
    } ;

    struct lessByGen
    {
        bool
        operator() ( const CachedRecord& op1, const CachedRecord& op2)
        {
            if( op1.generation < op2.generation) {
                return true ;
            } else if( op1.generation == op2.generation) {
                return op1.key.length() < op2.key.length() ;
            } else {
                return false ;
            }
        }
    } ;

    // Already cached
    vector<CachedRecord>::iterator iter =
        find_if( m_cache.begin(), m_cache.end(), equalByKey( key)) ;

    if( iter != m_cache.end()) {
        iter->value = value ;
        return ;
    }

    // When the cache is full,  most rarely referred items are removed.
    if( Num_Cached_Rercords_Max <= m_cache.size()) {
        size_t num_removed_record =
            m_cache.size() - Num_Cached_Rercords_Max + 1 ;

        partial_sort( m_cache.begin(),
                      m_cache.begin() + num_removed_record,
                      m_cache.end(),
                      lessByGen()) ;

        m_cache.erase( m_cache.begin(), m_cache.begin() + num_removed_record) ;
    }

    m_cache.push_back( CachedRecord( key, value)) ;
}


void
CachedStorage::resetCacheRefCounter()
{
    struct resetRefCounter
    {
        void
        operator() ( CachedRecord& record)
        {
            record.generation = 0 ;
        }
    } ;

    for_each( m_cache.begin(), m_cache.end(), resetRefCounter()) ;

    m_reference_times = 0 ;
}