#pragma once

#include <string>
#include <cstdint>

/* データ・アクセサのIF。*/
class DataStorage
{
    public:
        DataStorage() {}

        virtual
        ~DataStorage() {}

        virtual bool
        store( const std::string& key, const std::int32_t& value) = 0 ;

        virtual bool
        find( const std::string& key, std::int32_t* const value) = 0 ;
} ;
