#pragma once

#include "DataRepository.hpp"
#include <mutex>

class ThreadSafeDataRepository : public DataRepository
{
    public:
        ThreadSafeDataRepository( DataStorage* const storage) ;

        virtual ~ThreadSafeDataRepository() {}

        void
        store( const std::string& key, const std::int32_t& value) ;

        std::int32_t
        find( const std::string& key) ;

    private:
        std::mutex m_mutex ;
} ;

