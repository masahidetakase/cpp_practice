#include "FileStorage.hpp"
#include <stdexcept>
#include <algorithm>
#include <iterator>
#include <iostream>


using namespace std ;

FileStorage::FileStorage( const string& data_file_path)
    : m_filename( data_file_path),
      m_data_file_stream( data_file_path, ios::in|ios::out|ios::binary)
{
    if( !m_data_file_stream.is_open()) {
        // create a new data file.
        m_data_file_stream.close() ;
        m_data_file_stream.open( m_filename, ios::in|ios::out|ios::trunc) ;

        if( !m_data_file_stream.is_open()) {
            throw domain_error( string("Opening ") + data_file_path + " failed.\n") ;
        }
    }
}


FileStorage::~FileStorage()
{
    m_data_file_stream.close() ;
}


bool
FileStorage::store( const string& key, const int32_t& value)
{
    if( !moveRecordPos( key)) {
        m_data_file_stream.clear() ;
        m_data_file_stream.seekg( 0, ios::end) ;
    }

    // make a record
    fill( m_record_buf, m_record_buf + Record_Value_Offset, '\0') ;
    copy( key.begin(), key.end(), m_record_buf) ;
    *(int32_t*)( m_record_buf + Record_Value_Offset) = value ;

    // write a record.
    m_data_file_stream.write( m_record_buf, Record_Len) ;

    return !m_data_file_stream.fail() ;
}


bool
FileStorage::find( const std::string& key, std::int32_t* const value)
{
    if( moveRecordPos( key) && !m_data_file_stream.eof()) {
        *value = *(int32_t*)( m_record_buf + Record_Value_Offset) ;
        return true ;
    } else {
        m_data_file_stream.clear();
        return false ;
    }
}


bool
FileStorage::moveRecordPos( const std::string& key)
{
    if( key.empty()) { return false ;}

    m_data_file_stream.clear() ;

    bool ret = false ;

    m_data_file_stream.seekg( 0, ios::beg) ;

    while( !m_data_file_stream.eof()) {
        streamsize read_len = ( m_data_file_stream.read( m_record_buf, Record_Len)).gcount() ;

        if( read_len == Record_Len) {
            if( key == m_record_buf + Record_Key_Offset) {
                m_data_file_stream.clear() ;
                m_data_file_stream.seekg( - Record_Len, ios::cur) ;
                ret = true ;
                break ;
            }
        }
    }

    return ret ;
}

