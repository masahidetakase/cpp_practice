#include "DataRepository.hpp"
#include "DataStorage.hpp"
#include <stdexcept>
#include <limits>

using namespace std ;

DataRepository::DataRepository( DataStorage* const storage)
    : m_storage( storage) {}


DataRepository::~DataRepository() {}

void
DataRepository::store( const std::string& key, const std::int32_t& value)
{
    if( !m_storage->store( key, value)) {
        throw domain_error( "I/O error") ;
    }
}

std::int32_t
DataRepository::find( const std::string& key)
{
    int32_t value ;
    if( !m_storage->find( key, &value)) {
        value = std::numeric_limits<int>::min() ;
    }

    return value ;
}
