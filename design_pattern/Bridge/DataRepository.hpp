#pragma once

#include <string>
#include <cstdint>

class DataStorage ;

/** The class to store and find data with keys in a data storage. */
class DataRepository
{
    public:
        /**
         * @brief Create a repository with a data storage.
         * @param storage a data storage.
         */
        DataRepository( DataStorage* const storage) ;

        /**
         * @brief Close the storage.
         */
        virtual ~DataRepository() ;

        /**
         * @brief Store a 32 bit signed integer paired with a unique key string.
         *
         * @param key A string as.
         * @param value 32 bit signed integer.
         */
        virtual void
        store( const std::string& key, const std::int32_t& value) ;


        /**
         * @brief find
         * @details [long description]
         *
         * @param key a string.
         * @retval INT_MIN data not found.
         * @retval otherwise a value.
         */
        virtual std::int32_t
        find( const std::string& key) ;

    private:
        DataStorage* const m_storage ;
} ;
