#pragma once

#include "FileStorage.hpp"
#include <vector>

class CachedStorage : public FileStorage
{
    public:
        CachedStorage( const std::string& data_file_path) ;

        virtual ~CachedStorage() ;

        bool
        store( const std::string& key, const std::int32_t& value) ;

        bool
        find( const std::string& key, std::int32_t* const value) ;

    private:
        static const size_t Num_Cached_Rercords_Max = 6 ;
        static const size_t Cache_Refresh_Freq = 10 ;

        size_t m_reference_times ;

        struct CachedRecord
        {
            CachedRecord( const std::string& _key, const int& _value)
                : key( _key),
                  value( _value),
                  generation( 0) {}

            void
            nextGen() { ++generation ;}

            std::string key ;
            int value ;
            std::uint32_t generation ;
        } ;

        std::vector<CachedRecord> m_cache ;

        std::vector<CachedRecord>::iterator
        findInCache( const std::string& key) ;

        void
        pushCache( const std::string& key, const int& value) ;

        void
        resetCacheRefCounter() ;

} ;
