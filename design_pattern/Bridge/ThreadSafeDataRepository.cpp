#include "ThreadSafeDataRepository.hpp"

using namespace std;

ThreadSafeDataRepository::ThreadSafeDataRepository( DataStorage* const storage)
    : DataRepository( storage) {}


void
ThreadSafeDataRepository::store( const std::string& key, const std::int32_t& value)
{
    lock_guard<mutex>  locker( m_mutex) ;

    DataRepository::store( key, value) ;
}



int32_t
ThreadSafeDataRepository::find( const std::string& key)
{
    lock_guard<mutex>  locker( m_mutex) ;

    return DataRepository::find( key) ;
}
