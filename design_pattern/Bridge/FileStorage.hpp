#pragma once
#include <cstdint>
#include <string>
#include <fstream>
#include "DataStorage.hpp"

/*
   Structure of record
  |<key(ASCII string with NUL termination) 64byte>|<signed integer value>4byte|
 */

class FileStorage : public DataStorage
{
    public:
        FileStorage( const std::string& data_file_path) ;

        virtual ~FileStorage() ;

        bool
        store( const std::string& key, const std::int32_t& value) ;

        bool
        find( const std::string& key, std::int32_t* const value) ;

    private:
        static const int Record_Len = 64 + sizeof(int32_t) ;
        static const int Record_Key_Offset = 0 ;
        static const int Record_Value_Offset = 64 ;

        std::string m_filename ;

        std::fstream m_data_file_stream ;

        char m_record_buf[Record_Len] ;

        bool
        moveRecordPos( const std::string& key) ;
} ;


