#include "SVGDocument.hpp"
#include <cassert>
#include <stdexcept>
#include <utility>
#include <iomanip>
#include <cmath>

using namespace std ;

namespace svg_doc
{

SVGDocument::SVGDocument( const double& page_width_mm,
                          const double& page_height_mm)
    : m_paper_width_mm( page_width_mm),
      m_paper_height_mm( page_height_mm)
{
    if( ( m_paper_width_mm < Paper_Width_mm_Min) ||
        ( m_paper_height_mm < Paper_Height_mm_Min) ||
        ( Paper_Width_mm_Max < m_paper_width_mm) ||
        ( Paper_Height_mm_Max < m_paper_height_mm))
    {
        std::invalid_argument( "SVGDocument: unsupported document size.") ;
    }
}

SVGDocument::SVGDocument( const SVGDocument& src)
    : m_paper_width_mm( src.m_paper_width_mm),
      m_paper_height_mm( src.m_paper_height_mm),
      m_draw_object_list()
{
    for( auto obj : src.m_draw_object_list) {
        m_draw_object_list.push_back( shared_ptr<SVGDrawObject>( obj->clone())) ;
    }
}

void
SVGDocument::addObject( const shared_ptr<SVGDrawObject>& draw_object)
{
    m_draw_object_list.push_back( draw_object) ;
}

} // the end of svg_doc namespace


ostream&
operator<< ( ostream& ostrm, svg_doc::SVGDocument& doc)
{
    std::streamsize original_precision = ostrm.precision() ;
    std::ios_base::fmtflags original_flag = ostrm.flags() ;

    // header of SVG document.
    ostrm << fixed << setprecision(2) <<
             "<svg xmlns=\"http://www.w3.org/2000/svg\" " << endl <<
             "     xmlns:xlink=\"http://www.w3.org/1999/xlink\" " << endl <<
             "     width=" << doc.width() << "mm\"" << endl <<
             "     height=" << doc.height() << "mm\"" << endl <<
             ostrm << fixed << setprecision(2) <<
             "viewBox=\"0 0 " <<
             (int)ceil( doc.width()) << " " <<
             (int)ceil( doc.height()) << "\">" << endl ;


    for( svg_doc::SVGDocument::object_list::const_iterator obj_iter = doc.objectBegin() ;
         obj_iter != doc.objectEnd() ;
         ++obj_iter) {
        ostrm << (*obj_iter)->draw() ;
    }
    // end of SVG document.
    ostrm << "</svg>" << endl ;

    ostrm.precision( original_precision) ;
    ostrm.flags( original_flag) ;

    return ostrm ;
}
