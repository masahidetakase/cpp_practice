#pragma once

#include <string>
#include <ostream>
#include <memory>
#include <vector>
#include "SVGDrawObjectBase.hpp"

namespace svg_doc
{

class SVGDocument
{
    public:
        typedef std::vector<std::shared_ptr<SVGDrawObject>> object_list ;

        /**
         * @brief Create an one-page SVG Document.
         *
         * @param page_width_mm  page width (unit:mm).
         * @param page_height_mm page height (unit:mm).
         *
         */
        SVGDocument( const double& page_width_mm,
                     const double& page_height_mm) ;

        SVGDocument( const SVGDocument& src) ;

        SVGDocument( const SVGDocument&& src) = delete ;

        /**
         * @brief Discard a svg document.
         * @details [long description]
         */
        virtual ~SVGDocument() {}

        const SVGDocument&
        operator= ( const SVGDocument& rhs) = delete ;

        const SVGDocument&
        operator= ( const SVGDocument&& rhs) = delete ;

        void
        addObject( const std::shared_ptr<SVGDrawObject>& draw_object) ;


        object_list::const_iterator
        objectBegin() const { return m_draw_object_list.cbegin() ;}

        object_list::const_iterator
        objectEnd() const { return m_draw_object_list.cend() ;}

        const double&
        width() const { return m_paper_width_mm;}

        const double&
        height() const { return m_paper_height_mm;}

    private:
        static constexpr double Paper_Width_mm_Min = 128. ;
        static constexpr double Paper_Height_mm_Min = 182. ;
        static constexpr double Paper_Width_mm_Max = 841. ;
        static constexpr double Paper_Height_mm_Max = 11898. ;

        const double m_paper_width_mm ;
        const double m_paper_height_mm ;

        object_list m_draw_object_list ;
} ;

} // the end of svg_doc namespace.

std::ostream&
operator<< ( std::ostream& ostrm, svg_doc::SVGDocument& svg_doc) ;