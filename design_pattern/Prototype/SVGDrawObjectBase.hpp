#pragma once
#include <string>
#include <cassert>
#include <sstream>
#include <ios>

#include "SVGPoint.hpp"

namespace svg_doc
{

class SVGDrawObject
{
    public:
        virtual SVGDrawObject*
        clone() = 0 ;

        void
        setScale( const double& scale_rate)
        {
            m_scale_rate = scale_rate ;
        }

        void
        setPosition( const double& x_mm, const double& y_mm)
        {
            m_position.x_mm = x_mm ;
            m_position.y_mm = y_mm ;
        }

        virtual std::string
        draw() = 0 ;

        const std::string&
        name() const { return m_name;}

    protected:
        SVGDrawObject( const std::string& obj_name)
            : m_name( obj_name),
              m_scale_rate( 1.),
              m_position( 0., 0.) {}

        SVGDrawObject( const SVGDrawObject& src)
            : m_name( src.m_name),
              m_scale_rate( src.m_scale_rate),
              m_position( src.m_position) {}

        SVGDrawObject( const SVGDrawObject&& src)
            : m_name( src.m_name),
              m_scale_rate( src.m_scale_rate),
              m_position( src.m_position) {}

        const SVGDrawObject&
        operator= ( const SVGDrawObject& rhs) ;

        const SVGDrawObject&
        operator= ( const SVGDrawObject&& rhs) ;

    protected:
        std::string
        makeTransformStr()
        {
            std::stringstream transform_strm ;

            transform_strm << std::fixed << std::setprecision(2) <<
                "transform=\"" <<
                "translate" << m_position <<
                "scale( " << m_scale_rate << ", " << m_scale_rate << ") " <<
                "\"" ;

            return transform_strm.str() ;
        }

    private:
        std::string m_name ;

        double m_scale_rate ;

        Point m_position ;
} ;

} // the end of svg_doc
