#pragma once
#include <sstream>
#include <iomanip>
#include <stdexcept>

namespace svg_doc
{

struct Point
{
    Point( const double& sx, const double& sy)
        : x_mm( sx), y_mm( sy) {}

    double x_mm, y_mm ;

    std::string
    str() const
    {
        std::stringstream strm ;

        strm << std::fixed << std::setprecision(2) ;

        strm << x_mm << "mm," << y_mm << "mm" ;

        return strm.str() ;
    }

    Point
    operator+ ( const Point& rhs) const
    {
        return Point( x_mm + rhs.x_mm, y_mm + rhs.y_mm) ;
    }

    Point
    operator- ( const Point& rhs) const
    {
        return Point( x_mm - rhs.x_mm, y_mm - rhs.y_mm) ;
    }

    Point
    operator* ( const double& rhs) const
    {
        return Point( x_mm * rhs, y_mm * rhs) ;
    }

    Point
    operator/ ( const double& rhs) const
    {
        if( rhs == 0.) {
            throw std::domain_error( "divided by zero!") ;
        }

        return Point( x_mm / rhs, y_mm / rhs) ;
    }

    const Point&
    operator+= ( const Point& rhs)
    {
        this->x_mm += rhs.x_mm ;
        this->y_mm += rhs.y_mm ;

        return *this ;
    }

    const Point&
    operator-= ( const Point& rhs)
    {
        this->x_mm -= rhs.x_mm ;
        this->y_mm -= rhs.y_mm ;

        return *this ;
    }

    const Point&
    operator*= ( const double& rhs)
    {
        this->x_mm *= rhs ;
        this->y_mm *= rhs ;

        return *this ;
    }

    const Point&
    operator/= ( const double& rhs)
    {
        if( rhs == 0.) {
            throw std::domain_error( "divided by zero!") ;
        }

        this->x_mm /= rhs ;
        this->y_mm /= rhs ;

        return *this ;
    }

    bool
    operator== ( const Point& rhs) const
    {
        return ( x_mm == rhs.x_mm) && ( y_mm == rhs.y_mm) ;
    }

    bool
    operator!= ( const Point& rhs)
    {
        return ( x_mm != rhs.x_mm) || ( y_mm != rhs.y_mm) ;
    }
} ;

} // the end of svg_doc namespace

std::ostream&
operator<< (std::ostream& ostrm, const svg_doc::Point& point)
{
    std::streamsize original_precision = ostrm.precision() ;
    std::ios_base::fmtflags original_flag = ostrm.flags() ;

    ostrm << std::fixed << std::setprecision(2) ;

    ostrm << "( " << point.x_mm << "mm, " << point.y_mm << "mm)" ;

    ostrm.precision( original_precision) ;
    ostrm.flags( original_flag) ;

    return ostrm ;
}
