#include "Triangle.hpp"
#include <ios>

namespace svg_doc
{

using namespace std ;

int32_t Triangle::m_count = 0 ;

Triangle::Triangle( const Point& a, const Point& b, const Point& c)
    : SVGDrawObject( "triangle"),
      m_a( a),
      m_b( b),
      m_c( c)
{
    // unique id
    ++m_count ;

    // get the center of gravity.
    Point center = ( m_a + m_b + m_c) / 3 ;

    m_a -= center ;
    m_b -= center ;
    m_c -= center ;
}


SVGDrawObject*
Triangle::clone()
{
    return new Triangle( m_a, m_b, m_c) ;
}


std::string
Triangle::draw()
{
    std::stringstream path_strm ;

    path_strm <<
        "<g id=\"triangle_" << m_count << "\"" <<
        "stroke=\"purple\" stroke-width=\"1\"  fill=\"none\"" <<
        makeTransformStr() <<
        ">" << endl ;

    path_strm <<
        std::fixed << setprecision(2) <<
        "<path  d=" <<
        "    \"M\"" << m_a << endl <<
        "    \"L\"" << m_b << endl <<
        "    \"L\"" << m_c << endl <<
        ">" << endl ;

    return path_strm.str() ;
}

} // the end of svg_doc namespace.