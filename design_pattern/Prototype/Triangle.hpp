#include "SVGDrawObjectBase.hpp"
#include <cstdint>

namespace svg_doc
{

class Triangle : public SVGDrawObject
{
    public:
        Triangle( const Point& a, const Point& b, const Point& c) ;

        SVGDrawObject*
        clone() ;

        std::string
        draw() ;

    private:
        static int32_t  m_count ;

        Point m_a, m_b, m_c ;
} ;

} // the end of svg_doc namespace.
