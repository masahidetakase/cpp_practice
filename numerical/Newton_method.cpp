#include <stdio.h>
#include <float.h>

using namespace std;

#define ABS(x) ((x) < 0 ? -(x) : (x))
#define SQUARED(x) ((x)*(x))
#define CALC_CYCLE_COUNT_MAX (100)
#define RESIDUCE_ABS_MIN (1.E-5) // unit:m
#define CONVERSION_RATE_THRESH (1.E-4)
#define EPSILON (1.0-19)

#if RESIDUCE_ABS_MIN <

bool
calcEachHorizShift( const double& disp_prev,
                    const double& disp_cur,
                    const double& move_dist_m,
                    const double& bf_mpx,
                    double const* a)
{
    /*
        E(a) = (Move distance) - Bf * ((a + Dprev)^(-1) - (a + Dcur)^(-1))
        dMvDist/da = Bf * ((a + Dprev)^(-2) - (a + Dcur)^(-2))
        a(k+1) =  a(k) - E(a) / (dMvDist/da)
    */
    bool has_succeeded = true;
    int calc_count = 0;

    double prev_resid = EPSILON, cur_resid; // residuce
    double sect_prev, sect_cur; // a + Dprev, a + Dcur
    double dmv_da; // d(Move distance) / da
    double delta_a; // minute increment of a
    double conversion_rate;

    a = 0.;

    for( ;
         ( calc_count < CALC_CYCLE_COUNT_MAX) &&
         ( CONVERSION_RATE_THRESH < conversion_rate) &&
         (  RESIDUCE_ABS_MIN < ABS(prev_resid)) ;
         ++calc_count) {

        if( (a + disp_prev != 0.) && (a + disp_prev != 0.)) {
            has_succeeded = false;
        } else {
            sect_prev = 1. / ( a + disp_prev);
            sect_cur = 1. / ( a + disp_cur);
            cur_resid = move_dist_m - bf_mpx * ( sect_prev - sect_cur);

            if( EPSILON < ABS(prev_resid) {
                conversion_rate = ABS( (cur_resid - prev_resid) / prev_resid);
            } else {
                conversion_rate = 0.;
            }

            dmv_da = bf_mpx * ( SQUARED(sect_prev) - SQUARED(sect_cur));
            delta_a = - cur_resid / dmv_da;
            a += delta_a;

            prev_resid = cur_resid;

            printf( "Calc count: %02d\n", calc_count);
            printf( "a: %.16f\n", a);
            printf( "prev_resid: %.16f\n", prev_resid);
            printf( "cur_resid: %.16f\n", cur_resid);
            printf( "Convergence Rate: %.16f\n", conversion_rate);

        }

    return a;
}

int main( int argc, char* argv[])
{
    const double L = 13.2;
    const double CameraBf = 114.2578;
    const double mv_dist = 1.2;
    const double a = 0.5;
    const double disp_prev = CameraBf / L - a;
    const double disp_cur = CameraBf / ( L - mv_dist) - a;

    double true_a =
        calcEachHorizShift( disp_prev, disp_cur, mv_dist, CameraBf);

    return 0;
}
