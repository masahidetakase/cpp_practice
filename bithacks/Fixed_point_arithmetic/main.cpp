#include <iostream>
#include <cstring>
#include <cmath>


typedef struct tagFixedPointValue_RawBit
{ 
    unsigned char bit31 : 1 ;
    unsigned char bit30 : 1 ;
    unsigned char bit29 : 1 ;
    unsigned char bit28 : 1 ;
    unsigned char bit27 : 1 ;
    unsigned char bit26 : 1 ;
    unsigned char bit25 : 1 ;
    unsigned char bit24 : 1 ;
    unsigned char bit23 : 1 ;
    unsigned char bit22 : 1 ;
    unsigned char bit21 : 1 ;
    unsigned char bit20 : 1 ;
    unsigned char bit19 : 1 ;
    unsigned char bit18 : 1 ;
    unsigned char bit17 : 1 ;
    unsigned char bit16 : 1 ;
    unsigned char bit15 : 1 ;
    unsigned char bit14 : 1 ;
    unsigned char bit13 : 1 ;
    unsigned char bit12 : 1 ;
    unsigned char bit11 : 1 ;
    unsigned char bit10 : 1 ;
    unsigned char bit09 : 1 ;
    unsigned char bit08 : 1 ;
    unsigned char bit07 : 1 ;
    unsigned char bit06 : 1 ;
    unsigned char bit05 : 1 ;
    unsigned char bit04 : 1 ;
    unsigned char bit03 : 1 ;
    unsigned char bit02 : 1 ;
    unsigned char bit01 : 1 ;
    unsigned char bit00 : 1 ;
} FxPt_Rawbit ;

typedef struct tagFixedPointValue_I22D10
{ 
    signed int   integer : 22 ;
    unsigned int decimal : 10 ;

} FxPt_I22D10 ;

const float MAX_I22D10 = 2047.f + 0.9990234375f ;


typedef struct tagFixedPointValue_I24D8
{ 
    signed int   integer : 24 ;
    unsigned int decimal :  8 ;

} FxPt_I24D8 ;

typedef union tagFixedPointValue32
{
    FxPt_Rawbit rawbit   ;
    signed char sign : 1 ;
    FxPt_I22D10 i20d10   ;
    FxPt_I24D8  i24d8    ;
} FxPt32 ;


void showRawBit( const FxPt32* fxpt32)
{
    std::cout << 
        (int)fxpt32->rawbit.bit31 << (int)fxpt32->rawbit.bit30 <<
        (int)fxpt32->rawbit.bit29 << (int)fxpt32->rawbit.bit28 <<
        (int)fxpt32->rawbit.bit27 << (int)fxpt32->rawbit.bit26 << 
        (int)fxpt32->rawbit.bit25 << (int)fxpt32->rawbit.bit24 <<
        " " <<
        (int)fxpt32->rawbit.bit23 << (int)fxpt32->rawbit.bit22 <<
        (int)fxpt32->rawbit.bit21 << (int)fxpt32->rawbit.bit20 <<
        (int)fxpt32->rawbit.bit19 << (int)fxpt32->rawbit.bit18 <<
        (int)fxpt32->rawbit.bit17 << (int)fxpt32->rawbit.bit16 <<
        " " <<
        (int)fxpt32->rawbit.bit15 << (int)fxpt32->rawbit.bit14 <<
        (int)fxpt32->rawbit.bit13 << (int)fxpt32->rawbit.bit12 <<
        (int)fxpt32->rawbit.bit11 << (int)fxpt32->rawbit.bit10 <<
        (int)fxpt32->rawbit.bit09 << (int)fxpt32->rawbit.bit08 <<
        " " <<
        (int)fxpt32->rawbit.bit07 << (int)fxpt32->rawbit.bit06 <<
        (int)fxpt32->rawbit.bit05 << (int)fxpt32->rawbit.bit04 <<
        (int)fxpt32->rawbit.bit03 << (int)fxpt32->rawbit.bit02 <<
        (int)fxpt32->rawbit.bit01 << (int)fxpt32->rawbit.bit00 <<
        std::endl ;
}

void setFloat( const float float_val)
{
    if( )float_val

}



int main( int argc, char* argv[])
{
    FxPt32  x ;
    memset( &x, 0, sizeof(FxPt32)) ;
    x.rawbit.bit00 = 1 ;

    x.rawbit.bit18 = 1 ;

    std::cout << "Size of FxPt32: " << sizeof(FxPt32) << std::endl ;

    showRawBit( &x) ;
}
