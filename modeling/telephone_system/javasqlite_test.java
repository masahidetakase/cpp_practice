//javac  -classpath "./:/usr/lib64/javasqlite/sqlite.jar" javasqlite_test.java
//java -cp "./:/usr/lib64/javasqlite/sqlite.jar" javasqlite_test

import java.sql.*;

public class javasqlite_test
{
    public static void main( String args[])
    {
        try {
            Class.forName( "SQLite.JDBCDriver") ;
            
            Connection dbcon
                = DriverManager.getConnection( "jdbc:sqlite:/./telephone_system.db") ;

            Statement statement = dbcon.createStatement() ;
            String query_str = "SELECT access_code FROM accounts;" ;
            ResultSet result = statement.executeQuery( query_str) ; 

            while( result.next()) {
                String access_code = result.getString(1) ;
                System.out.println( "ACCESS_CODE: " + access_code) ;
            }

        } catch( Exception ex) {
            ex.printStackTrace() ;
        }


    }

} 