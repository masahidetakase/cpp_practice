#include <functional>
#include <array>
#include <iostream>

using namespace std;

int
main( int argc, char* argv[])
{
    array<uint8_t, 10> buf ;

    buf[0] = 0x12u;
    buf[1] = 0x34u;
    buf[2] = 0x56u;
    buf[3] = 0x78u;

    cout << "array->uint32: " << hex <<
            reinterpret_cast<uint32_t&>( buf[0]) << endl;
    return 0;
}