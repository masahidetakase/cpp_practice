#include <iostream>
#include <string>
#include <memory>
#include <cstdint>
#include <cctype>
#include <cassert>

static std::uint8_t
base64CharToValue( const char& ch) ;

// user-defined literal for binary digit
std::uint32_t
operator"" _bd( const char* str)
{
    std::string bin_digit_str( str) ;

    std::string::const_reverse_iterator rev_iter ;
    std::uint32_t digit_unit ;
    std::uint32_t value = 0 ;

    for( rev_iter = bin_digit_str.rbegin(), digit_unit = 1 ;
         rev_iter != bin_digit_str.rend() ;
         ++rev_iter, digit_unit *= 2) {

        if( !isdigit( *rev_iter)) { break ;}

        if( *rev_iter == '1') {
            value += digit_unit ;
        } else if( *rev_iter != '0') {
            assert( false) ;
        }
    }

    return value ;
}


std::string
operator"" _Base64Enc( const char* str, size_t num_chars)
{
    static const char Sextet_To_Char_Table[] =
        "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdef"
        "ghijklmnopqrstuvwxyz0123456789+/" ;

    struct EncodeStep
    {
        uint16_t mask ;
        uint32_t num_rshift_bit ;
        size_t octet_step ;
    } ;

    static const EncodeStep Encode_Steps[] =
        {
            { 0xFC00, 10, 0},
            { 0x03F0,  4, 1},
            { 0x0FC0,  6, 0},
            { 0x003F,  0, 2}
        } ;

    size_t sextets_len = ( num_chars * 8 + 6 - 1) / 6 ;
    size_t base64_str_len = ( ( sextets_len + 4 - 1) / 4) * 4 ;

    std::string base64_str( base64_str_len, '=') ;

    const uint8_t* octets_str = (const uint8_t*)str ;
    size_t octet_idx = 0 ;
    size_t encode_step_idx = 0 ;

    for( size_t sextet_idx = 0 ; sextet_idx < sextets_len ; ++sextet_idx) {

        uint16_t upper_octet = octets_str[octet_idx] ;
        uint16_t lower_octet = octet_idx != num_chars - 1 ? octets_str[octet_idx + 1] : 0 ;

        size_t table_idx =
            ( ( upper_octet << 8 | lower_octet) &
              Encode_Steps[encode_step_idx].mask) >>
            Encode_Steps[encode_step_idx].num_rshift_bit ;

        base64_str[sextet_idx] = Sextet_To_Char_Table[table_idx] ;

        octet_idx += Encode_Steps[encode_step_idx].octet_step ;

        encode_step_idx = ( encode_step_idx == 3) ? 0 : encode_step_idx + 1 ;
    }

    return base64_str ;
}


std::string
operator"" _Base64Dec( const char* str, size_t num_chars)
{
    assert( num_chars % 4 == 0) ;

    size_t octets_len = num_chars * 6 / 8 ;

    std::unique_ptr<std::uint8_t[]> decoded_uint8s( new std::uint8_t[octets_len]) ;

    static struct {
        size_t first_sextet_lshift ;
        size_t second_sextet_rshift ;
        size_t sextet_step ;
    } decode_step[3] =
        {
            { 2, 4, 1},
            { 4, 2, 1},
            { 6, 0, 2}
        } ;

    size_t decode_step_idx = 0 ;

    size_t sextet_idx = 0 ;

    for( size_t octet_idx = 0 ; octet_idx < octets_len ; ++octet_idx) {
        std::uint8_t upper_val = base64CharToValue(str[sextet_idx]) ;
        std::uint8_t lower_val = base64CharToValue(str[sextet_idx + 1]) ;

        upper_val <<= decode_step[decode_step_idx].first_sextet_lshift ;
        lower_val >>= decode_step[decode_step_idx].second_sextet_rshift ;

        decoded_uint8s[octet_idx] = (std::uint8_t)(( upper_val | lower_val) & 0xFF) ;

        sextet_idx += decode_step[decode_step_idx].sextet_step ;

        decode_step_idx = decode_step_idx == 2 ? 0 : decode_step_idx + 1 ;
    }

    return std::string( decoded_uint8s.get(), decoded_uint8s.get() + octets_len) ;
}


// BASE64 character -> six-bit value
static std::uint8_t
base64CharToValue( const char& ch)
{
    struct Base64CharToValue
    {
        std::uint8_t table[128] ;

        Base64CharToValue()
        {
            const char Sextet_To_Char_Table[] =
                "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdef"
                "ghijklmnopqrstuvwxyz0123456789+/" ;

            for( std::uint8_t idx = 0 ;
                 idx < sizeof(Sextet_To_Char_Table) / sizeof(char) ;
                 ++idx) {
                table[(size_t)Sextet_To_Char_Table[idx]] = idx ;
            }
        }
    } ;

    static Base64CharToValue base64_char_to_value ;

    return base64_char_to_value.table[(int)ch] ;
}


int
main( int argc, char* argv[])
{
    std::cout << 0001_bd << std::endl ;
    std::cout << 0010_bd << std::endl ;
    std::cout << 0011_bd << std::endl ;
    std::cout << 0100_bd << std::endl ;
    std::cout << 0101_bd << std::endl ;
    std::cout << 0110_bd << std::endl ;
    std::cout << 0111_bd << std::endl ;
    std::cout << 1000_bd << std::endl ;
    std::cout << 1001_bd << std::endl ;
    std::cout << 1010_bd << std::endl ;
    std::cout << 1011_bd << std::endl ;
    std::cout << 1100_bd << std::endl ;
    std::cout << 1101_bd << std::endl ;
    std::cout << 1110_bd << std::endl ;
    std::cout << 1111_bd << std::endl ;
    std::cout << 1000001_bd << std::endl ;

    std::cout << "This is a cat."_Base64Enc << std::endl ;
    std::cout << "44Gr44KD44KT44GT"_Base64Dec << std::endl ;

    std::cout << "にゃんこ"_Base64Enc << std::endl ;

    return 0 ;
}

