// Thread safe object pool.

#pragma once

#include <vector>
#include <mutex>
#include <climits>
#include <condition_variable>

namespace my_utility
{

template<class T>
class PooledObject
{
    public:
        PooledObject& operator -> () { return *m_object ;}

        PooledObject& operator * () { return *m_object ;}

    private:
        // prohibition against cloning.
        PooledObject( const PooledObject&) ;
        const PooledObject& operator = ( const PooledObject&) ;

        T* const m_object ;
} ;


template<class T, typename INIT_PROC>
class ThreadSafeObjectPool
{
    public:
        ThreadSafeObjectPool( const size_t pool_size, INIT_PROC initializer, const size_t max_pool_size = UINT_MAX)
            : m_initializer( initializer) {}

    private:
        size_t m_max_pool_size ;

        INIT_PROC m_initializer ;  // initilaizer( )

        std::vector<T> m_object_container ;

        std::vector<bool> m_object_status_array ;
} ;



} // the end of my_utility namespace.
