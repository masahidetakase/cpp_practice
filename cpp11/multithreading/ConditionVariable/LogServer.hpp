#pragma once
#include <string>
#include <mutex>
#include <condition_variable>
#include <queue>
#include <ostream>
#include <cstdint>
#include <chrono>
#include <functional>

class LogServer
{
    public:
        LogServer( std::ostream& log_ostrm) ;

        // prohibition against cloning.
        LogServer( const LogServer&) = delete ;
        LogServer( const LogServer&&) = delete ;
        LogServer& operator= (  const LogServer&) = delete ;
        LogServer& operator= (  const LogServer&&) = delete ;

        virtual ~LogServer() ;

        // メッセージ受付開始
        void
        start() ;

        // メッセージ受付停止
        void
        stop() ;

        // メッセージを送信する。
        void
        sendLog( const std::string& msg) ;

    private:
        static const std::chrono::duration<int,std::ratio<1,10>> Interval ;

        std::mutex m_mutex ;

        std::condition_variable m_not_empty_cond ;

        std::condition_variable m_active_cond ;

        bool m_is_active ;

        std::chrono::system_clock::time_point m_prev_time ;

        std::reference_wrapper<std::ostream> m_log_output_stream ;

        std::queue<std::string> m_lines ;

        void
        saveLog() ;

        void
        loop() ;
} ;