#include "LogServer.hpp"
#include <cursesw.h>
#include <ctime>
#include <condition_variable>

using namespace std ;
using namespace chrono ;

const chrono::duration<int,std::ratio<1,10>> LogServer::Interval(2) ;

LogServer::LogServer( std::ostream& log_ostrm)
    : m_is_active(false),
      m_prev_time( system_clock::now()),
      m_log_output_stream( log_ostrm) {}

void
LogServer::start()
{
    unique_lock<mutex> lock( m_mutex) ;

    if( !m_is_active) {
        m_is_active = true ;

        // output log start time.
        char time_str[128] ;
        auto cur_time = system_clock::to_time_t( system_clock::now()) ;
        ctime_r( &cur_time, time_str) ;

        m_log_output_stream.get() << "Start Logging: " << time_str << endl ;
    }
}

void
LogServer::stop()
{


}


void
LogServer::sendLog( const string& msg)
{

}

void
LogServer::saveLog()
{
    unique_lock<mutex> lock( m_mutex) ;

    condition_variable not_empty_cond ;

    // キューに値が入っていれば、次に進む。
    // そうでなければロックを開放し、待つ。
    not_empty_cond.wait( lock,
                         [this]()->bool {
                            return m_is_active && !this->m_lines.empty();
                         }) ;

    char time_str[128] ;
    auto cur_time = system_clock::to_time_t( system_clock::now()) ;
    ctime_r( &cur_time, time_str) ;

    while( !m_lines.empty()) {
        auto& log_str = m_lines.front() ;

        m_log_output_stream.get() << time_str << " " << log_str << endl ;

        m_lines.pop() ;
    }
}