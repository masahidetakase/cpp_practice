#pragma once
// Thread safe object pool.
#include <stack>
#include <vector>
#include <mutex>
#include <memory>
#include <functional>

namespace my_utility
{

template<typename T>
class ObjectPool
{
    public:
        // クライアントに提供するオブジェクトのタイプ
        typedef std::shared_ptr<T> provided_obj_type ;

        // クライアントに提供する前に行う処理の型
        typedef std::function<void(T&)> pre_rented_proc_type ;

        // クライアントから返却された後に行う処理の型
        typedef std::function<void(T&)> post_returned_proc_type ;

        explicit
        ObjectPool( const size_t num_objs_max,
                    const pre_rented_proc_type& pre_rented_proc = [](T&){},
                    const post_returned_proc_type& post_returned_proc = [](T&){})
            : m_pooled_objs( num_objs_max == 0 ? 10 : num_objs_max),
              m_pre_rented_proc( pre_rented_proc),
              m_post_returned_proc( post_returned_proc),
              m_takeBacker( this)
        {
            std::lock_guard<std::mutex> lock( m_mutex) ;

            for( auto& obj : m_pooled_objs) {
                m_inactive_objs.push( &obj) ;
            }
        }

        ObjectPool( const ObjectPool&) = delete ;
        ObjectPool( const ObjectPool&&) = delete ;

        ObjectPool
        operator= ( const ObjectPool&) = delete ;

        ObjectPool
        operator= ( const ObjectPool&&) = delete ;


        virtual ~ObjectPool() {}

        provided_obj_type
        get()
        {
            T* ret = nullptr ;

            std::lock_guard<std::mutex> lock( m_mutex) ;

            if( !m_inactive_objs.empty()) {
                ret = m_inactive_objs.top() ;
                m_inactive_objs.pop() ;
                m_pre_rented_proc( *ret) ;
            }

            return provided_obj_type( ret, m_takeBacker) ;
        }

        bool
        empty() const
        {
            std::lock_guard<std::mutex> lock( m_mutex) ;
            return( m_inactive_objs.size() == m_pooled_objs.size()) ;
        }

    private:
        std::mutex m_mutex ; // マルチスレッド対応

        // インスタンスを格納
        std::vector<T> m_pooled_objs ;

        // 未割り当てのインスタンスへのポインタを格納
        std::stack<T*> m_inactive_objs ;

        // 割り当てる前にインスタンスに処理を行う
        const pre_rented_proc_type m_pre_rented_proc ;

        // 返却されたインスタンスに後処理を行う
        const post_returned_proc_type m_post_returned_proc ;

        /* プールから割り当てたオブジェクトの参照カウントがゼロになると、
           自動的にプールに返却され、返却後処理が行われる。*/
        struct Takebacker
        {
            ObjectPool* const m_pool ;

            Takebacker( ObjectPool* const obj_pool)
                : m_pool( obj_pool) {}

            void
            operator() ( T* obj)
            {
                if( obj != nullptr) {
                    std::lock_guard<std::mutex> lock( m_pool->m_mutex) ;
                    m_pool->m_inactive_objs.push( obj) ;
                    m_pool->m_post_returned_proc( *obj) ;
                }
            }
        } ;

        Takebacker m_takeBacker ;
} ;

} // the end of my_utility namespace.
