#include "ObjectPool.hpp"
#include <iostream>
using namespace std ;
using namespace my_utility ;



int
main( int argc, char* argv[])
{
    ObjectPool<int> obj_pool( 5,
                             [](int& x){ x = 5;},
                             [](int& x){ cout << "latest value =" << x << endl;}) ;

    ObjectPool<int>::provided_obj_type obj[6] ;

    for( int i = 0 ; i < 2 ; ++i) {
        obj[i] = obj_pool.get() ;

        cout << "[" << i << "] = " ;
        if( obj[i].get() == nullptr) {
            cout <<  "NULL" ;
        } else {
            *obj[i] = i ;
            cout << *obj[i] ;
        }

        cout << endl;
    }

}
