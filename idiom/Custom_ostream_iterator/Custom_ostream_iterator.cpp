#include <iostream>
#include <string>
#include <sstream>
#include <iterator>
#include <algorithm>
#include <utility>

template<typename T>
class decorate_ostream_iterator : public std::iterator<std::output_iterator_tag, T>
{
    public:
        decorate_ostream_iterator( std::ostream& ostrm,
                                   const std::string& delim = "",
                                   const std::string& prefix = "",
                                   const std::string& suffix = "")
            : m_ostrm( ostrm), m_delimitor(delim), m_prefix( prefix), m_suffix( suffix) {}

        decorate_ostream_iterator& operator= ( const T& value)
        {
            m_ostrm << m_prefix << value << m_suffix << m_delimitor;
            return *this;
        }

        decorate_ostream_iterator& operator* () { return *this;}
        decorate_ostream_iterator& operator++ () { return *this;}
        decorate_ostream_iterator& operator++ (int) { return *this;}

    protected:
        std::ostream& m_ostrm;
        const std::string m_delimitor;
        const std::string m_prefix;
        const std::string m_suffix;
};


template<class InIter>
std::pair<InIter,InIter>
between( InIter target_first,
         InIter target_end,
         InIter fwd_ref_first,
         InIter fwd_ref_end,
         InIter back_ref_first,
         InIter back_ref_end)
{
    InIter fwd_ref_result = std::search( target_first, target_end, fwd_ref_first, fwd_ref_end);
    if( fwd_ref_result == target_end) { return std::make_pair(target_end,target_end);}

    int fwd_ref_len = std::distance( fwd_ref_first, fwd_ref_end);

    InIter back_ref_result = std::search( fwd_ref_result + fwd_ref_len, target_end, back_ref_first, back_ref_end);

    if( back_ref_result == target_end) { return std::make_pair(target_end,target_end);}

    return std::make_pair( fwd_ref_result + fwd_ref_len, back_ref_result);
}


int main( int argc, char* argv[])
{
    int int_values[] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 14, 18, -1, 5,2,7, 91, 45};
    decorate_ostream_iterator<int> deco_oiter( std::cout, ";", "<val/>", "</val>");

    std::copy( int_values, int_values + 9, deco_oiter);

    std::cout << std::endl;

    int fwd[] = { 14,18, -1};
    int back[] = { 91,45};

    std::pair<const int*, const int*> result =
    between( int_values, int_values + sizeof(int_values)/sizeof(int_values[0]),
             fwd, fwd + 3, back, back + 2);

    std::copy( result.first, result.second, std::ostream_iterator<int>(std::cout, ","));

    std::string str = "猫が大好き。";
    std::string str1 = "猫が";
    std::string str2 = "。";


    std::pair<std::string::const_iterator, std::string::const_iterator> result2 =
    between( str.begin(), str.end(),
             str1.begin(), str1.end(), str2.begin(), str2.end());

    std::copy( result2.first, result2.second, std::ostream_iterator<char>(std::cout));
}