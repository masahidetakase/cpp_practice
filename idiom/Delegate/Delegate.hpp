#pragma once

namespace utility
{

/**
 * Delegate class for invoking the method of the objects which has no argument and returns nothing.
 */
class DelegateBasae
{
    public:
        /**
         * Inoke the method of the object.
         */
        virtual void execute( void) = 0 ;

        virtual ~DelegateBasae() {}
} ;


/**
 * The template class for creating the delegates from any type of the objects.
 */
template< class T>
class Delegate : public DelegateBasae
{
    public:
        // the type of the pointer to "void T::method( void)" method. 
        typedef void ( T::*method_type)() ;

        Delegate( T& instance, method_type method) 
            : m_instance( &instance),
              m_method( method) {}

        void execute( void)
        {
            ( m_instance->*m_method)() ;
        }

        virtual ~Delegate() {}

    private:
        T* const m_instance ;

        method_type const m_method ;
} ;


template<class T>
DelegateBasae*
delegateFactory( T& obj, typename Delegate<T>::method_type method)
{
    return new Delegate<T>( obj, method) ;
}

} // the end of utility namespace.


