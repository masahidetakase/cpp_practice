#pragma once
#include <memory>
#include <vector>
#include <functional>

namespace linked_dsl
{
template<class operand_type>
class ChaninedProcess
{
    public:
        typedef std::shared_ptr<ChaninedProcess> proc_type;

        void
        linkParallele( const proc_type& proc)
        {
            m_parallele_linked_procs.push_back( proc);
        }

        void
        linkSerial( const proc_type& proc)
        {
            proc_type last_proc;

            for( last_proc = this;
                 last_proc->m_serial_linked_proc != nullptr;
                 last_proc = last_proc->m_serial_linked_proc) {}

            last_proc->m_serial_linked_proc = proc;
        }

        void
        execute( const operand_type& operand)
        {
            operand_type next_operand = process( operand);

            for( auto proc : m_parallele_linked_procs) {
                proc->execute
            }
        }

        virtual
        ~ChaninedProcess() {}

        virtual operand_type
        process( const operand_type& operand) = 0;

    protected:
        ChaninedProcess();

    private:
        std::vector<proc_type> m_parallele_linked_procs;

        proc_type m_serial_linked_proc;

};

} // the end of linked_dsl namespace.

template<class operand_type>
using ChainedProc =
    std::shared_ptr<linked_dsl::ChaninedProcess<operand_type>>;


template<class operand_type>
const ChainedProc<operand_type>
operator+ ( const ChainedProc<operand_type>& lhs,
            const ChainedProc<operand_type>& rhs)
{
    lhs->linkParallele( rhs);

    return lhs;
}

template<class operand_type>
const ChainedProc<operand_type>
operator* ( const ChainedProc<operand_type>& lhs,
            const ChainedProc<operand_type>& rhs)
{
    lhs->linkSerial( rhs);

    return lhs;
}

